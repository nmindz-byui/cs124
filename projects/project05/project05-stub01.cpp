#include <iostream>
#include <string>
using namespace std;

int main() {
  int year = 1865;
  
  int offsetYears = year - 1753;
  int offsetDays = offsetYears * 365;
  int leapDays = offsetYears / 4;
  int offset = offsetDays + leapDays;
  
  cout << offsetYears << "\n";
  cout << offsetDays << "\n";
  cout << leapDays << "\n";
  cout << offset << "\n";
  
  int mod7 = offset % 7;
  string day = "Noneday";
  
  switch (mod7) {
    case 1:
      day = "Monday";
      break;
    case 2:
      day = "Tuesday";
      break;
    case 3:
      day = "Wednesday";
      break;
    case 4:
      day = "Thursday";
      break;
    case 5:
      day = "Friday";
      break;
    case 6:
      day = "Saturday";
      break;
    case 0:
      day = "Sunday";
      break;
  }
  
  cout << day << "\n";
  
  return 0;
}
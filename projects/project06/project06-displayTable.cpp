#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

int main()
{
   int offset;
   int numDays;
   
   cout << "Enter offset: ";
   cin >> offset;
   
   cout << "Number of days: ";
   cin >> numDays;
   
   int padding = 2 * 2;
   int daysPadding = 4;
   
   if(offset < 6)
   {
      daysPadding = padding * ++offset;
   }
   
   int day = 1;
   int whileOffset = offset;

   string header = "  Su  Mo  Tu  We  Th  Fr  Sa";
   cout << header << endl;
   cout << setw(daysPadding) << "";

   while (day < numDays)
   {
      while (whileOffset < 7)
      {
         if (day > numDays) { break; }
         cout << setw(padding) << day;
         whileOffset += 1;
         day += 1;
      }
      whileOffset = 0;
      cout << endl;
   }
}
#include <iostream>
#include <string>
#include <cmath>
using namespace std;

int main()
{
   int month;
   int year;
   
   cout << "Enter a month number: ";
   cin >> month;
   
   cout << "Enter year: ";
   cin >> year;
   
   int offsetYears = year - 1753;
   int offsetDays = offsetYears * 365;
   int monthDays[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
   int leapYear = (year % 4) == 0;
   
   if(leapYear)
   {
      monthDays[1]++;
   }

   int currentYearDays = 0;
   
   for(int i = 0; i < month-1; i++)
   {
      currentYearDays += monthDays[i];
   }

   double leapDays = ((--year / 4.00) - (--year / 100.00) + (--year / 400.00)) - ((1753.00 / 4.00) - (1753.00 / 100.00) + (1753.00 / 400.00));
   int offset = offsetDays + round(leapDays) + currentYearDays;
   int mod7 = offset % 7;
   
   cout << "Offset: " << mod7 << endl;
}
/***********************************************************************
* Program:
*    Project 09, Mad Lib
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    The chapter preceding the last in this mad code challenge
*    
*    Estimated:  0.50 hrs   
*    Actual:     0.50 hrs
*
*     Honestly hate c-strings.
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;

/******************************************************************************
* Replaces tokens such as newline, quotes, etc...
******************************************************************************/
const char* parseToken(const char* WORD)
{
   //    #  Newline character. No space before or after.
   //    {  Open double quotes. Space before but not after.
   //    }  Close double quotes. Space after but not before.
   //    [  Open single quote. Space before but not after.
   //    ]  Close single quote. Space after but not before.

   if (WORD[1] == '{')
   {
      return "\"";
   }
   if (WORD[1] == '}')
   {
      return "\"";
   }
   if (WORD[1] == '[')
   {
      return "\'";
   }
   if (WORD[1] == ']')
   {
      return "\'";
   }
   if (WORD[1] == '#')
   {
      return "\n";
   }

   return WORD;
}

/******************************************************************************
* Magically transforms input placeholders into
* questions and stores their values
******************************************************************************/
void askQuestion(const char QUESTION[], char answer[][256])
{
   int pos = 1;
   char prompt[256][256];
   char questionable[256];

   if (QUESTION[0] != '<')
   {
      return;
   }

   for (int i = 0; i < 256; i++)
   {
      cout << "\t";
      while (true)
      {
         questionable[pos] = QUESTION[pos];
         if (QUESTION[pos] == '\0' || QUESTION[pos] == '>')
         {
            break;
         }
         if (QUESTION[pos] == '_')
         {
            questionable[pos] = ' ';
         }
         if (pos == 1)
         {
            questionable[pos] = toupper(QUESTION[pos]);
         }
         prompt[i][pos] = questionable[pos];
         cout << questionable[pos];
         pos++;
      }
      cout << ": ";
      cin.getline(answer[i], 256);
      break;
   }
}

/******************************************************************************
* exception genericFileError
******************************************************************************/
int genericFileError(char fileName[])
{
   cout << "Error reading file \"" << fileName << "\"" << endl;

   return -1;
}

/******************************************************************************
* Reads the file and calls the other functions
* related to processing the file (for now)
******************************************************************************/
char* readFile(char fileName[])
{
   ifstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      genericFileError(fileName);
   }

   int w = 0;
   char word[32];
   char story[1024][32];
   char answers[256][256];

   while (file >> word)
   {
      int c = 0;
      while (word[c] != ' ')
      {
         story[w][c] = word[c];
         c++;
      }
      askQuestion(parseToken(story[w]), answers);
      w++;
   }
   
   return 0;
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   char filename[256];
   cout << "Please enter the filename of the Mad Lib: ";
   cin.getline(filename, 256);

   readFile(filename);

   cout << "Thank you for playing." << endl;

   return 0;
}
/***********************************************************************
* Program:
*    Project 07, Calendar
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    A calendar generator for any month after January 1753.
*    
*    Estimated:  6.0 hrs   
*    Actual:     4.0 hrs
*
*      I had already written the code for computeOffset from the
*      pseudocode exercise a few days ago, so it was easier to
*      glue things all together and work with an already validated
*      algorithm. What was hard, however, was tracking a bug in the
*      display() function which I submitted to Assignment 25.
*
*      Yes, there was a bug that testBed didn't get right. It was
*      only with offset 5, due to a logic bug I introduced myself.
*      It has since been fixed in my repository and the code is as
*      it is in the function used in this project. Hope that doesn't
*      cost me a few points in grade. Sorry, should had tested more.
*
*      :)
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <string>
#include <cmath>
using namespace std;

/******************************************************************************
* Function that calculates the offset from our "epoch", January 1st, 1753
* and returns an integer that marks the first weekday of the month
******************************************************************************/
bool isLeapYear(int year)
{
   if (year % 100 == 0)
   {
      return (year % 400) == 0;
   }
   
   return (year % 4) == 0;
}

/******************************************************************************
* Counts how many leap years/days should be added to the offset
******************************************************************************/
double countLeapDays(int year)
{
   return ((--year / 4.00) - (--year / 100.00) + (--year / 400.00)) - ((1753.00 / 4.00) - (1753.00 / 100.00) + (1753.00 / 400.00));
}

/******************************************************************************
* Function that calculates the offset from our "epoch", January 1st, 1753
* and returns an integer that marks the first weekday of the month
******************************************************************************/
int computeOffset(int month, int year)
{
   int offsetYears = year - 1753;
   int offsetDays = offsetYears * 365;
   // Creates a map for each month's amount of days, since they are not regular
   int monthDays[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
   int leapYear = isLeapYear(year);
   
   if (leapYear)
   {
      monthDays[1]++;
   }
   
   int currentYearDays = 0;
   
   // Computes current year days past so far
   for (int i = 0; i < month-1; i++)
   {
      currentYearDays += monthDays[i];
   }
   
   // Counts how many leap years/days should be added to the offset
   double leapDays = countLeapDays(year);
   
   // Computes the final offset from regular years + leap years + current year days past up until the month
   int offset = offsetDays + round(leapDays) + currentYearDays;
   
   if (leapYear)
   {
      // Round down instead of up if leap year; prevents bad offset from January by 1
      offset = offsetDays + floor(leapDays) + currentYearDays;
   }
   
   int mod7 = offset % 7;
   
   return mod7;
}

/******************************************************************************
* Function responsible for calculating display offsets and printing
* the calendar in a table format
******************************************************************************/
void display(int numDays, int offset)
{
   int padding = 2 * 2;
   int daysPadding = 0;
   
   if (offset < 6)
   {
      daysPadding = padding * (offset+1);
   }
   
   int day = 1;
   int whileOffset = offset;
   
   string header = "  Su  Mo  Tu  We  Th  Fr  Sa";
   cout << header << endl;
   cout << setw(daysPadding) << "";
   
   // Make first row end and align properly
   ++whileOffset;
   
   while (day <= numDays)
   {
      while (whileOffset < 7)
      {
         if (day > numDays) { break; }
         cout << setw(padding) << day;
         whileOffset += 1;
         day += 1;
      }
      whileOffset = 0;
      
      // Treats the exception case when the week starts in a Sunday,
      // avoiding a breakline in the first row right after day 1
      if (offset == 6 && day == 2) { whileOffset++; continue; }
      if (offset == 6 && day < 2) { continue; }
      
      cout << endl;
   }
}

/******************************************************************************
* Retrieves user input and re-prompts if invalid
******************************************************************************/
void getUserInput(int &month, int &year)
{
   cout << "Enter a month number: ";
   cin >> month;
   
   while (!(month > 0 && month < 13))
   {
      cout << "Month must be between 1 and 12." << endl;
      cout << "Enter a month number: ";
      cin >> month;
   }
   
   cout << "Enter year: ";
   cin >> year;
   
   while (!(year >= 1753))
   {
      cout << "Year must be 1753 or later." << endl;
      cout << "Enter year: ";
      cin >> year;
   }
}

/*****************************************************************************
* The application entrypoint. The core. The beginning and the end.
*****************************************************************************/
int main()
{
   int month;
   int year;
   
   // TODO: Make monthDays available in a class so I don't have to declare it twice in code?
   // Maybe monthNames could also be reused in a similar way. Future updates, yay!
   int monthDays[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
   string monthNames[] = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
   
   getUserInput(month, year);
   
   int leapYear = isLeapYear(year);
   
   if (leapYear)
   {
      monthDays[1]++;
   }
   
   // Prints the header before the calendar table
   cout << endl << monthNames[month-1] << ", " << year << endl;
   
   int offset = computeOffset(month, year);
   display(monthDays[month-1], offset);
   
   return 0;
}
/***********************************************************************
* Program:
*    Project 10, Mad Lib
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    The final chapter in this mad challenge
*    
*    Estimated:  03.0 hrs   
*    Actual:     12.0 hrs
*
*     Had a hard time with resetting the variables for replay.
*     Could not, for the life of me, figure out how a \0 ends
*     in the output _some times_.
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

/******************************************************************************
* Replaces tokens such as newline, quotes, etc...
******************************************************************************/
const char* parseToken(const char* WORD)
{
   //    #  Newline character. No space before or after.
   //    {  Open double quotes. Space before but not after.
   //    }  Close double quotes. Space after but not before.
   //    [  Open single quote. Space before but not after.
   //    ]  Close single quote. Space after but not before.

   if (WORD[1] == '{')
   {
      return " \"";
   }
   if (WORD[1] == '}')
   {
      return "\" ";
   }
   if (WORD[1] == '[')
   {
      return " \'";
   }
   if (WORD[1] == ']')
   {
      return "\' ";
   }
   if (WORD[1] == '#')
   {
      return "\n";
   }

   return WORD;
}

/******************************************************************************
* Magically transforms input placeholders into
* questions and stores their values
******************************************************************************/
int askQuestion(const char QUESTION[], char (&answer)[256])
{
   int pos = 1;
   char questionable[256];

   if (QUESTION[0] != '<')
   {
      return 0;
   }

   cout << "\t";
   while (true)
   {
      questionable[pos] = QUESTION[pos];
      if (QUESTION[pos] == '\0' || QUESTION[pos] == '>')
      {
         break;
      }
      if (QUESTION[pos] == '_')
      {
         questionable[pos] = ' ';
      }
      if (pos == 1)
      {
         questionable[pos] = toupper(QUESTION[pos]);
      }
      cout << questionable[pos];
      pos++;
   }
   cout << ": ";
   cin.getline(answer, 256);

   return 1;
}

/******************************************************************************
* exception genericFileError
******************************************************************************/
int genericFileError(char fileName[])
{
   cout << "Error reading file \"" << fileName << "\"" << endl;

   return -1;
}

/******************************************************************************
* Reads the file and calls the other functions
* related to processing the file (for now)
******************************************************************************/
int readFile(char fileName[],
   char (*pStory)[32],
   char (*pAnswers)[256],
   int * pWordCount)
{
   ifstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      return genericFileError (fileName);
   }

   int a = 0;
   char word[32];

   while (file >> word)
   {
      int c = 0;
      while (word[c] != '\0')
      {
         pStory[* pWordCount][c] = word[c];
         c++;
      }
      a += askQuestion(parseToken(pStory[* pWordCount]), pAnswers[a]);
      (* pWordCount)++;
   }

   cout << "\n";

   return 0;
}

/******************************************************************************
* Displays the final story
******************************************************************************/
void display(char (*pStory)[32], char (*pAnswers)[256], int * pWordCount)
{
   int answersIdx = 0;
   for (int i = 0; i < * pWordCount; i++)
   {
      const char *TOKEN = parseToken(pStory[i]);
      const char *NEXT_TOKEN = parseToken(pStory[i + 1]);
      const char *PREVIOUS_TOKEN = parseToken(pStory[i - 1]);
      int isVar = (TOKEN[0] == '<');

      if (isVar)
      {
         cout << pAnswers[answersIdx];
         answersIdx++;
      } else if (pStory[i][1] == '}' && NEXT_TOKEN == "\n")
      {
         cout << TOKEN[0];
         continue;
      }
      else if (pStory[i][1] == '}' && pStory[i + 1][1] == '{')
      {
         cout << TOKEN;
         continue;
      }
      else if (pStory[i][1] == '{' && pStory[i - 1][1] == '}')
      {
         cout << TOKEN[1];
         continue;
      }
      else if (PREVIOUS_TOKEN == "\n" && pStory[i][0] == '<')
      {
         cout << TOKEN[1];
         continue;
      }
      else
      {
         cout << TOKEN;
      }

      if (NEXT_TOKEN[0] == ' ' ||
         (pStory[i][0] == '<' && !isVar) ||
         (pStory[i + 1][0] == '<' &&
            pStory[i + 1][1] == '{' ||
            pStory[i + 1][1] == '[') ||
         (pStory[i + 1][0] == '<' &&
            pStory[i + 1][1] == '}' ||
            pStory[i + 1][1] == ']') ||
         (pStory[i + 1][0] == '<' &&
            pStory[i + 1][1] == '#') ||
         NEXT_TOKEN[0] == ',' ||
         NEXT_TOKEN[0] == '.' ||
         NEXT_TOKEN[0] == '?' ||
         NEXT_TOKEN[0] == '!')
      {
         continue;
      }

      cout << " ";
   }
   cout << endl;
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   int wordCount = 0;
   int * pWordCount = &wordCount;
   char filename[256];
   char story[1024][32];
   char (*pStory)[32] = story;
   char answers[256][256];
   char (*pAnswers)[256] = answers;
   char replay = 'y';

   int storyReadSuccessfully = 0;

   while (replay != 'n')
   {
      std::memset(story, 0, sizeof (story));
      std::memset(answers, 0, sizeof (answers));
      std::memset(filename, 0, sizeof (filename));

      cout << "Please enter the filename of the Mad Lib: ";
      cin.getline(filename, 256);

      storyReadSuccessfully = readFile(filename, pStory, pAnswers, pWordCount);

      if (storyReadSuccessfully != 0)
      {
         return storyReadSuccessfully;
      }

      display(pStory, pAnswers, pWordCount);

      cout << "Do you want to play again (y/n)? ";
      cin.get(replay);
      cin.ignore(1024, '\n');
   }
   
   cout << "Thank you for playing." << endl;

   return 0;
}
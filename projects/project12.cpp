/***********************************************************************
* Program:
*    Project 12, Sudoku
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    The Sudoku challenge
*    
*    Estimated:  3.0 hrs   
*    Actual:     3.0 hrs
*
*     The display function. Again, those trailing spaces before the
*     newlines that testBed insists in picking on are a hell to deal
*     with. And it seems that my \b hack somehow triggers an error in
*     testBed even though the output at the end is exactly what it is
*     expecting for. If that is a problem (testBed errors even when
*     it should be ok) will I lose grade for that?
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cstring>

using namespace std;

/******************************************************************************
* Prototypes
******************************************************************************/
int genericFileError(char fileName[]);
int interact(int (*board)[9]);
int* computeRow(int offsetX, int (*board)[9], int (*row));
int* computeCol(int offsetY, int (*board)[9], int (*column));
int* computeRegion(int offsetX, int offsetY, int (*board)[9], int (*region));
void computeValues(int row, int col, int (*board)[9]);
int* computeValuesRaw(int row, int col, int (*board)[9]);
int saveBoard(char* filename, int (*board)[9]);
void editBoard(int offsetX, int offsetY, int (*board)[9], int value);
int readFile(char fileName[], int (*board)[9]);
int initializeBoard(int (*board)[9]);
int menuHelp();
int menuDisplay(int (*board)[9]);
int menuEditBoard(int (*board)[9]);
int menuComputeValues(int (*board)[9]);
int menuSaveQuit(int (*board)[9]);
void printHeaderRow();
void print3x3GridLine();
int translateY(const char X);
int checkColumnRange(const char X, const char Y);
int checkFilledSquare(int x, int y, int (*board)[9]);
int checkEditValue(int offsetX, int offsetY, int (*board)[9], int value);
int main();

/******************************************************************************
* 050%
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   int board[9][9];

   initializeBoard(board);

   return 0;
}


/******************************************************************************
* 100%
* exception genericFileError
******************************************************************************/
int genericFileError(char fileName[])
{
   cout << "Error reading file \"" << fileName << "\"" << '\n';

   return -1;
}

/******************************************************************************
* 100%
* Interacts with the user (interface)
******************************************************************************/
int interact(int (*board)[9])
{
   char option;

   cout << "> ";
   cin >> option;

   int retCode = 0;

   switch (option)
   {
      case 'D':
      case 'd':
         retCode = menuDisplay(board);
         cout << '\n';
         break;
      case 'E':
      case 'e':
         retCode = menuEditBoard(board);
         break;
      case 'S':
      case 's':
         retCode = menuComputeValues(board);
         break;
      case 'Q':
      case 'q':
         retCode = menuSaveQuit(board);
         break;
      case '?':
      default:
         retCode = menuHelp();
         cout << '\n';
         cout << '\n';
         break;
   }
   return retCode;
}

/******************************************************************************
* 100%
* Possible value for the row
******************************************************************************/
int* computeRow(int offsetX,
   int (*board)[9],
   int (*row))
{
   for (int i = 0; i < 9; i++)
   {
      board[offsetX][i] > 0 ? row[i] = board[offsetX][i] : row[i] = 0;
   }
}

/******************************************************************************
* 100%
* Possible value for the column
******************************************************************************/
int* computeCol(int offsetY,
   int (*board)[9],
   int (*column))
{
   for (int i = 0; i < 9; i++)
   {  
      board[i][offsetY] > 0 ? column[i] = board[i][offsetY] : column[i] = 0;
   }
}

/******************************************************************************
* 100%
* Possible value for adjacent 3x3 grid
******************************************************************************/
int* computeRegion(int offsetX,
   int offsetY,
   int (*board)[9],
   int (*region))
{
   int offsets[] = { 0, 3, 6 };
   int beginX = 0;
   int beginY = 0;
   int pos = 0;

   offsetX > 5 ? beginX = 6 : offsetX > 2 ? beginX = 3 : beginX = 0;
   offsetY > 5 ? beginY = 6 : offsetY > 2 ? beginY = 3 : beginY = 0;

   for (int i = beginX; i < beginX + 3; i++)
   {
      for (int j = beginY; j < beginY + 3; j++)
      {
         board[i][j] > 0 ? region[pos] = board[i][j] : region[pos] = 0;
         pos++;
      }
   }
}

/******************************************************************************
* 100%
* Calculates all possible values for a cell
******************************************************************************/
void computeValues(int row, int col, int (*board)[9])
{
   //     0 1 2 3 4 5 6 7 8
   //   =====================
   // 0 | 7 2 3 x x x 1 5 9 | 0
   //   |===================|
   // 1 | 6 x x 3 x 2 x x 8 | 1
   //   |===================|
   // 2 | 8 x x x 1 x x x 2 | 2
   //   |===================|
   // 3 | x 7 x 6 5 4 x 2 x | 3
   //   |===================|
   // 4 | x x 4 2 x 7 3 x x | 4
   //   |===================|
   // 5 | x 5 x 9 3 1 x 4 x | 5
   //   |===================|
   // 6 | 5 x x x 7 x x x 3 | 6
   //   |===================|
   // 7 | 4 x x 1 x 3 x x 6 | 7
   //   |===================|
   // 8 | 9 3 2 x x x 7 1 4 | 8
   //   |===================|
   //     0 1 2 3 4 5 6 7 8
   int pRegion[9];
   int pColumn[9];
   int pRow[9];
   int possible[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
   int possibleCount = 0;
   int possibleOut = 1;
   // int possibleNone = 8;

   computeRegion(row, col, board, pRegion);
   computeCol(col, board, pColumn);
   computeRow(row, board, pRow);

   for (int i = 0; i < 9; i++)
   {
      if (pRegion[i] > 0)
      {
         possible[pRegion[i] - 1]++;
      }
   }
   
   for (int i = 0; i < 9; i++)
   {
      if (pColumn[i] > 0)
      {
         possible[pColumn[i] - 1]++;
      }
   }

   for (int i = 0; i < 9; i++)
   {
      if (pRow[i] > 0)
      {
         possible[pRow[i] - 1]++;
      }
   }

   for (int i = 0; i < 9; i++)
   {
      if (possible[i] == 0)
      {
         possibleCount++;
      }
   }
   
   for (int i = 0; i < 9; i++)
   {
      if (possible[i] == 0 && possibleOut < possibleCount)
      {
         cout << i + 1 << ", ";
         possibleOut++;
      }
      else if (possible[i] == 0)
      {
         cout << i + 1;
      }
      // TODO: Give better output when there are
      // no possible values - ie.: You insert a bad value
      // 
      // else {
      //    possibleNone--;
      // }

      // if (possibleNone == 0)
      // {
      //    cout << "None";
      // }
   }

   return;
}

/******************************************************************************
* 100%
* Calculates all possible values for a cell, without cout
******************************************************************************/
int* computeValuesRaw(int row, int col, int (*board)[9])
{
   int pRegion[9];
   int pColumn[9];
   int pRow[9];
   int possible[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

   computeRegion(row, col, board, pRegion);
   computeCol(col, board, pColumn);
   computeRow(row, board, pRow);

   for (int i = 0; i < 9; i++)
   {
      if (pRegion[i] > 0)
      {
         possible[pRegion[i] - 1]++;
      }
   }
   
   for (int i = 0; i < 9; i++)
   {
      if (pColumn[i] > 0)
      {
         possible[pColumn[i] - 1]++;
      }
   }

   for (int i = 0; i < 9; i++)
   {
      if (pRow[i] > 0)
      {
         possible[pRow[i] - 1]++;
      }
   }

   int* hPossible = new int[9];
   for (int i = 0; i < 9; i++)
   {
      hPossible[i] = possible[i];
   }
   
   return hPossible;
}

/******************************************************************************
* 100%
* Edit/change a cell value in the board
******************************************************************************/
void editBoard(int offsetX, int offsetY, int (*board)[9], int value)
{
   if (value < 10)
   {
      board[offsetX][offsetY] = value;
      return;
   }
   else
   {
      cout << "Invalid value, try again." << '\n';
      return;
   }
}

/******************************************************************************
* 100%
* Reads the file and calls the other functions
* related to processing the file (for now)
******************************************************************************/
int readFile(char fileName[], int (*board)[9])
{
   ifstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      return genericFileError (fileName);
   }

   int i = 0;
   char cell[32];

   for (int i = 0; i < 9; i++)
   {
      for (int j = 0; j < 9; j++)
      {
         file >> cell;
         board[i][j] = atoi(cell);
      }
   }

   return 0;
}

/******************************************************************************
* 100%
* Initializes the empty board/game and runs the first
* few iterations to set things in place
******************************************************************************/
int initializeBoard(int (*board)[9])
{
   char filename[256];
   
   cout << "Where is your board located? ";
   cin.getline(filename, 256);
   int readBoard = readFile(filename, board);

   if (readBoard != 0)
   {
      return readBoard;
   }

   menuHelp();
   cout << '\n';
   menuDisplay(board);
   cout << '\n';

   int repeat = 0;
   while (repeat == 0)
   {
      repeat = interact(board);
   }
}

/******************************************************************************
* 100%
* Displays help instructions
******************************************************************************/
int menuHelp()
{
   cout << "Options:" << '\n';
   cout << "   " << "?  Show these instructions" << '\n';
   cout << "   " << "D  Display the board" << '\n';
   cout << "   " << "E  Edit one square" << '\n';
   cout << "   " << "S  Show the possible values for a square" << '\n';
   cout << "   " << "Q  Save and Quit" << '\n';

   return 0;
}

/******************************************************************************
* 100%
* Displays the board
******************************************************************************/
int menuDisplay(int (*board)[9])
{
   printHeaderRow();
   for (int i = 0; i < 9; i++)
   {
      cout << i + 1 << "  ";
      for (int j = 0; j < 9; j++)
      {
         if ((j + 1) % 3 == 0 && j != 8)
         {
            if (board[i][j] == 0)
            {
               cout << " " << "|";
               continue;
            }
            cout << board[i][j] << "|";
            continue;
         }
         if (board[i][j] == 0)
         {
            cout << " " << " ";
            continue;
         }
         cout << board[i][j] << " ";
      }
      cout << '\b';
      cout << '\n';

      if ((i + 1) % 3 == 0 && i < 8)
      {
         print3x3GridLine();
      }
   }

   return 0;
}

/******************************************************************************
* 100%
* Edits a square
******************************************************************************/
int menuEditBoard(int (*board)[9])
{
   char coordinate[3];
   int value = 0;

   cin.ignore(1024, '\n');
   cout << "What are the coordinates of the square: ";
   cin.getline(coordinate, 3);

   int coordinateY = coordinate[0];
   int coordinateX = coordinate[1] - '0';

   if (checkColumnRange(coordinate[1], coordinate[0]) < 0)
   {
      cout << "ERROR: Square '" << coordinate << "' is invalid";
      cout << '\n';
      return 0;
   }

   coordinateY = translateY(coordinate[0]);
   coordinateX = coordinate[1] - '0';

   if (checkFilledSquare(coordinateX - 1, coordinateY, board))
   {
      cout << "ERROR: Square '" << coordinate << "' is filled";
      cout << '\n';
      cout << '\n';
      return 0;
   }

   cout << "What is the value at '"
      << coordinate
      << "': ";
   cin >> value;
   cin.ignore(1024, '\n');

   // if (checkEditValue(coordinateX - 1, coordinateY, board, value))
   // {
   //    cout << "ERROR: Value '"
   //       << value
   //       << "' in square '"
   //       << coordinate
   //       << "' is invalid";
   //    cout << '\n';
   //    return 0;
   // }

   editBoard(coordinateX - 1, coordinateY, board, value);
   cout << '\n';

   return 0;
}

/******************************************************************************
* 100%
* Calls the compute function
******************************************************************************/
int menuComputeValues(int (*board)[9])
{
   char coordinate[3];

   cin.ignore(1024, '\n');
   cout << "What are the coordinates of the square: ";
   cin.getline(coordinate, 3);

   int coordinateY = coordinate[0];
   int coordinateX = coordinate[1] - '0';

   if (checkColumnRange(coordinate[1], coordinate[0]) < 0)
   {
      cout << "ERROR: Square '" << coordinate << "' is invalid";
      cout << '\n';
      return 0;
   }

   coordinateY = translateY(coordinate[0]);
   coordinateX = coordinate[1] - '0';

   cout << "The possible values for '"
      << coordinate
      << "' are: ";

   computeValues(coordinateX - 1, coordinateY, board);
   cout << '\n';

   return 0;
}

/******************************************************************************
* 100%
* Gets file name and saves file
******************************************************************************/
int menuSaveQuit(int (*board)[9])
{
   char fileName[256];
   cin.ignore(1024, '\n');

   cout << "What file would you like to write your board to: ";
   cin.getline(fileName, 256);

   saveBoard(fileName, board);

   cout << "Board written successfully" << '\n';

   return 1;
}

/******************************************************************************
* 100%
* Saves the board to file then quits
******************************************************************************/
int saveBoard(char* fileName, int (*board)[9])
{
   ofstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      return genericFileError (fileName);
   }

   for (int i = 0; i < 9; i++)
   {
      for (int j = 0; j < 9; j++)
      {
         file << board[i][j] << " ";
      }
      file << '\n';
   }

   file.close();

   return 1;
}

/******************************************************************************
* 100%
* Helper function to print the header board row
******************************************************************************/
void printHeaderRow()
{
   cout << "   A B C D E F G H I" << '\n';
}

/******************************************************************************
* 100%
* Helper function to print the 3x3 grid line
******************************************************************************/
void print3x3GridLine()
{
   cout << "   -----+-----+-----" << '\n';
}

/******************************************************************************
* 100%
* Helper function to translate X coordinates A through I to int
******************************************************************************/
int translateY(const char X)
{
   switch (X)
   {
      case 'a':
      case 'A':
         return 0;
         break;
      case 'b':
      case 'B':
         return 1;
         break;
      case 'c':
      case 'C':
         return 2;
         break;
      case 'd':
      case 'D':
         return 3;
         break;
      case 'e':
      case 'E':
         return 4;
         break;
      case 'f':
      case 'F':
         return 5;
         break;
      case 'g':
      case 'G':
         return 6;
         break;
      case 'h':
      case 'H':
         return 7;
         break;
      case 'i':
      case 'I':
         return 8;
         break;
   }
}

/******************************************************************************
* 100%
* Helper function to check if column letter is within range
******************************************************************************/
int checkColumnRange(const char X, const char Y)
{  
   int xx = X - '0';
   if (xx < 1 || xx > 9)
   {
      return -1;
   }

   switch (Y)
   {
      case 'a':
      case 'A':
         return 0;
         break;
      case 'b':
      case 'B':
         return 1;
         break;
      case 'c':
      case 'C':
         return 2;
         break;
      case 'd':
      case 'D':
         return 3;
         break;
      case 'e':
      case 'E':
         return 4;
         break;
      case 'f':
      case 'F':
         return 5;
         break;
      case 'g':
      case 'G':
         return 6;
         break;
      case 'h':
      case 'H':
         return 7;
         break;
      case 'i':
      case 'I':
         return 8;
         break;
      default:
         return -1;
         break;
   }
}

/******************************************************************************
* 100%
* Helper function to check if cell is filled
******************************************************************************/
int checkFilledSquare(int x, int y, int (*board)[9])
{
   if (board[x][y] != 0)
   {
      return 1;
   }

   return 0;
}

/******************************************************************************
* 100%
* Helper function to check if edit value is valid
******************************************************************************/
int checkEditValue(int offsetX, int offsetY, int (*board)[9], int value)
{
   if (value > 9)
   {
      return 1;
   }

   int* tmp = computeValuesRaw(offsetX - 1, offsetY, board);

   for (int i = 0; i < 9; i++)
   {
      if (i + 1 == value && tmp[i] == 0)
      {
         delete(tmp);
         return 0;
      }
   }
   
   delete(tmp);
   return 1;
}
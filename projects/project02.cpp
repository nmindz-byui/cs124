/***********************************************************************
* Program:
*    Project 02, Monthly Budget
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    This is a simple monthly budget calculator which takes into account
*    several inputs, such as your income, expected living expenses,
*    tithing, taxes and others in order to provide you an estimate of
*    monthly cash flow, in a simplified and aligned table view.
*    
*    Estimated:  6.0 hrs   
*    Actual:     3.0 hrs
*
*      The hardest piece of this assignment, to me, was computeTax
*      since the numbers seemed a bit "unreal", but it might just be
*      me not being used to US tax regulations.
*
*      Also, I tried to make it more "human-readable" by implementing
*      a 'type' of ValueList, which would work something like
*      "associative arrays" do in PHP, without any object functions or
*      anything sophisticated; the only goal was to have an array indexed
*      by strings to reduce code and make it easier to read.
*
*      I did not take performance into account. Also passing variables
*      "by reference" was an exercise since in the past I seem to have
*      messed things up and I've been away from pointers for a while. :)
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <map>
using namespace std;

/******************************************************************************
 * Defines a type of ValueList, which maps a string to a
 * corresponding double value. This structure resembles that of
 * associative arrays in PHP. i.e.: array["name"] = "Evandro";
 *****************************************************************************/
typedef map < string, double > ValueList;

/******************************************************************************
 * The definition of each "get" function, one for each expense/income source
 *****************************************************************************/

/******************************************************
 *  function getIncome 
 *****************************************************/
double getIncome()
{
   double x = 0.00;
   cout << "\t"
        << "Your monthly income: ";
   cin >> x;

   return x;
}

/******************************************************
 *  function getBudgetLiving 
 *****************************************************/
double getBudgetLiving()
{
   double x = 0.00;
   cout << "\t"
        << "Your budgeted living expenses: ";
   cin >> x;

   return x;
}

/******************************************************
 *  function getActualLiving 
 *****************************************************/
double getActualLiving()
{
   double x = 0.00;
   cout << "\t"
        << "Your actual living expenses: ";
   cin >> x;

   return x;
}

/******************************************************
 *  function getActualTax 
 *****************************************************/
double getActualTax()
{
   double x = 0.00;
   cout << "\t"
        << "Your actual taxes withheld: ";
   cin >> x;

   return x;
}

/******************************************************
 *  function getactualTithing 
 *****************************************************/
double getactualTithing()
{
   double x = 0.00;
   cout << "\t"
        << "Your actual tithe offerings: ";
   cin >> x;

   return x;
}

/******************************************************
 *  function getActualOther 
 *****************************************************/
double getActualOther()
{
   double x = 0.00;
   cout << "\t"
        << "Your actual other expenses: ";
   cin >> x;

   return x;
}

/******************************************************************************
 * The definition of each "compute" function, responsible for variable expenses
 *****************************************************************************/

/******************************************************
 *  function computeTithing 
 *****************************************************/
double computeTithing(double &income)
{
   return income * 0.1;
}

/******************************************************
 *  function computeTax 
 *****************************************************/
double computeTax(double &income)
{
   double baseMinimumTax = 0.00,
          previousIncomeLimit = 0.00,
          taxPercent = 0.00;

   double yearlyIncome = income * 12;

   if (yearlyIncome > 336550.00)
   {
      baseMinimumTax = 91043.00;
      previousIncomeLimit = 336550.00;
      taxPercent = 0.35;
   }
   else if (yearlyIncome > 188450.00)
   {
      baseMinimumTax = 42170.00;
      previousIncomeLimit = 188450.00;
      taxPercent = 0.33;
   }
   else if (yearlyIncome > 123700.00)
   {
      baseMinimumTax = 24040.00;
      previousIncomeLimit = 123700.00;
      taxPercent = 0.28;
   }
   else if (yearlyIncome > 61300.00)
   {
      baseMinimumTax = 8440.00;
      previousIncomeLimit = 61300.00;
      taxPercent = 0.25;
   }
   else if (yearlyIncome > 15100.00)
   {
      baseMinimumTax = 1510.00;
      previousIncomeLimit = 15100.00;
      taxPercent = 0.15;
   }
   else if (yearlyIncome > 0.00)
   {
      baseMinimumTax = 0.00;
      previousIncomeLimit = 0.00;
      taxPercent = 0.10;
   }

   return baseMinimumTax + taxPercent * (yearlyIncome - previousIncomeLimit);
}

/******************************************************************************
 * These are "helper" functions, designed to make `cout` output
 * more easily readable and error-free, according to design rules
 *****************************************************************************/

/******************************************************
 *  function writeRow 
 *****************************************************/
void writeRow(string row, int padding, double v1, double v2)
{
   cout << "\t"
        << row
        << setw(padding)
        << "$" << setw(11) << v1 << setw(5)
        << "$" << setw(11) << v2 << ""
        << endl;
}

/******************************************************
 *  function writeHeader 
 *****************************************************/
void writeHeader()
{
   cout << "\t"
        << "Item" << setw(24)
        << "Budget" << setw(16)
        << "Actual"
        << endl;
}

/******************************************************
 *  function writeDivider 
 *****************************************************/
void writeDivider()
{
   // Displays a divider row
   cout << "\t"
        << "==============="
        << " "
        << "==============="
        << " "
        << "==============="
        << endl;
}

/******************************************************************************
 * The display function responsible for calling computing functions,
 * output formatting functions and organizing all data in a orderly manner.
 *****************************************************************************/

/******************************************************
 *  function display 
 *****************************************************/
void display(ValueList inputs)
{
   // Calculate my expected debts
   double budgetTax = computeTax(inputs["income"]) / 12;
   double budgetTithing = computeTithing(inputs["income"]);

   double budgetOther = inputs["income"] - budgetTax
      - budgetTithing - inputs["budgetLiving"];

   double actualDifference = inputs["income"] - inputs["actualTax"]
      - inputs["actualTithing"] - inputs["actualLiving"]
      - inputs["actualOther"];

   double budgetDifference = 0;

   // Displays the header
   writeHeader();
   // Writes a header and body divider
   writeDivider();
   // Displays the rows, aligned, with their respective values
   writeRow("Income", 11, inputs["income"], inputs["income"]);
   // writeRow("Taxes", 12, budgetTax, inputs["actualTax"]);
   writeRow("Taxes", 12, 0.00, inputs["actualTax"]);
   // writeRow("Tithing", 10, budgetTithing, inputs["actualTithing"]);
   writeRow("Tithing", 10, 0.00, inputs["actualTithing"]);
   writeRow("Living", 11, inputs["budgetLiving"], inputs["actualLiving"]);
   // writeRow("Other", 12, budgetOther, inputs["actualOther"]);
   writeRow("Other", 12, 0.00, inputs["actualOther"]);
   // Writes a final divider row
   writeDivider();
   // writeRow("Difference", 7, budgetDifference, actualDifference);
   writeRow("Difference", 7, 0.00, 0.00);
}

/******************************************************************************
 * The application entrypoint. Responsible for setting global defaults,
 * displaying the first few lines of output and instructions and also
 * assigning the core variables at run time. Also closes execution after
 * calling the `display` function.
 *****************************************************************************/

/******************************************************
 *  function main 
 *****************************************************/
int main()
{
   cout.setf(ios::fixed);     // no scientific notation please
   cout.setf(ios::showpoint); // always show the decimal for real numbers
   cout.precision(2);         // two digits after the decimal

   // PUT greeting on the screen
   cout << "This program keeps track of your monthly budget" << endl;
   cout << "Please enter the following:" << endl;

   // Declares all variables and initialize them to make sure
   // that we will not get any trash from memory addresses
   ValueList inputs;

   inputs["income"] = getIncome();
   inputs["budgetLiving"] = getBudgetLiving();
   inputs["actualLiving"] = getActualLiving();
   inputs["actualTax"] = getActualTax();
   inputs["actualTithing"] = getactualTithing();
   inputs["actualOther"] = getActualOther();

   cout << "\nThe following is a report on your monthly expenses" << endl;
   display(inputs);

   return 0;
}
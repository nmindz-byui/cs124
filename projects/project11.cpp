/***********************************************************************
* Program:
*    Project 11, Sudoku
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    The Sudoku challenge
*    
*    Estimated:  03.0 hrs   
*    Actual:     12.0 hrs
*
*     Hardships goes here
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cstring>

using namespace std;

/******************************************************************************
* Prototypes
******************************************************************************/
int genericFileError(char fileName[]);
void interact(int (*board)[9]);
int* computeRow(int offsetX, int (*board)[9], int (*row));
int* computeCol(int offsetY, int (*board)[9], int (*column));
int* computeRegion(int offsetX, int offsetY, int (*board)[9], int (*region));
void computeValues(int row, int col, int (*board)[9]);
void saveBoard(const char* filename, int (*board)[9]);
int* editBoard(int offsetX, int offsetY, int (*board)[9], int value);
int readFile(char fileName[], int (*board)[9]);
int initializeBoard(int (*board)[9]);
void menuHelp();
void menuDisplay(int (*board)[9]);
void menuEditBoard(int (*board)[9]);
void menuComputeValues(int (*board)[9]);
void menuSaveQuit(int (*board)[9]);
int main();

/******************************************************************************
* PSEUDO FUNCTIONS
******************************************************************************/

// menuDisplay(board[9])
//    FOR i = 0 to 9 by 1s
//       FOR j = 0 to 9 by 1s
//          PUT board[i][j]
//          PUT " "
//       PUT '\n';
//    PUT '\n'
// end

// menuEditBoard(board)
//    SET coordinate
//    SET value

//    PUT “What are the coordinates of the square: “
//    GET coordinate

//    PUT “What is the value at ‘“
//    PUT coordinate
//    PUT “’: “
//    GET value

//    SET coordinateX = coordinate[1]
//    SET coordinateY = coordinate[0]

//    editBoard(coordinateX, coordinateY, value, board)
// end

// menuComputeValues(board)
//    SET coordinate

//    PUT “What are the coordinates of the square: “
//    GET coordinate

//    SET coordinateX = coordinate[1]
//    SET coordinateY = coordinate[0]

//    PUT “The possible values for 'B1' are: “
//    computeValue(coordinateX, coordinateY, board)
// end

// menuSaveQuit(board)
//    SET filename[256]
   
//    PUT “What file would you like to write your board to: “
//    GET filename

//    // Writes to current in-memory board to a file
//    saveBoard(filename, board)
// end


/******************************************************************************
* exception genericFileError
******************************************************************************/
int genericFileError(char fileName[])
{
   cout << "Error reading file \"" << fileName << "\"" << endl;

   return -1;
}

/******************************************************************************
* Interacts with the user (interface)
******************************************************************************/
void interact(int (*board)[9])
{
   char option;

   cout << "> ";
   cin >> option; 

   switch(option)
   {
      case 'D':
         menuDisplay(board);
         break;
      case 'E':
         menuEditBoard(board);
         break;
      case 'S':
         menuComputeValues(board);
         break;
      case 'Q':
         menuSaveQuit(board);
         break;
      case '?':
      default:
         menuHelp();
         break;
   }
}

/******************************************************************************
* Possible value for the row
******************************************************************************/
int* computeRow(int offsetX,
   int (*board)[9],
   int (*row))
{
   for (int i = 0; i < 9; i++)
   {
      board[offsetX][i] > 0 ? row[i] = board[offsetX][i] : row[i] = 0;
   }
}

/******************************************************************************
* Possible value for the column
******************************************************************************/
int* computeCol(int offsetY,
   int (*board)[9],
   int (*column))
{
   for (int i = 0; i < 9; i++)
   {  
      board[i][offsetY] > 0 ? column[i] = board[i][offsetY] : column[i] = 0;
   }
}

/******************************************************************************
* Possible value for adjacent 3x3 grid
******************************************************************************/
int* computeRegion(int offsetX,
   int offsetY,
   int (*board)[9],
   int (*region))
{
   int offsets[] = { 0, 3, 6 };
   int beginX = 0;
   int beginY = 0;
   int pos = 0;

   offsetX > 5 ? beginX = 6 : offsetX > 2 ? beginX = 3 : beginX = 0;
   offsetY > 5 ? beginY = 6 : offsetY > 2 ? beginY = 3 : beginY = 0;

   for (int i = beginX; i < beginX+3; i++)
   {
      for (int j = beginY; j < beginY+3; j++)
      {
         board[i][j] > 0 ? region[pos] = board[i][j] : region[pos] = 0;
         pos++;
      }
   }
}

/******************************************************************************
* Calculates all possible values for a cell
******************************************************************************/
void computeValues(int row, int col, int (*board)[9])
{
   //     0 1 2 3 4 5 6 7 8
   //   =====================
   // 0 | 7 2 3 x x x 1 5 9 | 0
   //   |===================|
   // 1 | 6 x x 3 x 2 x x 8 | 1
   //   |===================|
   // 2 | 8 x x x 1 x x x 2 | 2
   //   |===================|
   // 3 | x 7 x 6 5 4 x 2 x | 3
   //   |===================|
   // 4 | x x 4 2 x 7 3 x x | 4
   //   |===================|
   // 5 | x 5 x 9 3 1 x 4 x | 5
   //   |===================|
   // 6 | 5 x x x 7 x x x 3 | 6
   //   |===================|
   // 7 | 4 x x 1 x 3 x x 6 | 7
   //   |===================|
   // 8 | 9 3 2 x x x 7 1 4 | 8
   //   |===================|
   //     0 1 2 3 4 5 6 7 8
   int pRegion[9];
   int pColumn[9];
   int pRow[9];
   int possible[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

   computeRegion(row, col, board, pRegion);
   computeCol(col, board, pColumn);
   computeRow(row, board, pRow);

   for (int i = 0; i < 9; i++)
   {
      if (pRegion[i] > 0)
      {
         possible[pRegion[i] - 1]++;
      }
   }
   
   for (int i = 0; i < 9; i++)
   {
      if (pColumn[i] > 0)
      {
         possible[pColumn[i] - 1]++;
      }
   }

   for (int i = 0; i < 9; i++)
   {
      if (pRow[i] > 0)
      {
         possible[pRow[i] - 1]++;
      }
   }

   for (int i = 0; i < 9; i++)
   {
      if (possible[i] == 0)
      {
         cout << i+1 << ", ";
      }
   }
   cout << '\b';
}

/******************************************************************************
* Reads the file and calls the other functions
* related to processing the file (for now)
******************************************************************************/
int readFile(char fileName[], int (*board)[9])
{
   ifstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      return genericFileError (fileName);
   }

   int i = 0;
   char cell[32];

   //    A B C D E F G H I
   // 1  7 2 3|     |1 5 9
   // 2  6    |3   2|    8
   // 3  8    |  1  |    2
   //    -----+-----+-----
   // 4    7  |6 5 4|  2  
   // 5      4|2   7|3  
   // 6    5  |9 3 1|  4 
   //    -----+-----+-----
   // 7  5    |  7  |    3
   // 8  4    |1   3|    6
   // 9  9 3 2|     |7 1 4

   for (int i = 0; i < 9; i++)
   {
      for (int j = 0; j < 9; j++)
      {
         file >> cell;
         board[i][j] = atoi(cell);
      }
   }

   computeValues(3, 2, board);
   cout << '\n' << '\n';

   return 0;
}

int initializeBoard(int (*board)[9])
{
   char filename[256];
   
   cout << "Where is your board located?: ";
   cin.getline(filename, 256);
   int readBoard = readFile(filename, board);

   if (readBoard != 0)
   {
      return readBoard;
   }

   menuHelp();
   menuDisplay(board);
   interact(board);

}

/******************************************************************************
* Displays help instructions
******************************************************************************/
void menuHelp()
{
   cout << "Options:" << '\n';
   cout << "   " << "?  Show these instructions" << '\n';
   cout << "   " << "D  Display the board" << '\n';
   cout << "   " << "E  Edit one square" << '\n';
   cout << "   " << "S  Show the possible values for a square" << '\n';
   cout << "   " << "Q  Save and quit" << '\n';
   cout << '\n';
}

/******************************************************************************
* Displays the board
******************************************************************************/
void menuDisplay(int (*board)[9])
{
   for (int i = 0; i < 9; i++)
   {
      for (int j = 0; j < 9; j++)
      {
         cout << board[i][j] << " ";
      }
      cout << '\n';
   }
   cout << '\n';
}

/******************************************************************************
* Edits a square
******************************************************************************/
void menuEditBoard(int (*board)[9])
{
   //
}

/******************************************************************************
* Calls the compute function
******************************************************************************/
void menuComputeValues(int (*board)[9])
{
   //
}

/******************************************************************************
* Saves the board to file then quits
******************************************************************************/
void menuSaveQuit(int (*board)[9])
{
   //
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   int board[9][9];
   // char replay = 'y';

   // while (replay != 'n')
   // {
   //    std::memset(story, 0, sizeof (board));
   //    cout << "Do you want to play again (y/n)? ";
   //    cin.get(replay);
   //    cin.ignore(1024, '\n');
   // }

   initializeBoard(board);
   
   cout << "Thank you for playing." << endl;

   return 0;
}
/***********************************************************************
* Program:
*    Project 08, Mad Lib
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    Manipulating strings, part 1 of 2
*    
*    Estimated:  0.50 hrs   
*    Actual:     0.50 hrs
*
*     Took me some reading to realize it's all about the null character 
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <fstream>
#include <map>

using namespace std;

/******************************************************************************
 * Defines a type of Inputs, which maps a string to a
 * corresponding array of chars (string, yay). This structure resembles that of
 * associative arrays in PHP. i.e.: array["name"] = "Evandro";
 *****************************************************************************/
typedef map < string, char[] > Inputs;

/******************************************************************************
* Replaces tokens such as newline, quotes, etc...
******************************************************************************/
const char* parseToken(const char* word)
{
   //    #  Newline character. No space before or after.
   //    {  Open double quotes. Space before but not after.
   //    }  Close double quotes. Space after but not before.
   //    [  Open single quote. Space before but not after.
   //    ]  Close single quote. Space after but not before.

   if (word[1] == '{')
   {
      return "\"";
   }
   if (word[1] == '}')
   {
      return "\"";
   }
   if (word[1] == '[')
   {
      return "\'";
   }
   if (word[1] == ']')
   {
      return "\'";
   }
   if (word[1] == '#')
   {
      return "\n";
   }

   return word;
}

/******************************************************************************
* It asks all those pesky questions we seek answers for
******************************************************************************/
void askQuestions (Inputs &answers)
{
   cout << "Please enter the filename of the Mad Lib: ";
   cin.getline(answers["file"], 256);

   cout << "\tPlural noun: ";
   cin.getline(answers["plural_noun1"], 256);

   cout << "\tPlural noun: ";
   cin.getline(answers["plural_noun2"], 256);

   cout << "\tType of liquid: ";
   cin.getline(answers["type_of_liquid"], 256);

   cout << "\tAdjective: ";
   cin.getline(answers["adjective1"], 256);

   cout << "\tFunny noise: ";
   cin.getline(answers["funny_noise"], 256);

   cout << "\tAnother funny noise: ";
   cin.getline(answers["another_funny_noise"], 256);

   cout << "\tAdjective: ";
   cin.getline(answers["adjective2"], 256);

   cout << "\tAnimal: ";
   cin.getline(answers["animal1"], 256);

   cout << "\tAnother animal: ";
   cin.getline(answers["animal2"], 256);
}

/******************************************************************************
* Performs the act of asking a single question
******************************************************************************/
void askQuestion(char question[])
{
  int pos = 1;
  char prompt[256][256];

  for(int i = 0; i < 256; i++)
  {
    while(true)
    {
      if (question[pos] == '\0' || question[pos] == '>')
      {
        break;
      }
      if (question[pos] == '_')
      {
        question[pos] = ' ';
      }
      if (pos == 1)
      {
        question[pos] = toupper(question[pos]);
      }
      prompt[i][pos] = question[pos];
      cout << question[pos];
      pos++;
    }
    cout << ": " << endl;
    break;
  }
}

/******************************************************************************
* exception genericFileError
******************************************************************************/
int genericFileError(char fileName[])
{
   cout << "Error reading file \"" << fileName << "\"" << endl;

   return -1;
}

/******************************************************************************
* function readFile
******************************************************************************/
char* readFile(char fileName[])
{
   ifstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      cout << genericFileError(fileName);
   }

   // Zoos are places where wild <plural_noun> are kept in pens or cages <#> so
   // that <plural_noun> can come and look at them . There is a zoo <#> in the park 
   // beside the <type_of_liquid> fountain . When it is feeding time , <#> all 
   // the animals make <adjective> noises . The elephant goes <{> <funny_noise> 
   // <}> <#> and the turtledoves go <{> <another_funny_noise> . <}> My favorite 
   // animal is the <#> <adjective> <animal> , so fast it can outrun a/an 
   // <another_animal> . <#> You never know what you will find at the zoo . <#>

   int w = 0;
   char word[32];
   char story[256][32];

   while (file >> word)
   {
      int c = 0;
      while (word[c] != ' ')
      {
         story[w][c] = word[c];
         c++;
      }
      cout << parseToken(story[w]) << ' ';
      w++;
   }

   return 0;
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   Inputs answers;
   // askQuestions(answers);

   // char madlib[] = readFile(answers["file"]);
   char filename[256];
   cout << "Please enter the filename of the Mad Lib: ";
   cin.getline(filename, 256);

   readFile(filename);

   return 0;
}
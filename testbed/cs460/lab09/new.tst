#
# The first test is straight out of the textbook. 
# This should be easy to verify
# See /home/cs460/week09/lab09-1.txt
+
=Which node would you like to compute the shortest path from? 
<u
# Similar to figure ___
#
=The routing table is:\n
=         Destination  Distance    Predecessor\n
=                   v         2              u\n
=                   w         3              y\n
=                   x         1              u\n
=                   y         2              x\n
=                   z         4              y\n
# Similar to figure ___
#
=The forwarding table is:\n
=         Destination  Link\n
=                   v  (u, v)\n
=                   w  (u, x)\n
=                   x  (u, x)\n
=                   y  (u, x)\n
=                   z  (u, x)\n
-
# Same graph as in figure ____ except starting from node w
# See /home/cs460/week09/lab09-1.txt
#
+
=Which node would you like to compute the shortest path from? 
<w
=The routing table is:\n
=         Destination  Distance    Predecessor\n
=                   u         3              x\n
=                   v         3              w\n
=                   x         2              y\n
=                   y         1              w\n
=                   z         3              y\n
=The forwarding table is:\n
=         Destination  Link\n
=                   u  (w, y)\n
=                   v  (w, v)\n
=                   x  (w, y)\n
=                   y  (w, y)\n
=                   z  (w, y)\n
-
# This is the star-shaped graph from the Prepare reading quiz
# See /home/cs460/week09/lab09-2.txt
#
+ /home/cs460/week09/lab09-2.txt
=Which node would you like to compute the shortest path from? 
<x
=The routing table is:\n
=         Destination  Distance    Predecessor\n
=                   v         3              w\n
=                   w         1              x\n
=                   y         1              x\n
=                   z         2              x\n
=The forwarding table is:\n
=         Destination  Link\n
=                   v  (x, w)\n
=                   w  (x, w)\n
=                   y  (x, y)\n
=                   z  (x, z)\n
-
# The same star-shaped graph from the previous example
# This time starting from node W
# See /home/cs460/week09/lab09-2.txt
#
+ /home/cs460/week09/lab09-2.txt
=Which node would you like to compute the shortest path from? 
<w
=The routing table is:\n
=         Destination  Distance    Predecessor\n
=                   v         2              w\n
=                   x         1              w\n
=                   y         2              x\n
=                   z         3              w\n
=The forwarding table is:\n
=         Destination  Link\n
=                   v  (w, v)\n
=                   x  (w, x)\n
=                   y  (w, x)\n
=                   z  (w, z)\n
-
# This is a complex network with 24 nodes. It is actually the same 
# network we did our "Network game" with last week. You may recognize
# several of the IP addresses
# See /home/cs460/week09/lab09-3.txt
#
+ /home/cs460/week09/lab09-3.txt
=Which node would you like to compute the shortest path from? 
<82.13.0.95
=The routing table is:\n
=         Destination  Distance    Predecessor\n
=         12.0.128.14        75     14.0.128.0\n
=         131.27.0.15        10     82.13.0.95\n
=          14.0.128.0        25     82.13.0.95\n
=         14.0.128.14       125    14.0.128.15\n
=         14.0.128.15        75     14.0.128.0\n
=           147.0.0.0        30    131.27.0.15\n
=          147.0.0.13        70      147.0.0.0\n
=         147.0.0.192        70      147.0.0.0\n
=          205.0.0.10        30    131.27.0.15\n
=         205.16.42.0        60     205.0.0.10\n
=        205.187.12.0        60     205.0.0.10\n
=        205.38.192.0        60     205.0.0.10\n
=        239.15.157.0        30    131.27.0.15\n
=        239.15.157.5        50   239.15.157.0\n
=        239.15.157.6        70   239.15.157.5\n
=        239.15.157.7        70   239.15.157.5\n
=           65.19.3.0        25     82.13.0.95\n
=          65.19.3.12        85      65.19.3.0\n
=          65.19.3.15        85      65.19.3.0\n
=         65.19.3.249       105      65.19.3.0\n
=           79.0.0.10        25     82.13.0.95\n
=        79.10.87.215        50      79.0.0.10\n
=         79.36.0.249        75   79.10.87.215\n
=        79.86.12.192        75   79.10.87.215\n
=The forwarding table is:\n
=         Destination  Link\n
=         12.0.128.14  (82.13.0.95, 14.0.128.0)\n
=         131.27.0.15  (82.13.0.95, 131.27.0.15)\n
=          14.0.128.0  (82.13.0.95, 14.0.128.0)\n
=         14.0.128.14  (82.13.0.95, 14.0.128.0)\n
=         14.0.128.15  (82.13.0.95, 14.0.128.0)\n
=           147.0.0.0  (82.13.0.95, 131.27.0.15)\n
=          147.0.0.13  (82.13.0.95, 131.27.0.15)\n
=         147.0.0.192  (82.13.0.95, 131.27.0.15)\n
=          205.0.0.10  (82.13.0.95, 131.27.0.15)\n
=         205.16.42.0  (82.13.0.95, 131.27.0.15)\n
=        205.187.12.0  (82.13.0.95, 131.27.0.15)\n
=        205.38.192.0  (82.13.0.95, 131.27.0.15)\n
=        239.15.157.0  (82.13.0.95, 131.27.0.15)\n
=        239.15.157.5  (82.13.0.95, 131.27.0.15)\n
=        239.15.157.6  (82.13.0.95, 131.27.0.15)\n
=        239.15.157.7  (82.13.0.95, 131.27.0.15)\n
=           65.19.3.0  (82.13.0.95, 65.19.3.0)\n
=          65.19.3.12  (82.13.0.95, 65.19.3.0)\n
=          65.19.3.15  (82.13.0.95, 65.19.3.0)\n
=         65.19.3.249  (82.13.0.95, 65.19.3.0)\n
=           79.0.0.10  (82.13.0.95, 79.0.0.10)\n
=        79.10.87.215  (82.13.0.95, 79.0.0.10)\n
=         79.36.0.249  (82.13.0.95, 79.0.0.10)\n
=        79.86.12.192  (82.13.0.95, 79.0.0.10)\n
-

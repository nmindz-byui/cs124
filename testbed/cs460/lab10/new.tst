#
# These tests increase in difficulty. I recommend first trying to pass
# the earlier tests before attempting the later ones.
#
#------------------------------------------------------------
#
# Empty strings should generate an error message
#
+
=Please specify the message bits: 
<\n
=The message is empty, quitting.\n
-
# Next, a Parity Bit example with no errors. Note how
# the empty string for the generator yields in only the
# Parity code being run.
#
+
=Please specify the message bits: 
<0101\n
=Please specify the generator bits: 
<\n
=The parity bit of 0101 is 0\n
-
# We should simply ignore all input that is not a 1 or a 0
#
+
=Please specify the message bits: 
<    0ignore1ignore      0 space 1\n
=Please specify the generator bits: 
<\n
=The parity bit of 0101 is 0\n
-
# The CRC example from the textbook
#
+
=Please specify the message bits: 
<101110\n
=Please specify the generator bits: 
<1001\n
=The parity bit of 101110 is 0\n
=The CRC value of 101110 with a generator of 1001 is 011\n
-
# A more complicated example
#
+
=Please specify the message bits: 
<10100111001010101\n
=Please specify the generator bits: 
<11100101\n
=The parity bit of 10100111001010101 is 1\n
=The CRC value of 10100111001010101 with a generator of 11100101 is 1110110\n
-
# Long message and short generator
#
+
=Please specify the message bits: 
<1111001011010110\n
=Please specify the generator bits: 
<11\n
=The parity bit of 1111001011010110 is 0\n
=The CRC value of 1111001011010110 with a generator of 11 is 0\n
-
# Message and CRC are the same length
#
+
=Please specify the message bits: 
<101010101\n
=Please specify the generator bits: 
<111010100\n
=The parity bit of 101010101 is 1\n
=The CRC value of 101010101 with a generator of 111010100 is 11101000\n
-
# Generator begins with a zero. Note how we need to align
# the most significant bits of the message and generator. 
# This means we skip over the leading 0's.
#
+
=Please specify the message bits: 
<11010101\n
=Please specify the generator bits: 
<011\n
=The parity bit of 11010101 is 1\n
=The CRC value of 11010101 with a generator of 011 is 01\n
-
# The message begins with a zero. Again, we need to align
# the left-most (most significant) bit of the message and
# the generator.
#
+
=Please specify the message bits: 
<0001110101\n
=Please specify the generator bits: 
<101\n
=The parity bit of 0001110101 is 1\n
=The CRC value of 0001110101 with a generator of 101 is 10\n
-

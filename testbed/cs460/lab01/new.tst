#
# This first test will verify that we can get a single word 
# from the input buffer
#
+
# Initially, there is no upper case letters.
#
=Enter a string: 
<nothing
=Enter a string: 
<
=No strings starting with an upper case letter were seen.\n
-
# Same test as before but with a single upper case letter
#
+
=Enter a string: 
<OneString
=Enter a string: 
<
=One string starting with an upper case letter was seen.\n
-
# Now we will have two strings with upper case letters
#
+
=Enter a string: 
<One
=Enter a string: 
<Two
=Enter a string: 
<
=You entered 2 strings that started with an upper case letter.\n
-
# Now we will need to read in more than one word per line. 
# You will need to use getline() to handle this.
#
+
=Enter a string: 
<This is one very long string. We should only look at the first line of input.
=Enter a string: 
<
=One string starting with an upper case letter was seen.\n
-
# Multiple long lines will be tested here
#
+
=Enter a string: 
<tHIS SENTENCE HAS ONLY ONE LOWERCASE LETTER - THE FIRST ONE!
=Enter a string: 
<this sentence is all lowercase
=Enter a string: 
<
=No strings starting with an upper case letter were seen.\n
-
# Just a simple greeting
#
+
=Enter a string: 
<Hi
=Enter a string: 
<there
=Enter a string: 
<CS 460
=Enter a string: 
<student
=Enter a string: 
<
=You entered 2 strings that started with an upper case letter.\n
-
# Empty string case
#
+
=Enter a string: 
<
=No strings starting with an upper case letter were seen.\n
-

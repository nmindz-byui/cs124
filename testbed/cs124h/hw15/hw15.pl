#!/usr/bin/perl

#Author: Burdette Pixton
#Program: hw09.pl
#Summary: this will make a file that will be used for testing
#         the vending machine program (hw09)

@upChar = A..E;
@lowerChar = a..e;
@numbers = 1..5;

@errorChar = ('R', 'h', 'B', 'o');
@errorNum = ( '6', '9', '8', 'o');

@value = (
   ["35", "empty", "90", "50", "75"],
   ["55","10","empty","empty","20"],
   ["empty", "85", "40", "95", "60"],
   ["15", "empty", "65", "5", "100"],
   ["70","45","25","80","30"]
   );

open(TEMP, ">hw09.test");
&start;
for($i = 0; $i < 5; $i++)
{
   for($j = 0; $j < 5; $j++)
   {
      print TEMP "\n=Grid location: ";
      $grid = "$upChar[$i]$numbers[$j]";
      print TEMP "\n<$grid";
      &iffer;
      print TEMP "\n\$";
      print TEMP "\n=Grid location:";
      $grid = "$numbers[$j]$upChar[$i]";
      print TEMP "\n<$grid";
      &switcher;
      &iffer;
   }
}
print TEMP "\n=Grid location: ";
print TEMP "\n<OO";
print TEMP "\n-\n";
&start;

for($i = 0; $i < 5; $i++)
{
   for($j = 0; $j < 5; $j++)
   {
      print TEMP "\n=Grid location: ";
      $grid = "$lowerChar[$i]$numbers[$j]";
      print TEMP "\n<$grid";
      &iffer;
      print TEMP "\n\$";
      print TEMP "\n=Grid location: ";
      $grid = "$numbers[$j]$lowerChar[$i]";
      print TEMP "\n<$grid";
      &switcher;
      &iffer;
      print TEMP "\n\$";
   }
}
#error portion
for($i = 0; $i < 4; $i++)
{
   for($j = 0; $j < 4; $j++)
   {
      print TEMP "\n=Grid location: ";
      $grid = "$errorChar[$i]$errorNum[$j]";
      print TEMP "\n<$grid";
      print TEMP "\n=Input error: letter range is A-E, number range is 1-5.";
      print TEMP "\n\$";
   }
}
#end of error portion

print TEMP "\n=Grid location: ";
print TEMP "\n<00";
print TEMP "\n-";

sub start
{
   print TEMP "+\n";
   print TEMP "\$\n";
   print TEMP "=Enter a grid location (such as B2, 00 to terminate) ";
}

sub switcher
{
   $oldGrid = "$numbers[$j]$upChar[$i]";
   $grid = "$upChar[$i]$numbers[$j]";
   print TEMP "\n=Input of $oldGrid is being processed as $grid.";
}

sub iffer
{
   $grid = "$upChar[$i]$numbers[$j]";

   if( $value[$i][$j] =~ /empty/)
   {
      print TEMP "\n=Invalid selection: $grid is empty.";
   }
   else
   {
      print TEMP "\n=Item $grid costs $value[$i][$j] cents.";
      &change($grid);
   print TEMP "\n=Change is $qu quarter(s), $di dime(s), and $ni nickel(s).";
   }
}

sub change
{
   $price = $value[$i][$j];
   $input = 100 - $price;
   $quarter = $input / 25;
   $input %= 25;
   $dime = $input / 10;
   $input %= 10;
   $nickel = $input / 5;
   $input %= 5;
   $qu = &trunk($quarter);
   $di = &trunk($dime);
   $ni = &trunk($nickel);
}

sub trunk
{
     $_ = @_[0];
     /(\d+?)/;
     return $1;
}
#!/usr/bin/perl

#Author: Burdette Pixton
#Program: hw11.pl
#Summary: this will make a file that will be used for testing
#         the state program (hw11)

open(TEMP, ">hw11.test");
print TEMP "+";
print TEMP "\n\$\n";
print TEMP "=states containing 3 unique letters
=   Ohio\n";
print TEMP "=states containing 4 unique letters
=   Alabama
=   Alaska
=   Hawaii
=   Indiana
=   Iowa
=   Kansas
=   Mississippi
=   Tennessee
=   Utah\n";
print TEMP "=states containing 5 unique letters
=   Arkansas
=   Idaho
=   Illinois
=   Maine
=   Montana
=   Nevada
=   Oregon
=   Texas\n";
print TEMP "=states containing 6 unique letters
=   Arizona
=   Colorado
=   Delaware
=   Georgia
=   Missouri
=   Oklahoma
=   Virginia
=   Wisconsin\n";
print TEMP "=states containing 7 unique letters
=   Connecticut
=   Florida
=   Kentucky
=   Louisiana
=   Maryland
=   Michigan
=   Nebraska
=   New Jersey
=   New York
=   Vermont
=   Wyoming\n";
print TEMP "=states containing 8 unique letters
=   California
=   Massachusetts
=   Minnesota
=   New Mexico
=   North Dakota
=   South Dakota\n";
print TEMP "=states containing 9 unique letters
=   North Carolina
=   Pennsylvania
=   Washington\n";
print TEMP "=states containing 10 unique letters
=   New Hampshire
=   Rhode Island
=   West Virginia\n";
print TEMP "=states containing 11 unique letters
=   South Carolina";
   print TEMP "\n-";


#!/usr/bin/perl
until($valid)
{
   $valid = 1;
   print "Enter a month number: ";
   chomp($a = <STDIN>);
   if ($a > 12 || $a < 1)
   {
      print "Month must be between 1 and 12.\n";
      $valid = 0;
   }
}
$valid = 0;
until($valid)
{
   $valid = 1;
   print "Enter year: ";
   chomp($b = <STDIN>);
   if ($b < 1753)
   {
      print "Year must be 1753 or later.\n";
      $valid = 0;
   }
}
chomp(@cal = `cal $a $b`);
($a, $b, @cal) = @cal;
$a=~s/^\s*(.*?)\s*$/$1/;
$a=~s/ /, /;
$b=~s/^\s*(.*?)\s*$/$1/;
print "\n$a\n";
print "  $b\n";
for (@cal)
{
   print "  $_\n" if ($_);
}

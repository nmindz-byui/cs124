#! /usr/bin/perl

#Author: Burdette Pixton
#Program: hw07.pl
#Summary: this will make a file that will be used for testing
#         the temperature into either fahrenheit or celsius(hw07)

#this will make a file that will be used for testing the temperature (hw06)
@input = ("-10.1f","68.4F","846.0c","-533.0C","100.0f","0.9F",
          "100.5c","0.8C","0.9F","90.0F","933.3C","-476.2C","80.0F","98.6F",
          "-40.0c","212.0f","0.0C","-40.0j","23.6u","455.0T");
@correct = ("-10.1F","68.4F","846.0C","-533.0C","100.0F","0.9F",
          "100.5C","0.8C","0.9F","90.0F","933.3C","-476.2C","80.0F","98.6F",
          "-40.0C","212.0F","0.0C","-40.0J","23.6U","455.0T");
@temp = ("-23.4C","20.2C","1554.8F","-927.4F","37.8C","-17.3C","212.9F",
         "33.4F","-17.3C","32.2C","1711.9F","-825.2F","26.7C","37.0C",
         "-40.0F","100.0C","32.0F",
         "invalid - conversion not possible.",
         "invalid - conversion not possible.",
         "invalid - conversion not possible.");

open(TEMP, ">hw06.test");

for($i = 0; $i < 20; $i++)
{
   print TEMP "+\n";
   print TEMP "\$\n";
   print TEMP "=Enter temperature and an F (for Fahrenheit) or a C (for Celsius): ";
   print TEMP "\n<$input[$i]\n";
   print TEMP "=$correct[$i] is $temp[$i]\n";
   print TEMP "-\n";
}




#!/usr/bin/perl

#Author: Burdette Pixton
#Program: hw10.pl
#Summary: this will make a file that will be used for testing
#         the day of the week program(hw10)

@days = ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
         "Saturday");

@dates =("12/13/1812","1/1/1753","10/29/1929","2/29/1984","11/20/3006",
         "1/23/1880","3/1/1760");
@errorDates =("2/29/1900","13/1/1996","4/31/1780","1/1/1752","5/32/1950");
$errorMsg ="Input error: the";
@errorMsgs =("day must range from 1 to 28","month must range from 1 to 12",
             "day must range from 1 to 30","year must be greater than 1752",
             "day must range from 1 to 31");


open(TEMP, ">hw10.test");
print TEMP "+";
for($i = 0; $i < 7; $i++)
{
   &start;
   print TEMP "\n<$dates[$i]";
   print TEMP "\n=$dates[$i] is a $days[$i]";
}
for($i = 0; $i < 5; $i++)
{
   &start;
   print TEMP "\n<$errorDates[$i]";
   print TEMP "\n=$errorMsg $errorMsgs[$i]";
}
&start;
print TEMP "\n<0";
print TEMP "\n-";


sub start
{
   print TEMP "\n\$";
   print TEMP "\n=Enter date (MM/DD/YYYY) or 0 (zero) to quit: ";
}


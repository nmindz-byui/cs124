#!/usr/bin/perl

#Author: Burdette Pixton
#Program: hw12.pl
#Summary: this will make a file that will be used for testing
#         prime numbers from 1 - 1033 (hw12)

#make a hash, keys are the prime numbers, each number has a value of 1;
#go though each key with a value of 1, if it is divisable,
#put a zero has the value.

$max = 1034

open(TEMP, ">hw12.test");
print TEMP "+\n\$\n";

for($i = 2; $i < $max; $i++)
{
   $prime{$i} = 1;
}

for($i = 2; $i < $max; $i++)
{
   if($prime{$i})
   {
      for(keys %prime)
      {
         if($i < $_)
         {
            unless($_ % $i) #if it is false(zero), no reminder
            {
               $prime{$_} = 0;
            }
         }
      }
   }
}
&in_arr;
@result = sort by_number @arr;


for(@result)
{
   print TEMP "=$_\n";
}
print TEMP "-";

sub in_arr
{
   $j = 0;
   for(keys %prime)
   {
      if($prime{$_})
      {
         $arr[$j] = $_;
         $j++;
      }
   }
}

sub by_number
{
   $a <=> $b
}

+
=Select the test you want to run:\n
=\t1. Just create and destroy a hash\n
=\t2. The above plus add a few entries\n
=\t3. The above plus copy a hash table\n
=\t4. The above plus look for the entries\n
=\ta. Spell check\n
=> 
<1
#Create, destroy, and copy a Hash
=Create an integer Hash\n
=\tSize:     0\n
=\tCapacity: 5\n
=\tEmpty?    Yes\n
=Create a float Hash\n
=\tSize:     0\n
=\tCapacity: 10\n
=\tEmpty?    Yes\n
=Create a float Hash using the copy constructor\n
=\tSize:     0\n
=\tCapacity: 10\n
=\tEmpty?    Yes\n
=Create a float Hash using the assignment operator\n
=\tSize:     0\n
=\tCapacity: 10\n
=\tEmpty?    Yes\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a hash\n
=\t2. The above plus add a few entries\n
=\t3. The above plus copy a hash table\n
=\t4. The above plus look for the entries\n
=\ta. Spell check\n
=> 
<2
#
#First create a simple hash
=Create a small integer hash\n
=\tSize:     0\n
=\tCapacity: 10\n
=\tEmpty?    Yes\n
#
#After filling it, the hash should look like this:
# h[0] -->                                                    
# h[1] --> 431 --> 991 --> 101 --> 111                        
# h[2] --> 452 --> 982                                        
# h[3] --> 213 --> 123                                        
# h[4] --> 534                                                
# h[5] --> 005                                                
# h[6] --> 626                                                
# h[7] -->                                                    
# h[8] --> 408                                                
# h[9] -->                     
=Fill with 12 values\n
=\tSize:     12\n
=\tCapacity: 10\n
=\tEmpty?    No\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a hash\n
=\t2. The above plus add a few entries\n
=\t3. The above plus copy a hash table\n
=\t4. The above plus look for the entries\n
=\ta. Spell check\n
=> 
<3
#
#Create a hash of 25 buckets, each one will have exactly 4 elements in it.
=A hash of 25 buckets\n
=\tEmpty?    no\n
=\tSize:     100\n
=\tCapacity: 25\n
#
#Using the copy constructor, the new hash should have
#the same number of buckets as h1
=Copy the hash into another\n
=\tEmpty?    no\n
=\tSize:     100\n
=\tCapacity: 25\n
#
#Create an empty hash of 5 buckets
=Create a hash of 5 buckets\n
=\tEmpty?    yes\n
=\tSize:     0\n
=\tCapacity: 5\n
#
#It should be possible to copy the contents of one hash into another,
#even when the two hashes have a different number of buckets. 
#In this case, we will go from 25 buckets with 4 elements in each bucket,
#to 5 buckets with 20 elements in each 
=Copy the large hash of 25 buckets into the small one of 5\n
=\tEmpty?    no\n
=\tSize:     100\n
=\tCapacity: 25\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a hash\n
=\t2. The above plus add a few entries\n
=\t3. The above plus copy a hash table\n
=\t4. The above plus look for the entries\n
=\ta. Spell check\n
=> 
<4
#
#Create a hash of 10 floating point numbers with a range of 0 through 100
=Test adding and querying numbers (0.0 - 100.0) from the hash:\n
=  +5.5   Put 5.5 into the hash\n
=  ?5.5   Determine if 5.5 is in the hash\n
=  !      Display the size and capacity of the hash\n
=  #      Quit\n
=> 
<+5.5
=> 
<+55.0
=> 
<+7.2
# [0] -> 5.5 -> 7.2
# [1]
# [2]
# [3]
# [4]
# [5] -> 55.0
# [6]
# [7]
# [8]
# [9]
=> 
<!
=\tSize:     3\n
=\tCapacity: 10\n
=\tEmpty?    No\n
=> 
<+8.3
# [0] -> 5.5 -> 7.2 -> 8.3
# [1]
# [2]
# [3]
# [4]
# [5] -> 55.0
# [6]
# [7]
# [8]
# [9]
=> 
<!
=\tSize:     4\n
=\tCapacity: 10\n
=\tEmpty?    No\n
=> 
<?5.5
=\tFound!\n
=> 
<?5.4
=\tNot found.\n
=> 
<+14.8
=> 
<+25.5
=> 
<+83.6
=> 
<+99.9
=> 
<+0.1
# [0] -> 5.5 -> 7.2 -> 8.3 -> 0.1
# [1] -> 14.8
# [2] -> 25.5
# [3]
# [4]
# [5] -> 55.0
# [6]
# [7]
# [8] -> 83.6
# [9] -> 99.9
=> 
<!
=\tSize:     9\n
=\tCapacity: 10\n
=\tEmpty?    No\n
=> 
<? 99.9
=\tFound!\n
=> 
<#
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a hash\n
=\t2. The above plus add a few entries\n
=\t3. The above plus copy a hash table\n
=\t4. The above plus look for the entries\n
=\ta. Spell check\n
=> 
<a
=What file do you want to check? 
</home/cs235/week12/nephi.txt
=Misspelled: Nephi, yea\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a hash\n
=\t2. The above plus add a few entries\n
=\t3. The above plus copy a hash table\n
=\t4. The above plus look for the entries\n
=\ta. Spell check\n
=> 
<a
=What file do you want to check? 
</home/cs235/week12/twoCities.txt
=File contains no spelling errors\n
-

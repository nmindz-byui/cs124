+
=Select the test you want to run:\n
=\t1. Just create and destroy a Vector.\n
=\t2. The above plus fill the Vector.\n
=\t3. The above plus iterate through the Vector.\n
=\t4. The above plus copy the Vector.\n
=\ta. The extra credit test: constant and reverse iterators.\n
=> 
<1
#Create, destroy, and copy a Vector
=Create a bool vector using default constructor\n
=\tSize:     0\n
=\tCapacity: 0\n
=\tEmpty?    Yes\n
=Create a double vector using the non-default constructor\n
=\tSize:     10\n
=\tCapacity: 10\n
=\tEmpty?    No\n
=Create a double vector using the copy constructor\n
=\tSize:     10\n
>\tCapacity: 10\n
=\tEmpty?    No\n
=Copy a double vector using the assignment operator\n
=\tSize:     10\n
>\tCapacity: 10\n
=\tEmpty?    No\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Vector.\n
=\t2. The above plus fill the Vector.\n
=\t3. The above plus iterate through the Vector.\n
=\t4. The above plus copy the Vector.\n
=\ta. The extra credit test: constant and reverse iterators.\n
=> 
<2
#Fill the vector with push_back and access items with the [] operator
=Enter numbers, type 0 when done\n
=\t{ } > 
<2
=\t{ 2 } > 
<4
=\t{ 2 4 } > 
<6
=\t{ 2 4 6 } > 
<8
=\t{ 2 4 6 8 } > 
<0
=\tSize:     4\n
=\tCapacity: 4\n
=\tEmpty?    No\n
=First vector deleted\n
=Insert user-provided characters in the vector\n
=Enter characters, type 'q' when done\n
=\t{ } > 
<a
=\t{ a } > 
<b
=\t{ a b } > 
<c
=\t{ a b c } > 
<d
=\t{ a b c d } > 
<e
=\t{ a b c d e } > 
<f
=\t{ a b c d e f } > 
<q
=\tSize:  6\n
=\tNow we will clear the contents\n
=\tSize:     0\n
=\tCapacity: 8\n
=\tEmpty?    Yes\n
=\tSecond vector deleted\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Vector.\n
=\t2. The above plus fill the Vector.\n
=\t3. The above plus iterate through the Vector.\n
=\t4. The above plus copy the Vector.\n
=\ta. The extra credit test: constant and reverse iterators.\n
=> 
<3
#This part is a repeat of Test2
=Enter text, type "quit" when done\n
=\t{ } > 
<CS124
=\t{ CS124 } > 
<CS165
=\t{ CS124 CS165 } > 
<Data-Structures
=\t{ CS124 CS165 Data-Structures } > 
<CS246
=\t{ CS124 CS165 Data-Structures CS246 } > 
<CS364
=\t{ CS124 CS165 Data-Structures CS246 CS364 } > 
<CS499
=\t{ CS124 CS165 Data-Structures CS246 CS364 CS499 } > 
<quit
#Here we are using an iterator to go through the list
=Which item would you like to look up?\n
=  0\tCS124\n
=  1\tCS165\n
=  2\tData-Structures\n
=  3\tCS246\n
=  4\tCS364\n
=  5\tCS499\n
=\t> 
<2
#Here we are using the [] operator to get an existing value
=The old value is "Data-Structures". What is the new value?\n
#Here we are using the [] operator to set a new value
=\t> 
<CS235
=\t{ CS124 CS165 CS235 CS246 CS364 CS499 }\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Vector.\n
=\t2. The above plus fill the Vector.\n
=\t3. The above plus iterate through the Vector.\n
=\t4. The above plus copy the Vector.\n
=\ta. The extra credit test: constant and reverse iterators.\n
=> 
<4
#Create a vector of floats with the default constructor.\n
=Enter numbers, type 0.0 when done\n
=\t> 
<1.2
=\t> 
<2.3
=\t> 
<3.4
=\t> 
<4.5
=\t> 
<5.6
=\t> 
<6.7
=\t> 
<7.8
=\t> 
<8.9
=\t> 
<9.0
=\t> 
<0.0
=Copy vTemp into v1\n
#Both should be the same numbers because we are making a copy
=vTemp = { 1.2 2.3 3.4 4.5 5.6 6.7 7.8 8.9 9.0 }\n
=v1    = { 1.2 2.3 3.4 4.5 5.6 6.7 7.8 8.9 9.0 }\n
=Now vTemp is overwritten with -1.0\n
#vTemp should be all 0.1 now while v should be the same
=vTemp = { 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 }\n
=v1    = { 1.2 2.3 3.4 4.5 5.6 6.7 7.8 8.9 9.0 }\n
#Copy the contents of the Vector into a new Vector\n
=v1 is set to 0.2 while v2 should be the same\n
=v1    = { 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 0.2 }\n
=v2    = { 1.2 2.3 3.4 4.5 5.6 6.7 7.8 8.9 9.0 }\n
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Vector.\n
=\t2. The above plus fill the Vector.\n
=\t3. The above plus iterate through the Vector.\n
=\t4. The above plus copy the Vector.\n
=\ta. The extra credit test: constant and reverse iterators.\n
=> 
<a
=Create a vector of int with the default constructor.\n
=\tEnter four integers\n
=\t> 
<1
=\t> 
<4
=\t> 
<9
=\t> 
<16
=Move through the vector backwards using a non-constant iterator\n
=\t16\n
=\t9\n
=\t4\n
=\t1\n
=Move through the vector forwards with a constant iterator\n
=\t1\n
=\t4\n
=\t9\n
=\t16\n
=Move through the vector backwards with a constant iterator\n
=\t16\n
=\t9\n
=\t4\n
=\t1\n
=Extra Credit complete\n
-

+
=Select the test you want to run:\n
=\t1. Just create and destroy a Map\n
=\t2. The above plus add a few entries\n
=\t3. The above plus display the contents of a Map\n
=\t4. The above plus retrieve entries from the Map\n
=\ta. Count word frequency\n
=> 
<1
#Create, destroy, and copy a Map
=Create a bool-int Map using default constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Create a double-char Map dynamically allocated\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Create a double-char Map using the copy constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Copy a double-char Map using the assignment operator\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Map\n
=\t2. The above plus add a few entries\n
=\t3. The above plus display the contents of a Map\n
=\t4. The above plus retrieve entries from the Map\n
=\ta. Count word frequency\n
=> 
<2
#
#A simple map that is empty
=Create an integer-string Map\n
=\tEmpty? yes\n
=\tCount: 0\n
#
#Fill the map with the [] operator. The tree is:
#               8
#          +----+----+
#          4         12
#       +--+--+   +--+--+
#       2     6   9     13
#     +-+   +-+   +-+
#     0     5       11
#
=Fill with 10 values\n
=\tEmpty? no\n
=\tCount: 10\n
#
#Now the original should be clear but the copy should remain
=Empty the contents\n
=\tEmpty? yes\n
=\tCount: 0\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Map\n
=\t2. The above plus add a few entries\n
=\t3. The above plus display the contents of a Map\n
=\t4. The above plus retrieve entries from the Map\n
=\ta. Count word frequency\n
=> 
<3
#
#This is just a repeat of Test 2
=Create an empty bool-bool Map\n
=\tEmpty?    yes\n
=\tCount:    0\n
=\tContents: {  }\n
#
#Test filling the map with the [] operator.
#Also test the forward iterator
=Create a string-integer map that is filled with:  f c i b e g j a d h\n
=\tEmpty?    no\n
=\tCount:    10\n
=\tContents: {  1  2  3  4  5  6  7  8  9  10  }\n
#
#Test to make sure the assignmetn operator and the clear() method works
=Copy the map and destroy the original\n
=\tEmpty?    no\n
=\tCount:    10\n
=\tContents: {  1  2  3  4  5  6  7  8  9  10  }\n
#
#Finally test find() and backwards incrementing
=The left side of the tree:\n
=\t(f, 6)\n
=\t(e, 5)\n
=\t(d, 4)\n
=\t(c, 3)\n
=\t(b, 2)\n
=\t(a, 1)\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Map\n
=\t2. The above plus add a few entries\n
=\t3. The above plus display the contents of a Map\n
=\t4. The above plus retrieve entries from the Map\n
=\ta. Count word frequency\n
=> 
<4
#
#Test building a map using the [] operator
=Please enter a letter word pair. Enter ! for the letter when finished.\n
=\t> 
<d Dog
=\t> 
<c Cat
=\t> 
<f Fish
=\t> 
<b Bird
=\t> 
<m Mouse
=\t> 
<! !
#
#Verify that the map is build correctly. Note that since we are using the >
#to build the BST in the Map, and since we are iterating through the Map
#using in-order traversal, the tree will appear sorted
=There are 5 items in the map\n
=The contents of the map are: {  Bird  Cat  Dog  Fish  Mouse  }\n
#
#Next test that we can find items in the map
=Please enter the letter to be found. Enter ! when finished.\n
=\t> 
<f
=The letter 'f' corresponds to "Fish"\n
=\t> 
<d
=The letter 'd' corresponds to "Dog"\n
=\t> 
<b
=The letter 'b' corresponds to "Bird"\n
=\t> 
<c
=The letter 'c' corresponds to "Cat"\n
=\t> 
<m
=The letter 'm' corresponds to "Mouse"\n
=\t> 
<!
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Map\n
=\t2. The above plus add a few entries\n
=\t3. The above plus display the contents of a Map\n
=\t4. The above plus retrieve entries from the Map\n
=\ta. Count word frequency\n
=> 
<a
#
#Read the text from the file and put it in a Map much like Test 2
=What is the filename to be counted? 
</home/cs235/week10/D_C_121.txt
#
#There are 1345 words in this file, most of which have duplicates.
#This means there should be a few hundred nodes. The depth of the tree
#should be about 8. This means searching for a word should take, on average
#7 or 8 compares. That should be very fast.
=What word whose frequency is to be found. Type ! when done\n
=> 
<Nephi
=\tNephi : 0\n
=> 
<Lord
=\tLord : 6\n
=> 
<Christ
=\tChrist : 1\n
=> 
<I
=\tI : 2\n
=> 
<the
=\tthe : 79\n
#
#The next test is a shock! There is no "C++" in the D&C!!!
=> 
<C++
=\tC++ : 0\n
=> 
<!
-

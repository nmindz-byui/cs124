+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. Verify that the red-black balancing is handled correctly\n
=> 
<1
#
#This will not only check for compiling,
#but also if the size() method works with a NULL root
=Create a bool BST using the default constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
#
#The copy constructor should work with a NULL root as well
=Create a bool BST using the copy constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
#
#Finally, the assignment operator should work with a NULL root
=Copy a bool BST using the assignment operator\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. Verify that the red-black balancing is handled correctly\n
=> 
<2
#
#Test the insert() method with an empty tree and with a partially filled tree
=Create an integer Binary Search Tree\n
#
#The size() method should be able to traverse the tree
=\tSize of tree1: 10\n
#
#Test copying a filled tree using the copy constructor
=\tSize of tree2: 10\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. Verify that the red-black balancing is handled correctly\n
=> 
<3
#
#Test an iterator on an empty tree
=Empty tree\n
=\tSize:     0\n
=\tContents: {  }\n
#
#Test an iterator on a simple tree with three nodes
#          2.2
#     +-----+-----+
#    1.1         3.3
=A tree with three nodes\n
=\tFill the BST with: 2.2  1.1  3.3\n
=\tContents forward:  {  1.1  2.2  3.3  }\n
=\tContents backward: {  3.3  2.2  1.1  }\n
#
#Test an iterator on a multi-level tree that includes several
#backtracking strategies. The tree is:
#               f
#          +----+----+
#          c         i
#       +--+--+   +--+--+
#       b     e   g     j
#     +-+   +-+   +-+
#     a     d       h
=Fill the BST with: f  c  i  b  e  g  j  a  d  h  \n
=\tSize:     10\n
=\tContents: {  a  b  c  d  e  f  g  h  i  j  }\n
#
#Test deleting an non-trivial tree
=The tree after it was cleared\n
=\tSize:     0\n
=\tContents: {  }\n
#
#The assignment operator should preserve the original tree
=The tree that was copied\n
=\tSize:     10\n
=\tContents: {  a  b  c  d  e  f  g  h  i  j  }\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. Verify that the red-black balancing is handled correctly\n
=> 
<4
#
# First create a somewhat complex tree
#                       G
#          +------------+------------+
#          F                         J
#   +------+                  +------+------+
#   A                         H             O
#   +---+                     +---+     +---+---+
#       E                         I     M       P
#    +--+                            +--+--+
#    C                               K     N
#  +-+-+                             +-+
#  B   D                               L
=Fill the tree with: G F J A H N E I L O C K M B D P\n
=\tContents: {  A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  }\n
#
#Next find and remove a leaf node.
#This will involve the minimal amount of modification to the tree
#The new tree is:
#                       G
#          +------------+------------+
#          F                         J
#   +------+                  +------+------+
#   A                         H             O
#   +---+                     +---+     +---+---+
#       E                         I     M       P
#    +--+                            +--+--+
#    C                               K     N
#  +-+                               +-+
#  B                                   L
=Remove a leaf node: 'D'\n
=\tNode 'D' found\n
=\tContents: {  A  B  C  E  F  G  H  I  J  K  L  M  N  O  P  }\n
#
#A second attempt to remove 'D' should find nothing
=Attempt to remove 'D' again\n
=\tNode not found!\n
#
#Next we will remove a node that has a single child: 'E'.
#This will require C to take E's place.
#                       G
#          +------------+------------+
#          F                         J
#   +------+                  +------+------+
#   A                         H             O
#   +---+                     +---+     +---+---+
#       C                         I     M       P
#    +--+                            +--+--+
#    B                               K     N
#                                    +-+
#                                      L
=Remove a one-child node: 'E'\n
=\tNode 'E' found\n
=\tContents: {  A  B  C  F  G  H  I  J  K  L  M  N  O  P  }\n
#
#Next we will remove an item that has two children.
#This requires us to replace it with the in-order successor, a node
#guarenteed to have zero or one child. This is 'K'. The new tree is:
#                       G
#          +------------+------------+
#          F                         K
#   +------+                  +------+------+
#   A                         H             O
#   +---+                     +---+     +---+---+
#       C                         I     M       P
#    +--+                            +--+--+
#    B                               L     N
=Remove a two-child node: 'J'\n
=\tNode 'J' found\n
=\tContents: {  A  B  C  F  G  H  I  K  L  M  N  O  P  }\n
#
#Finally we will remove the root. This will require to replace 'G'
#with the in-order successor. That would be 'H', the new root!
#The new tree is:
#                       H
#          +------------+------------+
#          F                         K
#   +------+                  +------+------+
#   A                         I             O
#   +---+                               +---+---+
#       C                               M       P
#    +--+                            +--+--+
#    B                               L     N
=Remove the root: 'G'\n
=\tNode 'G' found\n
=\tContents: {  A  B  C  F  H  I  K  L  M  N  O  P  }\n
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. Verify that the red-black balancing is handled correctly\n
=> 
<a
=Create a simple Binary Search Tree\n
#
#A black root
#                60-b
=\tPass Case 1\n
#
#Two red children
#                60-b
#             +----+----+
#           50-r      70-r
=\tPass Case 2\n
#
#Add a child which should cause 50 and 70 to turn black
#                60-b
#             +----+----+
#           50-b      70-b
#         +---+
#       20-r
=\tPass Case 3\n
#
#Add a child to 20 which should cause a right rotation on 50
#                60-b
#             +----+----+
#           20-b     70-b
#         +---+---+
#       10-r    50-r
=\tPass Case 4a\n
#
#Add 90 (Case 2) followed by 95 (Case 4b) rotate left 
#                   60-b
#           +---------+---------+
#         20-b                90-b
#     +-----+-----+       +-----+-----+
#   10-r        50-r    70-r        95-r
=\tPass Case 4b\n
#
#Add 30 (Case 3 then 2) followed by 40 (Case 4c)  
#                      60-b
#             +----------+----------+
#           20-r                  90-b
#       +-----+-----+        +------+-----+
#     10-b        40-b     70-r         95-r
#               +---+---+
#             30-r    50-r
=\tPass Case 4c\n
#
#Add 85 (Case 3 then 2) followed by 80 (Case 4d)
#                        60-b
#             +------------+------------+
#           20-r                      90-r
#       +-----+-----+            +------+-----+
#     10-b        40-b         80-b         95-b
#                +--+--+      +--+--+  
#              30-r  50-r   70-r  85-r  
=\tPass Case 4d\n
=Final tree: {  10  20  30  40  50  60  70  80  85  90  95  }\n
=Test Balance complete\n
-
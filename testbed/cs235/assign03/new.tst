+
=Select the test you want to run:\n
=\t1. Just create and destroy a Queue\n
=\t2. The above plus push, pop, and top\n
=\t3. The above plus test implementation of the circular Queue\n
=\t4. Exercise the error handling\n
=\ta. Selling Stock\n
=> 
<1
#Create, destory, and copy a Queue
=Create a bool Queue using default constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Create a double Queue using the non-default constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Create a double Queue using the copy constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Copy a double Queue using the assignment operator\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Queue\n
=\t2. The above plus push, pop, and top\n
=\t3. The above plus test implementation of the circular Queue\n
=\t4. Exercise the error handling\n
=\ta. Selling Stock\n
=> 
<2
#Create a Dollars Queue with the default constructor\n
=Enter money amounts, type $0 when done\n
#Initially an empty queue
=\t{ } > 
<$1
#First resize should set the capacity to one
=\t{ $1.00 } > 
<-2
#Now the capacity should be two
=\t{ $1.00 $(2.00) } > 
<3.0
#Double again to four
=\t{ $1.00 $(2.00) $3.00 } > 
<(4.1)
=\t{ $1.00 $(2.00) $3.00 $(4.10) } > 
<$(-5.21)
#Double again to 8
=\t{ $1.00 $(2.00) $3.00 $(4.10) $(5.21) } > 
<0
=\tSize:     5\n
=\tEmpty?    No\n
#When we clear q1, then q2 and q3 should keep their data
=\tq1 = { }\n
=\tq2 = { $1.00 $(2.00) $3.00 $(4.10) $(5.21) }\n
=\tq3 = { $1.00 $(2.00) $3.00 $(4.10) $(5.21) }\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Queue\n
=\t2. The above plus push, pop, and top\n
=\t3. The above plus test implementation of the circular Queue\n
=\t4. Exercise the error handling\n
=\ta. Selling Stock\n
=> 
<3
=Create a string Queue with the default constructor\n
=\tTo add the word "dog", type +dog\n
=\tTo pop the word off the queue, type -\n
=\tTo display the state of the queue, type *\n
=\tTo quit, type !\n
#Initially the capacity should be 4 so no resizing is needed here
=\t{ } > 
<+alfa
=\t{ alfa } > 
<+bravo
=\t{ alfa bravo } > 
<+charlie
=\t{ alfa bravo charlie } > 
<+delta
=\t{ alfa bravo charlie delta } > 
<*
=Size:     4\n
=Empty?    No\n
#Next we will make room for two elements on the front
=\t{ alfa bravo charlie delta } > 
<-
=\t{ bravo charlie delta } > 
<-
=\t{ charlie delta } > 
<*
=Size:     2\n
=Empty?    No\n
#This tests the circular queue. We will add two elements which will wrap around
=\t{ charlie delta } > 
<+echo
=\t{ charlie delta echo } > 
<+foxtrot
=\t{ charlie delta echo foxtrot } > 
<*
#Now our capacity should be unchagned (there are only 3 elements on the queue),
#but we are wrapping around using the circular queue
=Size:     4\n
=Empty?    No\n
#Next we will empty the queue
=\t{ charlie delta echo foxtrot } > 
<-
=\t{ delta echo foxtrot } > 
<-
=\t{ echo foxtrot } > 
<-
=\t{ foxtrot } > 
<-
=\t{ } > 
<*
#The capacity should remain at 4 even though the size is zero
=Size:     0\n
=Empty?    Yes\n
#From here we will fill the queue with four elements
=\t{ } > 
<+hotel
=\t{ hotel } > 
<+india
=\t{ hotel india } > 
<+juliett
=\t{ hotel india juliett } > 
<+kilo
=\t{ hotel india juliett kilo } > 
<-
#Now we will get into the wrapped condition of the circular queue
=\t{ india juliett kilo } > 
<+lima
=\t{ india juliett kilo lima } > 
<*
=Size:     4\n
=Empty?    No\n
#Now we will add a fifth element. This requires a resize. 
#Our resize code must know how to deal with a wrapping condition
=\t{ india juliett kilo lima } > 
<+mike
=\t{ india juliett kilo lima mike } > 
<*
=Size:     5\n
=Empty?    No\n
=\t{ india juliett kilo lima mike } > 
<-
=\t{ juliett kilo lima mike } > 
<-
=\t{ kilo lima mike } > 
<*
=Size:     3\n
=Empty?    No\n
=\t{ kilo lima mike } > 
<!
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Queue\n
=\t2. The above plus push, pop, and top\n
=\t3. The above plus test implementation of the circular Queue\n
=\t4. Exercise the error handling\n
=\ta. Selling Stock\n
=> 
<4
#
#Test front() from an empty queue
=\tQueue::front() error message correctly caught.\n
=\t"ERROR: attempting to access an element in an empty queue"\n
#
#Test back() from an empty queue
=\tQueue::back() error message correctly caught.\n
=\t"ERROR: attempting to access an element in an empty queue"\n
#
#Test pop() from an empty queue
=\tCorrect! When we pop() with an empty queue, nothing bad happens.\n
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Queue\n
=\t2. The above plus push, pop, and top\n
=\t3. The above plus test implementation of the circular Queue\n
=\t4. Exercise the error handling\n
=\ta. Selling Stock\n
=> 
<a
=This program will allow you to buy and sell stocks. The actions are:\n
=  buy 200 $1.57   - Buy 200 shares at $1.57\n
=  sell 150 $2.15  - Sell 150 shares at $2.15\n
=  display         - Display your current stock portfolio\n
=  quit            - Display a final report and quit the program\n
#
#Buy two batches
=> 
<buy 100 $2.00
=> 
<buy 400 $3.00
=> 
<display
=Currently held:\n
=\tBought 100 shares at $2.00\n
=\tBought 400 shares at $3.00\n
=Proceeds: $0.00\n
#
#Sell 150 which will require taking 100 from the first batch
#and 50 from the second
=> 
<sell 150 5.5
=> 
<display
=Currently held:\n
=\tBought 350 shares at $3.00\n
=Sell History:\n
=\tSold 100 shares at $5.50 for a profit of $350.00\n
=\tSold 50 shares at $5.50 for a profit of $125.00\n
=Proceeds: $475.00\n
#
#Next we will sell 150 which should be completely within the remaining batch
=> 
<sell 150 $5.50
=> 
<display
=Currently held:\n
=\tBought 200 shares at $3.00\n
=Sell History:\n
=\tSold 100 shares at $5.50 for a profit of $350.00\n
=\tSold 50 shares at $5.50 for a profit of $125.00\n
=\tSold 150 shares at $5.50 for a profit of $375.00\n
=Proceeds: $850.00\n
#
#Buy two more batches
=> 
<buy 300 $1.50
=> 
<buy 120 $5.25
=> 
<display
=Currently held:\n
=\tBought 200 shares at $3.00\n
=\tBought 300 shares at $1.50\n
=\tBought 120 shares at $5.25\n
=Sell History:\n
=\tSold 100 shares at $5.50 for a profit of $350.00\n
=\tSold 50 shares at $5.50 for a profit of $125.00\n
=\tSold 150 shares at $5.50 for a profit of $375.00\n
=Proceeds: $850.00\n
=> 
<quit
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Queue\n
=\t2. The above plus push, pop, and top\n
=\t3. The above plus test implementation of the circular Queue\n
=\t4. Exercise the error handling\n
=\ta. Selling Stock\n
=> 
<a
=This program will allow you to buy and sell stocks. The actions are:\n
=  buy 200 $1.57   - Buy 200 shares at $1.57\n
=  sell 150 $2.15  - Sell 150 shares at $2.15\n
=  display         - Display your current stock portfolio\n
=  quit            - Display a final report and quit the program\n
#
#Buy two batches for a total of 512 shares
=> 
<buy 256 2.56
=> 
<buy 256 3.44
#
#Sell 400. That is 256 from first batch and 144 from the second.
#This leaves us with 112 in the remaining batch
=> 
<sell 400 5.00
#
#Buy 145 more. Now we have 2 batches: 112 in the first and 145 in the last
=> 
<buy 145 5.10
#
#Sell 125. This will clear out the first batch and require an additional
#13 more from the last. It will then leave 132 in the last.
=> 
<sell 125 8.00
=> 
<display
=Currently held:\n
=\tBought 132 shares at $5.10\n
=Sell History:\n
=\tSold 256 shares at $5.00 for a profit of $624.64\n
=\tSold 144 shares at $5.00 for a profit of $224.64\n
=\tSold 112 shares at $8.00 for a profit of $510.72\n
=\tSold 13 shares at $8.00 for a profit of $37.70\n
=Proceeds: $1397.70\n
=> 
<quit
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Queue\n
=\t2. The above plus push, pop, and top\n
=\t3. The above plus test implementation of the circular Queue\n
=\t4. Exercise the error handling\n
=\ta. Selling Stock\n
=> 
<a
=This program will allow you to buy and sell stocks. The actions are:\n
=  buy 200 $1.57   - Buy 200 shares at $1.57\n
=  sell 150 $2.15  - Sell 150 shares at $2.15\n
=  display         - Display your current stock portfolio\n
=  quit            - Display a final report and quit the program\n
#
#We will buy 200 and sell 150 over three instances. This leave us with 50
=> 
<buy 200 $1.00
=> 
<sell 50 $1.50
=> 
<sell 50 $1.75
=> 
<sell 50 $2.00
#
#After this, we will have 2 batchs: 50 in the first and 100 in the second
=> 
<buy 100 $4.00
#
#The first will now be gone leaving us only with the second
=> 
<sell 50 $4.10
#
#Now the last batch will be sold off
=> 
<sell 50 $1.00
=> 
<sell 50 4.01
=> 
<display
=Sell History:\n
=\tSold 50 shares at $1.50 for a profit of $25.00\n
=\tSold 50 shares at $1.75 for a profit of $37.50\n
=\tSold 50 shares at $2.00 for a profit of $50.00\n
=\tSold 50 shares at $4.10 for a profit of $155.00\n
=\tSold 50 shares at $1.00 for a profit of $(150.00)\n
=\tSold 50 shares at $4.01 for a profit of $0.50\n
=Proceeds: $118.00\n
=> 
<quit
-


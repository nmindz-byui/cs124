+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. To test the binarySort() function\n
=> 
<1
#
#This will not only check for compiling,
#but also if the size() method works with a NULL root
=Create a bool BST using the default constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
#
#The copy constructor should work with a NULL root as well
=Create a bool BST using the copy constructor\n
=\tSize:     0\n
=\tEmpty?    Yes\n
#
#Finally, the assignment operator should work with a NULL root
=Copy a bool BST using the assignment operator\n
=\tSize:     0\n
=\tEmpty?    Yes\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. To test the binarySort() function\n
=> 
<2
#
#Test the insert() method with an empty tree and with a partially filled tree
=Create an integer Binary Search Tree\n
#
#The size() method should be able to traverse the tree
=\tSize of tree1: 10\n
#
#Test copying a filled tree using the copy constructor
=\tSize of tree2: 10\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. To test the binarySort() function\n
=> 
<3
#
#Test an iterator on an empty tree
=Empty tree\n
=\tSize:     0\n
=\tContents: {  }\n
#
#Test an iterator on a simple tree with three nodes
#          2.2
#     +-----+-----+
#    1.1         3.3
=A tree with three nodes\n
=\tFill the BST with: 2.2  1.1  3.3\n
=\tContents forward:  {  1.1  2.2  3.3  }\n
=\tContents backward: {  3.3  2.2  1.1  }\n
#
#Test an iterator on a multi-level tree that includes several
#backtracking strategies. The tree is:
#               f
#          +----+----+
#          c         i
#       +--+--+   +--+--+
#       b     e   g     j
#     +-+   +-+   +-+
#     a     d       h
=Fill the BST with: f  c  i  b  e  g  j  a  d  h  \n
=\tSize:     10\n
=\tContents: {  a  b  c  d  e  f  g  h  i  j  }\n
#
#Test deleting an non-trivial tree
=The tree after it was cleared\n
=\tSize:     0\n
=\tContents: {  }\n
#
#The assignment operator should preserve the original tree
=The tree that was copied\n
=\tSize:     10\n
=\tContents: {  a  b  c  d  e  f  g  h  i  j  }\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. To test the binarySort() function\n
=> 
<4
#
# First create a somewhat complex tree
#                       G
#          +------------+------------+
#          F                         J
#   +------+                  +------+------+
#   A                         H             O
#   +---+                     +---+     +---+---+
#       E                         I     M       P
#    +--+                            +--+--+
#    C                               K     N
#  +-+-+                             +-+
#  B   D                               L
=Fill the tree with: G F A E C B D J H I O M K L N P\n
=\tContents: {  A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  }\n
#
#Next find and remove a leaf node.
#This will involve the minimal amount of modification to the tree
#The new tree is:
#                       G
#          +------------+------------+
#          F                         J
#   +------+                  +------+------+
#   A                         H             O
#   +---+                     +---+     +---+---+
#       E                         I     M       P
#    +--+                            +--+--+
#    C                               K     N
#  +-+                               +-+
#  B                                   L
=Remove a leaf node: 'D'\n
=\tNode 'D' found\n
=\tContents: {  A  B  C  E  F  G  H  I  J  K  L  M  N  O  P  }\n
#
#A second attempt to remove 'D' should find nothing
=Attempt to remove 'D' again\n
=\tNode not found!\n
#
#Next we will remove a node that has a single child: 'E'.
#This will require C to take E's place.
#                       G
#          +------------+------------+
#          F                         J
#   +------+                  +------+------+
#   A                         H             O
#   +---+                     +---+     +---+---+
#       C                         I     M       P
#    +--+                            +--+--+
#    B                               K     N
#                                    +-+
#                                      L
=Remove a one-child node: 'E'\n
=\tNode 'E' found\n
=\tContents: {  A  B  C  F  G  H  I  J  K  L  M  N  O  P  }\n
#
#Next we will remove an item that has two children.
#This requires us to replace it with the in-order successor, a node
#guarenteed to have zero or one child. This is 'K'. The new tree is:
#                       G
#          +------------+------------+
#          F                         K
#   +------+                  +------+------+
#   A                         H             O
#   +---+                     +---+     +---+---+
#       C                         I     M       P
#    +--+                            +--+--+
#    B                               L     N
=Remove a two-child node: 'J'\n
=\tNode 'J' found\n
=\tContents: {  A  B  C  F  G  H  I  K  L  M  N  O  P  }\n
#
#Finally we will remove the root. This will require to replace 'G'
#with the in-order successor. That would be 'H', the new root!
#The new tree is:
#                       H
#          +------------+------------+
#          F                         K
#   +------+                  +------+------+
#   A                         I             O
#   +---+                               +---+---+
#       C                               M       P
#    +--+                            +--+--+
#    B                               L     N
=Remove the root: 'G'\n
=\tNode 'G' found\n
=\tContents: {  A  B  C  F  H  I  K  L  M  N  O  P  }\n
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BST\n
=\t2. The above plus add a few nodes\n
=\t3. The above plus display the contents of a BST\n
=\t4. The above plus find and delete nodes from a BST\n
=\ta. To test the binarySort() function\n
=> 
<a
=Four string objects\n
=\tBefore: Beta, Alpha, Epsilon, Delta\n
=\tAfter:  Alpha, Beta, Delta, Epsilon\n
=Twenty one-decimal numbers\n
=\tBefore:\t5.1, 2.4, 8.2, 2.7, 4.7, 1.8, 9.9, 3.4, 5.0, 1.0,\n
=\t\t4.4, 3.4, 8.3, 2.9, 1.7, 7.9, 9.5, 9.3, 3.6, 2.9\n
=\tAfter:\t1.0, 1.7, 1.8, 2.4, 2.7, 2.9, 2.9, 3.4, 3.4, 3.6,\n
=\t\t4.4, 4.7, 5.0, 5.1, 7.9, 8.2, 8.3, 9.3, 9.5, 9.9\n
=One hundred three-digit numbers\n
=\tBefore:\t889, 192, 528, 675, 154, 746, 562, 482, 448, 842,\n
=\t\t929, 330, 615, 225, 785, 577, 606, 426, 311, 867,\n
=\t\t773, 775, 190, 414, 155, 771, 499, 337, 298, 242,\n
=\t\t656, 188, 334, 184, 815, 388, 831, 429, 823, 331,\n
=\t\t323, 752, 613, 838, 877, 398, 415, 535, 776, 679,\n
=\t\t455, 602, 454, 545, 916, 561, 369, 467, 851, 567,\n
=\t\t609, 507, 707, 844, 643, 522, 284, 526, 903, 107,\n
=\t\t809, 227, 759, 474, 965, 689, 825, 433, 224, 601,\n
=\t\t112, 631, 255, 518, 177, 224, 131, 446, 591, 882,\n
=\t\t913, 201, 441, 673, 997, 137, 195, 281, 563, 151\n
=\tAfter:\t107, 112, 131, 137, 151, 154, 155, 177, 184, 188,\n
=\t\t190, 192, 195, 201, 224, 224, 225, 227, 242, 255,\n
=\t\t281, 284, 298, 311, 323, 330, 331, 334, 337, 369,\n
=\t\t388, 398, 414, 415, 426, 429, 433, 441, 446, 448,\n
=\t\t454, 455, 467, 474, 482, 499, 507, 518, 522, 526,\n
=\t\t528, 535, 545, 561, 562, 563, 567, 577, 591, 601,\n
=\t\t602, 606, 609, 613, 615, 631, 643, 656, 673, 675,\n
=\t\t679, 689, 707, 746, 752, 759, 771, 773, 775, 776,\n
=\t\t785, 809, 815, 823, 825, 831, 838, 842, 844, 851,\n
=\t\t867, 877, 882, 889, 903, 913, 916, 929, 965, 997\n
-
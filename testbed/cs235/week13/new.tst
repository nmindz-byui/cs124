+
=Select the test you want to run:\n
=\t1. Just create and destroy a graph\n
=\t2. The above plus add a few entries\n
=\t3. Determine if two verticies are connected\n
=\t4. Find all the verticies connected to a given vertex\n
=\ta. Maze\n
=> 
<1
#
#Simple test of creating a graph
=Create a graph of 10 verticies\n
=\tSize: 10\n
=Create a graph of 20 verticies\n
=\tSize: 20\n
=Create a graph using the copy constructor\n
=\tSize: 20\n
=Copy a graph using the assignment operator\n
=\tSize: 20\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a graph\n
=\t2. The above plus add a few entries\n
=\t3. Determine if two verticies are connected\n
=\t4. Find all the verticies connected to a given vertex\n
=\ta. Maze\n
=> 
<2
#
#Test adding edges to a graph using two verticies
=Create a graph of 5 verticies\n
=\tA --> B\n
=\tB --> C\n
=\tC --> A\n
#
#Test adding edgest to a graph using a set of verticies
=\tD --> {A, B, C, D}\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a graph\n
=\t2. The above plus add a few entries\n
=\t3. Determine if two verticies are connected\n
=\t4. Find all the verticies connected to a given vertex\n
=\ta. Maze\n
=> 
<3
#
#Read a 5x5 graph:
#    a1 a2 a3 a4 a5 b1 b2 b3 b4 b5 c1 c2 c3 c4 c5 d1 d2 d3 d4 d5 e1 e2 e3 e4 e5
# a1  
# a2 y
# a3    y
# a4       y
# a5          y
# b1                   y
# b2    y
# b3       y
# b4          y
# b5                         y
# c1                                  y
# c2                                     y
# c3                      y
# c4                                           y
# c5                            y
# d1                               y
# d2                                                    y
# d3                                                                   y
# d4                                        y
# d5                                                                         y
# e1                                              y
# e2                                                             y
# e3                                                                y
# e4                                                       y
# e5                                                                      y
#    a1 a2 a3 a4 a5 b1 b2 b3 b4 b5 c1 c2 c3 c4 c5 d1 d2 d3 d4 d5 e1 e2 e3 e4 e5
=Determine if a given edge exists in the graph\n
=> 
<a1 a2
=\ta1 - a2 is an edge\n
=> 
<a2 a1
=\ta2 - a1 is NOT an edge\n
=> 
<b3 c3
=\tb3 - c3 is an edge\n
=> 
<c3 b3
=\tc3 - b3 is NOT an edge\n
=> 
<a1 e4
=\ta1 - e4 is NOT an edge\n
=> 
<quit
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a graph\n
=\t2. The above plus add a few entries\n
=\t3. Determine if two verticies are connected\n
=\t4. Find all the verticies connected to a given vertex\n
=\ta. Maze\n
=> 
<4
=For the given class, the prerequisites will be listed:\n
=> 
<CS165
=\tCS124\n
=> 
<CS235
=\tCS165\n
=> 
<CS364
=\tCS246\n
=\tCS308\n
=> 
<CS306
=\tCS235\n
=\tCS237\n
=> 
<quit
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a graph\n
=\t2. The above plus add a few entries\n
=\t3. Determine if two verticies are connected\n
=\t4. Find all the verticies connected to a given vertex\n
=\ta. Maze\n
=> 
<a
=What is the filename? 
</home/cs235/week13/maze5x5.txt
=+  +--+--+--+--+\n
=|  |  |        |\n
=+  +  +  +--+  +\n
=|     |  |  |  |\n
=+  +--+  +  +  +\n
=|        |     |\n
=+  +--+--+--+--+\n
=|     |        |\n
=+  +  +  +--+  +\n
=|  |     |     |\n
=+--+--+--+--+  +\n
=Press any key to solve the maze.\n
<\n
=+  +--+--+--+--+\n
=|##|  |        |\n
=+  +  +  +--+  +\n
=|##   |  |  |  |\n
=+  +--+  +  +  +\n
=|##      |     |\n
=+  +--+--+--+--+\n
=|## ##|## ## ##|\n
=+  +  +  +--+  +\n
=|  |## ##|   ##|\n
=+--+--+--+--+  +\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a graph\n
=\t2. The above plus add a few entries\n
=\t3. Determine if two verticies are connected\n
=\t4. Find all the verticies connected to a given vertex\n
=\ta. Maze\n
=> 
<a
=What is the filename? 
</home/cs235/week13/maze10x10.txt
=+  +--+--+--+--+--+--+--+--+--+\n
=|  |        |        |        |\n
=+  +  +--+  +  +  +  +  +--+--+\n
=|  |     |  |  |  |     |     |\n
=+  +  +  +  +  +  +--+--+  +  +\n
=|     |  |     |        |  |  |\n
=+--+--+  +  +--+  +--+--+  +--+\n
=|     |  |     |  |     |     |\n
=+  +  +  +  +  +  +  +  +--+  +\n
=|  |     |  |  |     |     |  |\n
=+  +  +--+  +  +  +--+  +--+  +\n
=|  |     |  |  |     |        |\n
=+  +  +--+  +  +--+--+--+--+--+\n
=|  |  |     |                 |\n
=+  +--+  +--+--+  +--+  +--+--+\n
=|     |  |     |     |        |\n
=+  +  +--+  +  +--+--+  +  +--+\n
=|  |     |  |  |        |  |  |\n
=+  +  +  +--+  +  +  +--+  +  +\n
=|  |  |        |  |     |     |\n
=+--+--+--+--+--+--+--+--+--+  +\n
=Press any key to solve the maze.\n
<\n
=+  +--+--+--+--+--+--+--+--+--+\n
=|##|## ## ##|        |        |\n
=+  +  +--+  +  +  +  +  +--+--+\n
=|##|##   |##|  |  |     |     |\n
=+  +  +  +  +  +  +--+--+  +  +\n
=|## ##|  |##   |        |  |  |\n
=+--+--+  +  +--+  +--+--+  +--+\n
=|     |  |## ##|  |     |     |\n
=+  +  +  +  +  +  +  +  +--+  +\n
=|  |     |  |##|     |     |  |\n
=+  +  +--+  +  +  +--+  +--+  +\n
=|  |     |  |##|     |        |\n
=+  +  +--+  +  +--+--+--+--+--+\n
=|  |  |     |## ## ## ##      |\n
=+  +--+  +--+--+  +--+  +--+--+\n
=|     |  |     |     |## ##   |\n
=+  +  +--+  +  +--+--+  +  +--+\n
=|  |     |  |  |        |##|  |\n
=+  +  +  +--+  +  +  +--+  +  +\n
=|  |  |        |  |     |## ##|\n
=+--+--+--+--+--+--+--+--+--+  +\n
-

+
=Select the test you want to run:\n
=\t1. Just create and destroy a linked list\n
=\t2. The above plus insert items onto the linked list\n
=\t3. The above plus loop through the linked list\n
=\t4. The above plus find items in the linked list\n
=\t5. The above plus test removing an element\n
=\ta. Insertion Sort\n
=> 
<1
#
#Using the non-default constructor to initialize a Node
=Create a bool linked list\n
=\tn1->data:  true\n
=\tn1->pNext: address\n
=\tn1->pPrev: NULL\n
=\tn1->pNext->data:  false\n
=\tn1->pNext->pNext: NULL\n
=\tn1->pNext->pPrev: address\n
#
#Same as the previous test: using the non-default constructor
=Create a double linked list and add two elements: 1.1 2.2\n
=\tn2->data:  1.1\n
=\tn2->pNext: address\n
=\tn2->pPrev: NULL\n
=\tn2->pNext->data:  2.2\n
=\tn2->pNext->pNext: NULL\n
=\tn2->pNext->pPrev: address\n
#
#Use the copy() command. Should be same as the previous test
=Copy the double linked list\n
=\tn3->data:  1.1\n
=\tn3->pNext: address\n
=\tn3->pPrev: NULL\n
=\tn3->pNext->data:  2.2\n
=\tn3->pNext->pNext: NULL\n
=\tn3->pNext->pPrev: address\n
=Destroying the second and third linked list\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a linked list\n
=\t2. The above plus insert items onto the linked list\n
=\t3. The above plus loop through the linked list\n
=\t4. The above plus find items in the linked list\n
=\t5. The above plus test removing an element\n
=\ta. Insertion Sort\n
=> 
<2
#
#Special case of starting a list from scratch
=Create an integer linked list and put the number 10 on top\n
=\tn->data:  20\n
=\tn->pNext: NULL\n
=\tn->pPrev: NULL\n
#
#Special case where we are adding onto the end of the list
=Add the number 10 to the front\n
=\t{ 10, 20 }\n
#
#Special case where we are adding onto the end of the list
#With insert(25), this is just putting an item on the end as we did before
=Add 25 to the back\n
=\t{ 10, 20, 25 }\n
#
#With insert(15), we are putting an item in the middle. Don't drop the rest!
=Add 15 to the middle\n
=\t{ 10, 15, 20, 25 }\n
#
#Special case of changing the head of the list.
#This requires the third parameter of the insert() function
=Add 5 to the head\n
=\t{ 5, 10, 15, 20, 25 }\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a linked list\n
=\t2. The above plus insert items onto the linked list\n
=\t3. The above plus loop through the linked list\n
=\t4. The above plus find items in the linked list\n
=\t5. The above plus test removing an element\n
=\ta. Insertion Sort\n
=> 
<3
#
#Just a bunch of insert()s exactly like before, 
#but now we will use the insertion operator to display the list
=Create a char linked list: { a, b, c, d, e, f }\n
=\t{ a, b, c, d, e, f }\n
#
#Exercise the freeData() function
=Empty the list\n
=\tThe list is empty\n
#
#Do another full exactly like before
=Fill the list now with: { Z, Y, X, W, V, U }\n
=\t{ Z, Y, X, W, V, U }\n
#
#Anohter copy and another delete
=Copy of the list:\n
=\t{ Z, Y, X, W, V, U }\n
=Empty the list\n
=\tThe list is empty\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a linked list\n
=\t2. The above plus insert items onto the linked list\n
=\t3. The above plus loop through the linked list\n
=\t4. The above plus find items in the linked list\n
=\t5. The above plus test removing an element\n
=\ta. Insertion Sort\n
=> 
<4
=Create a string linked list\n
=Instructions:\n
=\t+ dog 4  inserts dog onto the 4th slot, 0 begin the head\n
=\t? cat    determines if cat is in the list\n
=\t*        clear the list\n
=\t!        quit\n
#Add an item to an empty list
={  } > 
<+ three 0
#Add items to the end - done twice 
={ three } > 
<+ five 1
={ three, five } > 
<+ seven 2
#Add an item to the beginning of an existing list
={ three, five, seven } > 
<+ zero 0
#Add items to the middle of the list
={ zero, three, five, seven } > 
<+ four 2
={ zero, three, four, five, seven } > 
<+ six 4
={ zero, three, four, five, six, seven } > 
<+ two 1
={ zero, two, three, four, five, six, seven } > 
<+ one 1
#
#Look for items not found
={ zero, one, two, three, four, five, six, seven } > 
<? Four
=\tThe text was not found\n
={ zero, one, two, three, four, five, six, seven } > 
<? eight
=\tThe text was not found\n
#
#Look for items that are found
={ zero, one, two, three, four, five, six, seven } > 
<? four
=\tThe text was found\n
={ zero, one, two, three, four, five, six, seven } > 
<? seven
=\tThe text was found\n
={ zero, one, two, three, four, five, six, seven } > 
<? zero
=\tThe text was found\n
#
#Clear the list
={ zero, one, two, three, four, five, six, seven } > 
<*
#
#This is a duplicate of the previous tests. 
#Here we are seeing if we can rebuild a list after we clear it.
={  } > 
<+ uno 0
={ uno } > 
<+ dos 1
={ uno, dos } > 
<+ tres 2
={ uno, dos, tres } > 
<? one
=\tThe text was not found\n
={ uno, dos, tres } > 
<? seven
=\tThe text was not found\n
={ uno, dos, tres } > 
<? dos
=\tThe text was found\n
={ uno, dos, tres } > 
<!
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a linked list\n
=\t2. The above plus insert items onto the linked list\n
=\t3. The above plus loop through the linked list\n
=\t4. The above plus find items in the linked list\n
=\t5. The above plus test removing an element\n
=\ta. Insertion Sort\n
=> 
<5
#
#This will create a linked list and fill it with 7 elements
#
=Before anything was removed:   Pig, Kid, Hog, Cow, Bat, Cat, Dog\n
#
#Next we will find and remove "Hog"
#
=Remove Hog from the middle:    Pig, Kid, Cow, Bat, Cat, Dog\n
#
#Next we will find and remove Dog
#
=Remove Dog from the end:       Pig, Kid, Cow, Bat, Cat\n
#
#Next we will find and remove Pig
#
=Remove Pig from the beginning: Kid, Cow, Bat, Cat\n
#
#Finally we will loop through the list and delete everything.
#
=\tRemoving Kid\n
=\tRemoving Cow\n
=\tRemoving Bat\n
=\tRemoving Cat\n
=The list is empty\n
=Test 5 complete\n
+
=Select the test you want to run:\n
=\t1. Just create and destroy a linked list\n
=\t2. The above plus insert items onto the linked list\n
=\t3. The above plus loop through the linked list\n
=\t4. The above plus find items in the linked list\n
=\t5. The above plus test removing an element\n
=\ta. Insertion Sort\n
=> 
<a
=An array of 10 numbers\n
=Unsorted:\n
=        3.6      8.1      4.5      5.4      7.2      1.8      6.3      2.7\n
=        0.9      9.0\n
=Sorted:\n
=        0.9      1.8      2.7      3.6      4.5      5.4      6.3      7.2\n
=        8.1      9.0\n
=\n
=An array of 100 words\n
=Unsorted:\n
=        the       be       to       of      and        a       in     that\n
=       have        I       it      for      not       on     with       he\n
=         as      you       do       at     this      but      his       by\n
=       from     they       we      say      her      she       or       an\n
=       will       my      one      all    would    there    their     what\n
=         so       up      out       if    about      who      get    which\n
=         go       me     when     make      can     like     time       no\n
=       just      him     know     take   people     into     year     your\n
=       good     some    could     them      see    other     than     then\n
=        now     look     only     come      its     over    think     also\n
=       back    after      use      two      how      our     work    first\n
=       well      way     even      new     want  because      any    these\n
=       give      day     most       us\n
=Sorted:\n
=          I        a    about    after      all     also       an      and\n
=        any       as       at     back       be  because      but       by\n
=        can     come    could      day       do     even    first      for\n
=       from      get     give       go     good     have       he      her\n
=        him      his      how       if       in     into       it      its\n
=       just     know     like     look     make       me     most       my\n
=        new       no      not      now       of       on      one     only\n
=         or    other      our      out     over   people      say      see\n
=        she       so     some     take     than     that      the    their\n
=       them     then    there    these     they    think     this     time\n
=         to      two       up       us      use     want      way       we\n
=       well     what     when    which      who     will     with     work\n
=      would     year      you     your\n
-

+
=Select the test you want to run:\n
=\t1. Just create and destroy a List\n
=\t2. The above plus push items onto the List\n
=\t3. The above plus iterate through the List\n
=\t4. The above plus insert and remove items from the list\n
=\ta. Fibonacci\n
=> 
<1
#
#This is little more than a test to see if the code can compile
=Create a bool List using the default constructor\n
=\tSize:  0\n
=\tEmpty? Yes\n
#
#Using push_back(), we have a one element list
=Create a double List and add one element: 3.14159\n
=\tSize:  1\n
=\tEmpty? No\n
=\tFront: 3.14159\n
=\tBack:  3.14159\n
#
#The copy constructor should create a new version of the list
=Copy the double List using the copy-constructor\n
=\tSize:  1\n
=\tEmpty? No\n
=\tFront: 3.14159\n
=\tBack:  3.14159\n
#
#Create a list with one node. That list will get destroyed
#with the following copy constructor
=Copy a double List using the assignment operator\n
=\tSize:  1\n
=\tEmpty? No\n
=\tFront: 3.14159\n
=\tBack:  3.14159\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a List\n
=\t2. The above plus push items onto the List\n
=\t3. The above plus iterate through the List\n
=\t4. The above plus insert and remove items from the list\n
=\ta. Fibonacci\n
=> 
<2
#
#Test push_back() by adding items to the back of the list
#
#Create an integer List with the default constructor
=Enter integer values to put on the back, type 0 when done\n
=\t(   ...   ) > 
<2
=\t( 2 ... 2 ) > 
<4
=\t( 2 ... 4 ) > 
<6
=\t( 2 ... 6 ) > 
<8
=\t( 2 ... 8 ) > 
<0
#Copy the list from l1 to l2 using the copy constructor.
=Copied l1 into l2\n
#Next change the end of l1 to -42 in the front and 42 in the back.
=Modified l1\n
#Finally, clear all the elements out of l2
=Copied list l2 is empty.\n
#
#Test push_front() by adding items to the front of the list\n
#
=Enter integer values to put on the front, type 0 when done\n
=\t(   ...   ) > 
<1
=\t( 1 ... 1 ) > 
<3
=\t( 3 ... 1 ) > 
<5
=\t( 5 ... 1 ) > 
<7
=\t( 7 ... 1 ) > 
<0
#Test the size() method
=Sizes of l1 and l2 are correct\n
#Make sure that l1 was unchanged from earlier
=The list l1 was unchanged\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a List\n
=\t2. The above plus push items onto the List\n
=\t3. The above plus iterate through the List\n
=\t4. The above plus insert and remove items from the list\n
=\ta. Fibonacci\n
=> 
<3
=Create a string List with the default constructor\n
=Instructions:\n
=\t+ dog   pushes dog onto the front\n
=\t- cat   pushes cat onto the back\n
=\t#       displays the contents of the list backwards\n
=\t*       clear the list\n
=\t!       quit\n
#
#Test push_front() three times.
#Everything should appear in the opposite order
#Note that we will create an iterator on an empty list here
={  } > 
<+three
={  three  } > 
<+four
={  four  three  } > 
<+five
#
#Test push_back() three times.
#Things are added in the correct order
={  five  four  three  } > 
<-two
={  five  four  three  two  } > 
<-one
={  five  four  three  two  one  } > 
<-zero
#
#Next we will display the list backwards. 
#This will exercise rbegin(), rend(), and the -- operator
={  five  four  three  two  one  zero  } > 
<#
=\tBackwards: {  zero  one  two  three  four  five  }\n
#
#Next test clear() which should remove everthing from the list
={  five  four  three  two  one  zero  } > 
<*
#
#Test rbegin() and rend() on an empty list
={  } > 
<#
=\tBackwards: {  }\n
#
#Finally we should be able to start the list over after clear()
#the same as if we were starting from a fresh list
={  } > 
<+front
={  front  } > 
<-back
={  front  back  } > 
<#
=\tBackwards: {  back  front  }\n
={  front  back  } > 
<!
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a List\n
=\t2. The above plus push items onto the List\n
=\t3. The above plus iterate through the List\n
=\t4. The above plus insert and remove items from the list\n
=\ta. Fibonacci\n
=> 
<4
=Instructions:\n
=\t+ 3 A  put 'A' after the 3rd item in the list\n
=\t- 4    remove the fourth item from the list\n
=\t!       quit\n
#
#Remove 'b', the element in slot 1
=   0  1  2  3  4  5  6  7  8  9 10 11 12\n
={  a  b  c  d  e  f  g  h  i  j  k  l  m  }\n
=> 
<-1
#
#Remove 'd', the element in slot 2
=   0  1  2  3  4  5  6  7  8  9 10 11\n
={  a  c  d  e  f  g  h  i  j  k  l  m  }\n
=> 
<-2
#
#Remove 'c', the element in slot 1
=   0  1  2  3  4  5  6  7  8  9 10\n
={  a  c  e  f  g  h  i  j  k  l  m  }\n
=> 
<-1
#
#Now we will put 'B' in the slot between 0 and 1
=   0  1  2  3  4  5  6  7  8  9\n
={  a  e  f  g  h  i  j  k  l  m  }\n
=> 
<+1B
#
#Next we will put 'D' in the slot between 1 and 2
=   0  1  2  3  4  5  6  7  8  9 10\n
={  a  B  e  f  g  h  i  j  k  l  m  }\n
=> 
<+2D
#
#Now we will pu tC in the slot between 1 and 2
=   0  1  2  3  4  5  6  7  8  9 10 11\n
={  a  B  D  e  f  g  h  i  j  k  l  m  }\n
=> 
<+2C
#
#Test removing off the end of the list. This is a special case
=   0  1  2  3  4  5  6  7  8  9 10 11 12\n
={  a  B  C  D  e  f  g  h  i  j  k  l  m  }\n
=> 
<-12
#
#Test removing off the beginning of the list, another special case
=   0  1  2  3  4  5  6  7  8  9 10 11\n
={  a  B  C  D  e  f  g  h  i  j  k  l  }\n
=> 
<-0
#
#Test adding onto the beginning of the list, another special case
=   0  1  2  3  4  5  6  7  8  9 10\n
={  B  C  D  e  f  g  h  i  j  k  l  }\n
=> 
<+0A
#
#Finally, test adding onto the end of the list, another special case
=   0  1  2  3  4  5  6  7  8  9 10 11\n
={  A  B  C  D  e  f  g  h  i  j  k  l  }\n
=> 
<+12M
#
#All done! The CAPITAL letters are the ones added 
=   0  1  2  3  4  5  6  7  8  9 10 11 12\n
={  A  B  C  D  e  f  g  h  i  j  k  l  M  }\n
=> 
<!
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a List\n
=\t2. The above plus push items onto the List\n
=\t3. The above plus iterate through the List\n
=\t4. The above plus insert and remove items from the list\n
=\ta. Fibonacci\n
=> 
<a
=How many Fibonacci numbers would you like to see? 
<10
=\t1\n
=\t1\n
=\t2\n
=\t3\n
=\t5\n
=\t8\n
=\t13\n
=\t21\n
=\t34\n
=\t55\n
=Which Fibonacci number would you like to display? 
<500
=\t139,423,224,561,697,880,139,724,382,870,407,283,950,070,256,587,697,307,264,108,962,948,325,571,622,863,290,691,557,658,876,222,521,294,125\n
-


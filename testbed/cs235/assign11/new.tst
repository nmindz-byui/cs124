+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<1
=Bubble Sort\n
=\tBefore:\t889, 192, 528, 675, 154, 746, 562, 482, 448, 842,\n
=\t\t929, 330, 615, 225, 785, 577, 606, 426, 311, 867,\n
=\t\t773, 775, 190, 414, 155, 771, 499, 337, 298, 242,\n
=\t\t656, 188, 334, 184, 815, 388, 831, 429, 823, 331,\n
=\t\t323, 752, 613, 838, 877, 398, 415, 535, 776, 679,\n
=\t\t455, 602, 454, 545, 916, 561, 369, 467, 851, 567,\n
=\t\t609, 507, 707, 844, 643, 522, 284, 526, 903, 107,\n
=\t\t809, 227, 759, 474, 965, 689, 825, 433, 224, 601,\n
=\t\t112, 631, 255, 518, 177, 224, 131, 446, 591, 882,\n
=\t\t913, 201, 441, 673, 997, 137, 195, 281, 563, 151\n
=\n
=\tAfter:\t107, 112, 131, 137, 151, 154, 155, 177, 184, 188,\n
=\t\t190, 192, 195, 201, 224, 224, 225, 227, 242, 255,\n
=\t\t281, 284, 298, 311, 323, 330, 331, 334, 337, 369,\n
=\t\t388, 398, 414, 415, 426, 429, 433, 441, 446, 448,\n
=\t\t454, 455, 467, 474, 482, 499, 507, 518, 522, 526,\n
=\t\t528, 535, 545, 561, 562, 563, 567, 577, 591, 601,\n
=\t\t602, 606, 609, 613, 615, 631, 643, 656, 673, 675,\n
=\t\t679, 689, 707, 746, 752, 759, 771, 773, 775, 776,\n
=\t\t785, 809, 815, 823, 825, 831, 838, 842, 844, 851,\n
=\t\t867, 877, 882, 889, 903, 913, 916, 929, 965, 997\n
=The array is sorted\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<2
=Selection Sort\n
=\tBefore:\t889, 192, 528, 675, 154, 746, 562, 482, 448, 842,\n
=\t\t929, 330, 615, 225, 785, 577, 606, 426, 311, 867,\n
=\t\t773, 775, 190, 414, 155, 771, 499, 337, 298, 242,\n
=\t\t656, 188, 334, 184, 815, 388, 831, 429, 823, 331,\n
=\t\t323, 752, 613, 838, 877, 398, 415, 535, 776, 679,\n
=\t\t455, 602, 454, 545, 916, 561, 369, 467, 851, 567,\n
=\t\t609, 507, 707, 844, 643, 522, 284, 526, 903, 107,\n
=\t\t809, 227, 759, 474, 965, 689, 825, 433, 224, 601,\n
=\t\t112, 631, 255, 518, 177, 224, 131, 446, 591, 882,\n
=\t\t913, 201, 441, 673, 997, 137, 195, 281, 563, 151\n
=\n
=\tAfter:\t107, 112, 131, 137, 151, 154, 155, 177, 184, 188,\n
=\t\t190, 192, 195, 201, 224, 224, 225, 227, 242, 255,\n
=\t\t281, 284, 298, 311, 323, 330, 331, 334, 337, 369,\n
=\t\t388, 398, 414, 415, 426, 429, 433, 441, 446, 448,\n
=\t\t454, 455, 467, 474, 482, 499, 507, 518, 522, 526,\n
=\t\t528, 535, 545, 561, 562, 563, 567, 577, 591, 601,\n
=\t\t602, 606, 609, 613, 615, 631, 643, 656, 673, 675,\n
=\t\t679, 689, 707, 746, 752, 759, 771, 773, 775, 776,\n
=\t\t785, 809, 815, 823, 825, 831, 838, 842, 844, 851,\n
=\t\t867, 877, 882, 889, 903, 913, 916, 929, 965, 997\n
=The array is sorted\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<3
=Insertion Sort\n
=\tBefore:\t889, 192, 528, 675, 154, 746, 562, 482, 448, 842,\n
=\t\t929, 330, 615, 225, 785, 577, 606, 426, 311, 867,\n
=\t\t773, 775, 190, 414, 155, 771, 499, 337, 298, 242,\n
=\t\t656, 188, 334, 184, 815, 388, 831, 429, 823, 331,\n
=\t\t323, 752, 613, 838, 877, 398, 415, 535, 776, 679,\n
=\t\t455, 602, 454, 545, 916, 561, 369, 467, 851, 567,\n
=\t\t609, 507, 707, 844, 643, 522, 284, 526, 903, 107,\n
=\t\t809, 227, 759, 474, 965, 689, 825, 433, 224, 601,\n
=\t\t112, 631, 255, 518, 177, 224, 131, 446, 591, 882,\n
=\t\t913, 201, 441, 673, 997, 137, 195, 281, 563, 151\n
=\n
=\tAfter:\t107, 112, 131, 137, 151, 154, 155, 177, 184, 188,\n
=\t\t190, 192, 195, 201, 224, 224, 225, 227, 242, 255,\n
=\t\t281, 284, 298, 311, 323, 330, 331, 334, 337, 369,\n
=\t\t388, 398, 414, 415, 426, 429, 433, 441, 446, 448,\n
=\t\t454, 455, 467, 474, 482, 499, 507, 518, 522, 526,\n
=\t\t528, 535, 545, 561, 562, 563, 567, 577, 591, 601,\n
=\t\t602, 606, 609, 613, 615, 631, 643, 656, 673, 675,\n
=\t\t679, 689, 707, 746, 752, 759, 771, 773, 775, 776,\n
=\t\t785, 809, 815, 823, 825, 831, 838, 842, 844, 851,\n
=\t\t867, 877, 882, 889, 903, 913, 916, 929, 965, 997\n
=The array is sorted\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<4
=Binary Sort\n
=\tBefore:\t889, 192, 528, 675, 154, 746, 562, 482, 448, 842,\n
=\t\t929, 330, 615, 225, 785, 577, 606, 426, 311, 867,\n
=\t\t773, 775, 190, 414, 155, 771, 499, 337, 298, 242,\n
=\t\t656, 188, 334, 184, 815, 388, 831, 429, 823, 331,\n
=\t\t323, 752, 613, 838, 877, 398, 415, 535, 776, 679,\n
=\t\t455, 602, 454, 545, 916, 561, 369, 467, 851, 567,\n
=\t\t609, 507, 707, 844, 643, 522, 284, 526, 903, 107,\n
=\t\t809, 227, 759, 474, 965, 689, 825, 433, 224, 601,\n
=\t\t112, 631, 255, 518, 177, 224, 131, 446, 591, 882,\n
=\t\t913, 201, 441, 673, 997, 137, 195, 281, 563, 151\n
=\n
=\tAfter:\t107, 112, 131, 137, 151, 154, 155, 177, 184, 188,\n
=\t\t190, 192, 195, 201, 224, 224, 225, 227, 242, 255,\n
=\t\t281, 284, 298, 311, 323, 330, 331, 334, 337, 369,\n
=\t\t388, 398, 414, 415, 426, 429, 433, 441, 446, 448,\n
=\t\t454, 455, 467, 474, 482, 499, 507, 518, 522, 526,\n
=\t\t528, 535, 545, 561, 562, 563, 567, 577, 591, 601,\n
=\t\t602, 606, 609, 613, 615, 631, 643, 656, 673, 675,\n
=\t\t679, 689, 707, 746, 752, 759, 771, 773, 775, 776,\n
=\t\t785, 809, 815, 823, 825, 831, 838, 842, 844, 851,\n
=\t\t867, 877, 882, 889, 903, 913, 916, 929, 965, 997\n
=The array is sorted\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<5
=Heap Sort\n
=\tBefore:\t889, 192, 528, 675, 154, 746, 562, 482, 448, 842,\n
=\t\t929, 330, 615, 225, 785, 577, 606, 426, 311, 867,\n
=\t\t773, 775, 190, 414, 155, 771, 499, 337, 298, 242,\n
=\t\t656, 188, 334, 184, 815, 388, 831, 429, 823, 331,\n
=\t\t323, 752, 613, 838, 877, 398, 415, 535, 776, 679,\n
=\t\t455, 602, 454, 545, 916, 561, 369, 467, 851, 567,\n
=\t\t609, 507, 707, 844, 643, 522, 284, 526, 903, 107,\n
=\t\t809, 227, 759, 474, 965, 689, 825, 433, 224, 601,\n
=\t\t112, 631, 255, 518, 177, 224, 131, 446, 591, 882,\n
=\t\t913, 201, 441, 673, 997, 137, 195, 281, 563, 151\n
=\n
=\tAfter:\t107, 112, 131, 137, 151, 154, 155, 177, 184, 188,\n
=\t\t190, 192, 195, 201, 224, 224, 225, 227, 242, 255,\n
=\t\t281, 284, 298, 311, 323, 330, 331, 334, 337, 369,\n
=\t\t388, 398, 414, 415, 426, 429, 433, 441, 446, 448,\n
=\t\t454, 455, 467, 474, 482, 499, 507, 518, 522, 526,\n
=\t\t528, 535, 545, 561, 562, 563, 567, 577, 591, 601,\n
=\t\t602, 606, 609, 613, 615, 631, 643, 656, 673, 675,\n
=\t\t679, 689, 707, 746, 752, 759, 771, 773, 775, 776,\n
=\t\t785, 809, 815, 823, 825, 831, 838, 842, 844, 851,\n
=\t\t867, 877, 882, 889, 903, 913, 916, 929, 965, 997\n
=The array is sorted\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<6
=Merge Sort\n
=\tBefore:\t889, 192, 528, 675, 154, 746, 562, 482, 448, 842,\n
=\t\t929, 330, 615, 225, 785, 577, 606, 426, 311, 867,\n
=\t\t773, 775, 190, 414, 155, 771, 499, 337, 298, 242,\n
=\t\t656, 188, 334, 184, 815, 388, 831, 429, 823, 331,\n
=\t\t323, 752, 613, 838, 877, 398, 415, 535, 776, 679,\n
=\t\t455, 602, 454, 545, 916, 561, 369, 467, 851, 567,\n
=\t\t609, 507, 707, 844, 643, 522, 284, 526, 903, 107,\n
=\t\t809, 227, 759, 474, 965, 689, 825, 433, 224, 601,\n
=\t\t112, 631, 255, 518, 177, 224, 131, 446, 591, 882,\n
=\t\t913, 201, 441, 673, 997, 137, 195, 281, 563, 151\n
=\n
=\tAfter:\t107, 112, 131, 137, 151, 154, 155, 177, 184, 188,\n
=\t\t190, 192, 195, 201, 224, 224, 225, 227, 242, 255,\n
=\t\t281, 284, 298, 311, 323, 330, 331, 334, 337, 369,\n
=\t\t388, 398, 414, 415, 426, 429, 433, 441, 446, 448,\n
=\t\t454, 455, 467, 474, 482, 499, 507, 518, 522, 526,\n
=\t\t528, 535, 545, 561, 562, 563, 567, 577, 591, 601,\n
=\t\t602, 606, 609, 613, 615, 631, 643, 656, 673, 675,\n
=\t\t679, 689, 707, 746, 752, 759, 771, 773, 775, 776,\n
=\t\t785, 809, 815, 823, 825, 831, 838, 842, 844, 851,\n
=\t\t867, 877, 882, 889, 903, 913, 916, 929, 965, 997\n
=The array is sorted\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<7
=Quick Sort\n
=\tBefore:\t889, 192, 528, 675, 154, 746, 562, 482, 448, 842,\n
=\t\t929, 330, 615, 225, 785, 577, 606, 426, 311, 867,\n
=\t\t773, 775, 190, 414, 155, 771, 499, 337, 298, 242,\n
=\t\t656, 188, 334, 184, 815, 388, 831, 429, 823, 331,\n
=\t\t323, 752, 613, 838, 877, 398, 415, 535, 776, 679,\n
=\t\t455, 602, 454, 545, 916, 561, 369, 467, 851, 567,\n
=\t\t609, 507, 707, 844, 643, 522, 284, 526, 903, 107,\n
=\t\t809, 227, 759, 474, 965, 689, 825, 433, 224, 601,\n
=\t\t112, 631, 255, 518, 177, 224, 131, 446, 591, 882,\n
=\t\t913, 201, 441, 673, 997, 137, 195, 281, 563, 151\n
=\n
=\tAfter:\t107, 112, 131, 137, 151, 154, 155, 177, 184, 188,\n
=\t\t190, 192, 195, 201, 224, 224, 225, 227, 242, 255,\n
=\t\t281, 284, 298, 311, 323, 330, 331, 334, 337, 369,\n
=\t\t388, 398, 414, 415, 426, 429, 433, 441, 446, 448,\n
=\t\t454, 455, 467, 474, 482, 499, 507, 518, 522, 526,\n
=\t\t528, 535, 545, 561, 562, 563, 567, 577, 591, 601,\n
=\t\t602, 606, 609, 613, 615, 631, 643, 656, 673, 675,\n
=\t\t679, 689, 707, 746, 752, 759, 771, 773, 775, 776,\n
=\t\t785, 809, 815, 823, 825, 831, 838, 842, 844, 851,\n
=\t\t867, 877, 882, 889, 903, 913, 916, 929, 965, 997\n
=The array is sorted\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<0
=How many items in the test (10000 - 40000 are good numbers)? 
<10000
=What type of test would you like to run?\n
=   1. random numbers\n
=   2. already sorted in ascending order\n
=   3. already sorted in descending order\n
=   4. almost sorted in ascending order\n
=   5. random but with a small number of possible values\n
=> 
<1
#
#Random order. It should look something like this:
#      Sort Name    Time       Assigns      Compares
# ---------------+-------+-------------+-------------
#    Bubble Sort |  0.74 |    74432037 |    49995000 <- big assign and compare
# Selection Sort |  0.25 |       29982 |    49995000 <- big compare
# Insertion Sort |  0.13 |    24830726 |      118919 <- big assign
#    Binary Sort |  0.01 |       20000 |      154523 <- exactly 2,000 assign
#      Heap Sort |  0.00 |      372762 |      237163
#     Merge Sort |  0.01 |      138141 |      270454
#     Quick Sort |  0.00 |      113475 |      184371
#
=      Sort Name    Time       Assigns      Compares\n
= ---------------+-------+-------------+-------------\n
>    Bubble Sort |  0.74 |    74432037 |    49995000\n
> Selection Sort |  0.25 |       29982 |    49995000\n
> Insertion Sort |  0.13 |    24830726 |      118919\n
>    Binary Sort |  0.01 |       20000 |      154523\n
>      Heap Sort |  0.00 |      372762 |      237163\n
>     Merge Sort |  0.01 |      138141 |      270454\n
>     Quick Sort |  0.00 |      113475 |      184371\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<0
=How many items in the test (10000 - 40000 are good numbers)? 
<10000
=What type of test would you like to run?\n
=   1. random numbers\n
=   2. already sorted in ascending order\n
=   3. already sorted in descending order\n
=   4. almost sorted in ascending order\n
=   5. random but with a small number of possible values\n
=> 
<2
#
#Ascending order. It should look something like this:
#      Sort Name    Time       Assigns      Compares
# ---------------+-------+-------------+-------------
#    Bubble Sort |  0.27 |           0 |        9999 <- 0 assigns 9999 compares
# Selection Sort |  0.24 |           0 |    49995000 <- no assigns
# Insertion Sort |  0.00 |       19998 |      123617 <- small assigns
#    Binary Sort |  0.23 |       20000 |    49995000 <- big compares
#      Heap Sort |  0.00 |      395868 |      244560
#     Merge Sort |  0.00 |       10000 |        9999
#     Quick Sort |  0.01 |       39996 |      143615
#
=      Sort Name    Time       Assigns      Compares\n
= ---------------+-------+-------------+-------------\n
>    Bubble Sort |  0.27 |           0 |        9999\n
> Selection Sort |  0.24 |           0 |    49995000\n
> Insertion Sort |  0.00 |       19998 |      123617\n
>    Binary Sort |  0.23 |       20000 |    49995000\n
>      Heap Sort |  0.00 |      395868 |      244560\n
>     Merge Sort |  0.00 |       10000 |        9999\n
>     Quick Sort |  0.01 |       39996 |      143615\n
-
+
=Select the test you want to run:\n
=\t0. To compare all the sorting algorithms\n
=\t1. Bubble Sort\n
=\t2. Selection Sort\n
=\t3. Insertion Sort\n
=\t4. Binary Sort\n
=\t5. Heap Sort\n
=\t6. Merge Sort\n
=\t7. Quick Sort\n
=> 
<0
=How many items in the test (10000 - 40000 are good numbers)? 
<10000
=What type of test would you like to run?\n
=   1. random numbers\n
=   2. already sorted in ascending order\n
=   3. already sorted in descending order\n
=   4. almost sorted in ascending order\n
=   5. random but with a small number of possible values\n
=> 
<3
#
#Descending order. It should look something like this:
#      Sort Name    Time       Assigns      Compares
# ---------------+-------+-------------+-------------
#    Bubble Sort |  0.96 |   149985000 |    49995000 <- big assign and compare
# Selection Sort |  0.25 |       15000 |    49995000 <- big compare
# Insertion Sort |  0.28 |    50014998 |      113631 <- big assign
#    Binary Sort |  0.22 |       20000 |    49995000 <- big compare 2000 assign
#      Heap Sort |  0.01 |      350088 |      227607
#     Merge Sort |  0.00 |      138160 |      205394
#     Quick Sort |  0.00 |       54993 |      143614
#
=      Sort Name    Time       Assigns      Compares\n
= ---------------+-------+-------------+-------------\n
>    Bubble Sort |  0.96 |   149985000 |    49995000\n
> Selection Sort |  0.25 |       15000 |    49995000\n
> Insertion Sort |  0.28 |    50014998 |      113631\n
>    Binary Sort |  0.22 |       20000 |    49995000\n
>      Heap Sort |  0.01 |      350088 |      227607\n
>     Merge Sort |  0.00 |      138160 |      205394\n
>     Quick Sort |  0.00 |       54993 |      143614\n
-

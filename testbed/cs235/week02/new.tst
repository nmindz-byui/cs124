+
=Select the test you want to run:\n
=\t1. Just create and destroy a Stack.\n
=\t2. The above plus push items onto the Stack.\n
=\t3. The above plus pop items off the stack.\n
=\t4. The above plus exercise the error handling.\n
=\ta. Infix to Postfix.\n
=\tb. Extra credit: Infix to Assembly.\n
=> 
<1
#Create, destory, and copy a Stack
=Create a bool Stack using default constructor\n
=\tSize:     0\n
=\tCapacity: 0\n
=\tEmpty?    Yes\n
=Create a double Stack using the non-default constructor\n
=\tSize:     0\n
=\tCapacity: 10\n
=\tEmpty?    Yes\n
=Create a double Stack using the copy constructor\n
=\tSize:     0\n
>\tCapacity: 10\n
=\tEmpty?    Yes\n
=Copy a double Stack using the assignment operator\n
=\tSize:     0\n
>\tCapacity: 10\n
=\tEmpty?    Yes\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Stack.\n
=\t2. The above plus push items onto the Stack.\n
=\t3. The above plus pop items off the stack.\n
=\t4. The above plus exercise the error handling.\n
=\ta. Infix to Postfix.\n
=\tb. Extra credit: Infix to Assembly.\n
=> 
<2
#Create an integer Stack with the default constructor\n
#This test will exerise the grow() function
=Enter numbers, type 0 when done\n
=\t> 
<9
=\t> 
<8
=\t> 
<7
=\t> 
<6
=\t> 
<5
=\t> 
<4
=\t> 
<3
=\t> 
<2
=\t> 
<1
=\t> 
<0
#The capacity should be a power of two
=After filling the Stack, the size is:\n
=\tSize:     9\n
=\tCapacity: 16\n
=\tEmpty?    No\n
#We will copy the stack and destroy the old.
=After copying the Stack to a new Stack, the size is:\n
=\tSize:     9\n
>\tCapacity: 16\n
=\tEmpty?    No\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Stack.\n
=\t2. The above plus push items onto the Stack.\n
=\t3. The above plus pop items off the stack.\n
=\t4. The above plus exercise the error handling.\n
=\ta. Infix to Postfix.\n
=\tb. Extra credit: Infix to Assembly.\n
=> 
<3
#This will test pushing, poping, toping, and copying of a stack
=Create a string Stack with the default constructor\n
=\tTo add the word "dog", type +dog\n
=\tTo pop the word off the stack, type -\n
=\tTo see the top word, type *\n
=\tTo quit, type !\n
#
#Test pushing items onto the stack
=\t{ } > 
<+Genesis
=\t{ Genesis } > 
<+Exodus
=\t{ Genesis Exodus } > 
<+Levidicus
=\t{ Genesis Exodus Levidicus } > 
<+Numbers
=\t{ Genesis Exodus Levidicus Numbers } > 
<+Deuteronomy
#
#Test accessing the last item on the stack with top()
=\t{ Genesis Exodus Levidicus Numbers Deuteronomy } > 
<*
=Deuteronomy\n
#
#Test popping items off the stack
=\t{ Genesis Exodus Levidicus Numbers Deuteronomy } > 
<-
=\t{ Genesis Exodus Levidicus Numbers } > 
<-
=\t{ Genesis Exodus Levidicus } > 
<-
=\t{ Genesis Exodus } > 
<-
=\t{ Genesis } > 
<*
=Genesis\n
#
#Test pushing items after we have poped a few
=\t{ Genesis } > 
<+Matthew
=\t{ Genesis Matthew } > 
<+Mark
=\t{ Genesis Matthew Mark } > 
<*
=Mark\n
=\t{ Genesis Matthew Mark } > 
<+Luke
=\t{ Genesis Matthew Mark Luke } > 
<+John
=\t{ Genesis Matthew Mark Luke John } > 
<+Acts
=\t{ Genesis Matthew Mark Luke John Acts } > 
<-
#
#Now we will look at the size and capacity. 
#Since the maximum number of items was 5, there should be a capacity of 8
=\t{ Genesis Matthew Mark Luke John } > 
<!
=\tSize:     5\n
>\tCapacity: 8\n
=\tEmpty?    No\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Stack.\n
=\t2. The above plus push items onto the Stack.\n
=\t3. The above plus pop items off the stack.\n
=\t4. The above plus exercise the error handling.\n
=\ta. Infix to Postfix.\n
=\tb. Extra credit: Infix to Assembly.\n
=> 
<4
#
#Test to make sure we cannot top off of an empty stack
=\tStack::top() error message correctly caught.\n
=\t"ERROR: Unable to reference the element from an empty Stack"\n
#
#Test to make sure we cannot pop off of an empty stack
=\tStack::pop() error message correctly caught.\n
=\t"ERROR: Unable to pop from an empty Stack"\n
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Stack.\n
=\t2. The above plus push items onto the Stack.\n
=\t3. The above plus pop items off the stack.\n
=\t4. The above plus exercise the error handling.\n
=\ta. Infix to Postfix.\n
=\tb. Extra credit: Infix to Assembly.\n
=> 
<a
=Enter an infix equation.  Type "quit" when done.\n
#
#Simple test where order of operations is not verified
=infix > 
<4 + 6
=\tpostfix:  4 6 +\n
=\n
#
#If - is wrong, you are not taking order of operations into account
=infix > 
<a + b * c ^ d - e
=\tpostfix:  a b c d ^ * + e -\n
=\n
#
#Another test exercising the order of operations
=infix > 
<a ^ b + c * d
=\tpostfix:  a b ^ c d * +\n
=\n
#
#This test will verify that tokens can consist of more than one letter
=infix > 
<3.14159 * diameter
=\tpostfix:  3.14159 diameter *\n
=\n
#
#This test exercises the code's ability to see where one token begins
#and another token ends. The best way to do this is to create a Token
#class that defines the extraction operator. The rules for the end of
#of a variable are quite different than the rules for the end of a number
=infix > 
<4.5+a5+.1215  +   1
=\tpostfix:  4.5 a5 + .1215 + 1 +\n
=\n
#
#This is really no different than the previous test
=infix > 
<pi*r^2
=\tpostfix:  pi r 2 ^ *\n
=\n
#
#This too is no different than the previous test
=infix > 
<(5.0  /  .9)*(fahrenheit - 32)
=\tpostfix:  5.0 .9 / fahrenheit 32 - *\n
=\n
=infix > 
<quit
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Stack.\n
=\t2. The above plus push items onto the Stack.\n
=\t3. The above plus pop items off the stack.\n
=\t4. The above plus exercise the error handling.\n
=\ta. Infix to Postfix.\n
=\tb. Extra credit: Infix to Assembly.\n
=> 
<b
=Enter an infix equation.  Type "quit" when done.\n
#
#Simple test to see if a single triplet of assembly statements can be generated
=infix > 
<4 + 6
=\tLOAD 4\n
=\tADD 6\n
=\tSTORE VALUE1\n
#
#Another simple triplet
=infix > 
<3.14159 * diameter
=\tLOAD 3.14159\n
=\tMULTIPLY diameter\n
=\tSTORE VALUE1\n
#
#This test will see if we can increment the VALUE variable correctly.
#There are three triples of statements, all ADD
=infix > 
<4.5+a5+.1215  +   1
=\tLOAD 4.5\n
=\tADD a5\n
=\tSTORE VALUE1\n
=\tLOAD VALUE1\n
=\tADD .1215\n
=\tSTORE VALUE2\n
=\tLOAD VALUE2\n
=\tADD 1\n
=\tSTORE VALUE3\n
#
#This mixes order-of-operation complexity with multiple operands
=infix > 
<pi*r^2
=\tLOAD r\n
=\tEXPONENT 2\n
=\tSTORE VALUE1\n
=\tLOAD pi\n
=\tMULTIPLY VALUE1\n
=\tSTORE VALUE2\n
#
#Here we have code similar to what we can find in a C++ statement
=infix > 
<(5.0  /  .9)*(fahrenheit - 32)
=\tLOAD 5.0\n
=\tDIVIDE .9\n
=\tSTORE VALUE1\n
=\tLOAD fahrenheit\n
=\tSUBTRACT 32\n
=\tSTORE VALUE2\n
=\tLOAD VALUE1\n
=\tMULTIPLY VALUE2\n
=\tSTORE VALUE3\n
=infix > 
<quit
-


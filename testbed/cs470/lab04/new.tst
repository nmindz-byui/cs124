+
#
# The first test is trival: nothign is to be removed or filtered
#
=> 
<Plain text
=\tPlain text\n
#
# The second test has a single valid link
#
=> 
<This is <b>Bold</b> text.
=\tThis is <b>Bold</b> text.\n
#
# The next text has attributes in a valid tag that should come across
#
=> 
<Text <a href="www.byui.edu">hyperlink</a> Text
=\tText <a href="www.byui.edu">hyperlink</a> Text\n
#
# Next we have an invalid tag: the <p> tag.
#
=> 
<<p>Paragraph</p>
=\t&lt;p&gt;Paragraph&lt;/p&gt;\n
#
# The span tag also is also not on the white list.
#
=> 
<Text <span> span </span> text
=\tText &lt;span&gt; span &lt;/span&gt; text\n
#
# Properly nested tags, all of which are on the white list
#
=> 
<plain <b> bold <i> bold/italic </i> bold </b> plain
=\tplain <b> bold <i> bold/italic </i> bold </b> plain\n
#
# Inproperly nested tags that are on the white list
#
=> 
<plain <b> bold <i> bold/italic </b> not nested properly </i> plain
=\tplain <b> bold <i> bold/italic &lt;/b&gt; not nested properly </i> plain</b>\n
#
# All the white list tags properly nested
#
=> 
<<a>1<abbr>2<acronym>3<b>4<blockquote>5<cite>6<code>7<del><em><i><q><strike><strong>8</strong>9</strike>10</q>11</i>12</em></del></code></cite></blockquote></b></acronym></abbr></a>13
=\t<a>1<abbr>2<acronym>3<b>4<blockquote>5<cite>6<code>7<del><em><i><q><strike><strong>8</strong>9</strike>10</q>11</i>12</em></del></code></cite></blockquote></b></acronym></abbr></a>13\n
#
# Done!
#
=> 
<quit
=\tquit\n
-
+
#
# Simple password consisting of nothing but numbers.
# If you choose 10 bits, you are rounding up instead of down.
# Note log_2(1024) = 10 because 2^10 = 1024. Since we are just
# below that number, this is 9 bits
#
=Please enter the password: 
<439
=There are 1000 combinations\n
=That is equivalent to a key of 9 bits\n
-
+
#
# Another simple password consisting only of uppercase letters.
# This should be 26^4 combinations
#
=Please enter the password: 
<TEFZ
=There are 456976 combinations\n
=That is equivalent to a key of 18 bits\n
-
+
#
# Very similar to the previous test. Here
# you will need to handle very large numbers
#
=Please enter the password: 
<helfrich
=There are 208827064576 combinations\n
=That is equivalent to a key of 37 bits\n
-
+
#
# Mixed case. Now the alphabet size is 52. Notice
# How many combinations there are compared to the lowercase
# version of the same.
#
=Please enter the password: 
<HELfriCH
=There are 53459728531456 combinations\n
=That is equivalent to a key of 45 bits\n
-
+
#
# A mixture of numbers, letters, and symbols. 
# Note that there are 32 symbols
#
=Please enter the password: 
<5Ra%
=There are 78074896 combinations\n
=That is equivalent to a key of 26 bits\n
-

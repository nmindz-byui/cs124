+
#
# Trivial test with three simple strings, each of one character in length
#
=Enter three strings: 
<a
<b
<c
=s1 = "a"\n
=s2 = "b"\n
=s3 = "c"\n
=\n
=Compare\n
=a <  b\n
=a <= b\n
=a != b\n
=a <  c\n
=a <= c\n
=a != c\n
=\n
=Append\n
=b +  c --> bc\n
=a +  b --> ab\n
=a += b --> ab\n
=b += c --> bc\n
=\n
=Assign\n
=bc = bc\n
=text = "text"\n
=x = 'x'\n
=after clear() = ""\n
-
+
#
# Slightly more complex with multi-letter input
#
=Enter three strings: 
<Banana
<Banana
<Apples
=s1 = "Banana"\n
=s2 = "Banana"\n
=s3 = "Apples"\n
=\n
=Compare\n
=Banana >= Banana\n
=Banana <= Banana\n
=Banana == Banana\n
=Banana >  Apples\n
=Banana >= Apples\n
=Banana != Apples\n
=\n
=Append\n
=Banana +  Apples --> BananaApples\n
=Banana +  Banana --> BananaBanana\n
=Banana += Banana --> BananaBanana\n
=Banana += Apples --> BananaApples\n
=\n
=Assign\n
=BananaApples = BananaApples\n
=text = "text"\n
=x = 'x'\n
=after clear() = ""\n
-
+
#
# This line mostly tests the extraction operator and getline.
#
=Enter three strings: 
<abcd e f g h
<lmnop wxyz
=s1 = "abcd e f g h"\n
=s2 = "lmnop"\n
=s3 = "wxyz"\n
=\n
=Compare\n
=abcd e f g h <  lmnop\n
=abcd e f g h <= lmnop\n
=abcd e f g h != lmnop\n
=abcd e f g h <  wxyz\n
=abcd e f g h <= wxyz\n
=abcd e f g h != wxyz\n
=\n
=Append\n
=lmnop +  wxyz --> lmnopwxyz\n
=abcd e f g h +  lmnop --> abcd e f g hlmnop\n
=abcd e f g h += lmnop --> abcd e f g hlmnop\n
=lmnop += wxyz --> lmnopwxyz\n
=\n
=Assign\n
=lmnopwxyz = lmnopwxyz\n
=text = "text"\n
=x = 'x'\n
=after clear() = ""\n
-

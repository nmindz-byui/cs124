+
#
# PART 1
#
# The first test will test the ability to find the code, heap, and stack
#
=Please select an option:\n
=  1.  Find the address\n
=  2.  Display the contents of the stack\n
=  3.  Manipulate the stack\n
=> 
<1
#
# This should look like:
#
# Stack: 0x7fffaf94cc30
# Heap:  0x24f9010
# Code:  0x4011e9
#
>Stack: 0x7ffd07ee9c80\n
>Heap:  0xe6d010\n
>Code:  0x4011e9\n
-
+
#
# PART 2
#
# The second test will display the contents of the stack.
#
=Please select an option:\n
=  1.  Find the address\n
=  2.  Display the contents of the stack\n
=  3.  Manipulate the stack\n
=> 
<2
#
# This should look like:
#
#[ i]        address         hexadecimal             decimal        characters
#----+---------------+-------------------+-------------------+---------------+
#[-4] 0x7ffddbb9b9c8                   0                   0   . . . . . . . .
#[-3] 0x7ffddbb9b9d0              23cace             2345678   . . # . . . . .
#[-2] 0x7ffddbb9b9d8        7ffddbb9ba60     140728289835616   ` . . . . . . .
#[-1] 0x7ffddbb9b9e0        7ffddbb9ba1c     140728289835548   . . . . . . . .
#[ 0] 0x7ffddbb9b9e8              34bf15             3456789   . . 4 . . . . .
#[ 1] 0x7ffddbb9b9f0             15480e8            22315240   . . T . . . . .
#[ 2] 0x7ffddbb9b9f8        7ffddbb9b9f8     140728289835512   . . . . . . . .
#[ 3] 0x7ffddbb9ba00        7ffddbb9ba00     140728289835520   . . . . . . . .
#[ 4] 0x7ffddbb9ba08           4dbb9ba20         20866251296     . . . . . . .
#[ 5] 0x7ffddbb9ba10        7f97199cdef8     140286946500344   . . . . . . . .
#[ 6] 0x7ffddbb9ba18                   0                   0   . . . . . . . .
#[ 7] 0x7ffddbb9ba20              400f80             4198272   . . @ . . . . .
#[ 8] 0x7ffddbb9ba28        7ffddbb9bb50     140728289835856   P . . . . . . .
#[ 9] 0x7ffddbb9ba30                   0                   0   . . . . . . . .
#[10] 0x7ffddbb9ba38                   0                   0   . . . . . . . .
#[11] 0x7ffddbb9ba40        7ffddbb9ba70     140728289835632   p . . . . . . .
#[12] 0x7ffddbb9ba48              401123             4198691   # . @ . . . . .
#[13] 0x7ffddbb9ba50           200401a30          8594135600   0 . @ . . . . .
#[14] 0x7ffddbb9ba58              12d687             1234567   . . . . . . . .
#[15] 0x7ffddbb9ba60    25256e69616d2525 2676666952426792229   % % m a i n % %
#[16] 0x7ffddbb9ba68                   0                   0   . . . . . . . .
#[17] 0x7ffddbb9ba70                   0                   0   . . . . . . . .
#[18] 0x7ffddbb9ba78        7f9718e28b35     140286934289205   5 . . . . . . .
#[19] 0x7ffddbb9ba80          2000000000        137438953472   . . . .   . . .
#[20] 0x7ffddbb9ba88        7ffddbb9bb58     140728289835864   X . . . . . . .
#
=[ i]        address         hexadecimal             decimal        characters\n
=----+---------------+-------------------+-------------------+-----------------+\n
>[-4] 0x7ffddbb9b9c8                   0                   0   . . . . . . . .
>[-3] 0x7ffddbb9b9d0              23cace             2345678   . . # . . . . .
>[-2] 0x7ffddbb9b9d8        7ffddbb9ba60     140728289835616   ` . . . . . . .
>[-1] 0x7ffddbb9b9e0        7ffddbb9ba1c     140728289835548   . . . . . . . .
>[ 0] 0x7ffddbb9b9e8              34bf15             3456789   . . 4 . . . . .
>[ 1] 0x7ffddbb9b9f0             15480e8            22315240   . . T . . . . .
>[ 2] 0x7ffddbb9b9f8        7ffddbb9b9f8     140728289835512   . . . . . . . .
>[ 3] 0x7ffddbb9ba00        7ffddbb9ba00     140728289835520   . . . . . . . .
>[ 4] 0x7ffddbb9ba08           4dbb9ba20         20866251296     . . . . . . .
>[ 5] 0x7ffddbb9ba10        7f97199cdef8     140286946500344   . . . . . . . .
>[ 6] 0x7ffddbb9ba18                   0                   0   . . . . . . . .
>[ 7] 0x7ffddbb9ba20              400f80             4198272   . . @ . . . . .
>[ 8] 0x7ffddbb9ba28        7ffddbb9bb50     140728289835856   P . . . . . . .
>[ 9] 0x7ffddbb9ba30                   0                   0   . . . . . . . .
>[10] 0x7ffddbb9ba38                   0                   0   . . . . . . . .
>[11] 0x7ffddbb9ba40        7ffddbb9ba70     140728289835632   p . . . . . . .
>[12] 0x7ffddbb9ba48              401123             4198691   # . @ . . . . .
>[13] 0x7ffddbb9ba50           200401a30          8594135600   0 . @ . . . . .
>[14] 0x7ffddbb9ba58              12d687             1234567   . . . . . . . .
>[15] 0x7ffddbb9ba60    25256e69616d2525 2676666952426792229   % % m a i n % %
>[16] 0x7ffddbb9ba68                   0                   0   . . . . . . . .
>[17] 0x7ffddbb9ba70                   0                   0   . . . . . . . .
>[18] 0x7ffddbb9ba78        7f9718e28b35     140286934289205   5 . . . . . . .
>[19] 0x7ffddbb9ba80          2000000000        137438953472   . . . .   . . .
>[20] 0x7ffddbb9ba88        7ffddbb9bb58     140728289835864   X . . . . . . .
-
+
#
# PART 3
#
# The final test is to manipulate the stack so scope is violated
#
=Please select an option:\n
=  1.  Find the address\n
=  2.  Display the contents of the stack\n
=  3.  Manipulate the stack\n
> 
<3
=number: 
<101010101
=text:   
<ScopeIsForWimps
=\tnumber: 101010101\n
=\ttext:   ScopeIsForWimps\n
=\tGood job, you got a(n) 'A'\n
-
+
#
# Bob has full access so, once he is authenticated,
# the program should behave as if there is no access control
#
=Username: 
<Bob
=Password: 
<passwordBob
=Which student's grade would you like to review?\n
=\t1.\tSamual Stevenson\n
=\t2.\tSusan Bakersfield\n
=\t3.\tSylvester Stallone\n
=\t0.\tNo student, exit\n
=> 
<1
=Editing the scores of Samual Stevenson\n
=Score list\n
=\tScore \tWeight\n
=(1)\t77%\t0.4\n
=(2)\t100%\t0.2\n
=(3)\t95%\t0.2\n
=(4)\t87%\t0.2\n
=(0)\tExit\n
=Which score would you like to edit (0-4): 
<0
=Student name:\n
=\tSamual Stevenson\n
=Grade:\n
=\t87.2% : B+\n
=Detailed score:\n
=\tScore \tWeight\n
=\t77%\t0.4\n
=\t100%\t0.2\n
=\t95%\t0.2\n
=\t87%\t0.2\n
=---------------------------------------------------\n
=Which student's grade would you like to review?\n
=\t1.\tSamual Stevenson\n
=\t2.\tSusan Bakersfield\n
=\t3.\tSylvester Stallone\n
=\t0.\tNo student, exit\n
=> 
<0
-
+
#
# Sly has limited access, only able to view his own grades
#
=Username: 
<Sly
=Password: 
<passwordSly
=Which student's grade would you like to review?\n
=\t1.\tSamual Stevenson\n
=\t2.\tSusan Bakersfield\n
=\t3.\tSylvester Stallone\n
=\t0.\tNo student, exit\n
=> 
<1
#
# Notice how the grade data is empty. 
# If this were Sam, he would see the following
#   (1)_tab_score_tab_weight_newline
# However, Sly cannot see these. Thus we expect
#   (1)_tab_newline
#
=Editing the scores of Samual Stevenson\n
=Score list\n
=(1)\t\n
=(2)\t\n
=(3)\t\n
=(4)\t\n
=(0)\tExit\n
=Which score would you like to edit (0-4): 
<0
=---------------------------------------------------\n
=Which student's grade would you like to review?\n
=\t1.\tSamual Stevenson\n
=\t2.\tSusan Bakersfield\n
=\t3.\tSylvester Stallone\n
=\t0.\tNo student, exit\n
=> 
<3
#
# HEre Sly can see his own grades.
#
=Editing the scores of Sylvester Stallone\n
=Score list\n
=\tScore \tWeight\n
=(1)\t63%\t0.4\n
=(2)\t0%\t0.2\n
=(3)\t58%\t0.2\n
=(4)\t49%\t0.2\n
=(0)\tExit\n
=Which score would you like to edit (0-4): 
<0
#
# We have a significant problem here. Sly can edit
# his own grades. Clearly Bell-LaPadula is not sufficient
# for this grade program. We should probably use ACLs.
#
=Student name:\n
=\tSylvester Stallone\n
=Grade:\n
=\t46.6% : F\n
=Detailed score:\n
=\tScore \tWeight\n
=\t63%\t0.4\n
=\t0%\t0.2\n
=\t58%\t0.2\n
=\t49%\t0.2\n
=---------------------------------------------------\n
=Which student's grade would you like to review?\n
=\t1.\tSamual Stevenson\n
=\t2.\tSusan Bakersfield\n
=\t3.\tSylvester Stallone\n
=\t0.\tNo student, exit\n
=> 
<0
-
+
#
# Now we will try an unauthenticed individual
#
=Username: 
<Mr.J.T.Hacker
=Password: 
<12345
=Failed to authenticate Mr.J.T.Hacker\n
=Which student's grade would you like to review?\n
=\t1.\tSamual Stevenson\n
=\t2.\tSusan Bakersfield\n
=\t3.\tSylvester Stallone\n
=\t0.\tNo student, exit\n
=> 
<3
#
# Mr. J.T.Hacker should not have access to anyone's grades
#
=Editing the scores of Sylvester Stallone\n
=Score list\n
=(1)\t\n
=(2)\t\n
=(3)\t\n
=(4)\t\n
=(0)\tExit\n
=Which score would you like to edit (0-4): 
<0
=---------------------------------------------------\n
=Which student's grade would you like to review?\n
=\t1.\tSamual Stevenson\n
=\t2.\tSusan Bakersfield\n
=\t3.\tSylvester Stallone\n
=\t0.\tNo student, exit\n
=> 
<0
=

+
#
# Trivial case. They are the same so they are homographs
#
=Specify the first filename:  
<file.txt
=Specify the second filename: 
<file.txt
=The files are homographs\n
-
+
#
# Another trivial case. They are different so they are not homographs
#
=Specify the first filename:  
<file1.txt
=Specify the second filename: 
<file2.txt
=The files are NOT homographs\n
-
+
#
# Slightly more complex. the "./" should refer to itself so they are the same
#
=Specify the first filename:  
<./file.txt
=Specify the second filename: 
<file.txt
=The files are homographs\n
-
+
#
# The more general case to the above test. We can add as many "./"s as we want
#
=Specify the first filename:  
</home/cs470/lesson12/file.txt
=Specify the second filename: 
</home/./cs470/./lesson12/./././file.txt
=The files are homographs\n
-
+
#
# Note that ".." is not the same thing as "."
#
=Specify the first filename:  
</home/../cs470/../lesson12/file.txt
=Specify the second filename: 
</home/./cs470/./lesson12/file.txt
=The files are NOT homographs\n
-
+
#
# Even if the directories "one" "two" and "three" are not present, going
# up three and back down is the same thing as doing nothing.
#
=Specify the first filename:  
<one/two/three/../../../file.txt
=Specify the second filename: 
<./file.txt
=The files are homographs\n
-
+
#
# Once again, "." is not the same thing as ".."
#
=Specify the first filename:  
</directory/../file.txt
=Specify the second filename: 
</directory/./file.txt
=The files are NOT homographs\n
-
+
#
# Once we hit the root, we can add as many ".."s as we want. 
#
=Specify the first filename:  
<../../../../../../../../home/cs470/lesson12/lesson12.out
=Specify the second filename: 
</home/cs470/lesson12/banana/../lesson12.out
=The files are homographs\n
-
+
#
# The order of the ".." and "." matters
#
=Specify the first filename:  
</one/./../two/./../three/./../four/.txt
=Specify the second filename: 
<one/.././two/.././three/.././four.txt
=The files are NOT homographs\n
-




#!/usr/bin/perl

#Author: Burdette Pixton
#Program: hw08.pl
#Summary: this will make a file that will be used for testing
#         a multiplication table (1-12) (hw08)

$max = 12;
$vert = 1;
$horz = 1;
open(TEMP, ">hw08.test");
print TEMP "+\n\$\n";
print TEMP "=  ";
for($horz = 1; $horz <= $max; $horz++)
{
   &spaces($horz);
}


for($horz = 1; $horz <= $max; $horz++)
{
   &outerLoop;
   for($vert =1; $vert <= $max; $vert++)
   {
      $product = $vert * $horz;
      &spaces($product);
   }

}

print TEMP "\n-";

sub spaces
{
   $_ = @_[0];
   if($_ < 10)
   {
      print TEMP "    $_";
   }
   elsif($_ < 100)
   {
      print TEMP "   $_";
   }
   else
   {
      print TEMP "  $_";
   }
}


sub outerLoop
{
   print TEMP "\n";
   if($horz < 10)
   {
      print TEMP "= $horz";
   }
   else
   {
      print TEMP "=$horz";
   }
}

#!/usr/bin/perl

#Author: Burdette Pixton
#Program: hw14.pl
#Summary: this will make a file that will be used for testing
#         for reduced fractions (hw14)

open(TEMP, ">hw14.test");
print TEMP "+\n\$";

@correctIn = ("52/104","-192/-36","12/12","39/-13","30/45","78/-100","0/8","5/3");
@ans = ("1/2","16/3","1/1","-3/1","2/3","-39/50","0/8","5/3");
@errorIn = ("q123/4","Q/b","34","1;5","11/");
$errAns="Parse error";

print TEMP "\n=Enter a fraction to be reduced (such as 30/45) or quit";

for($i = 0; $i < 8; $i++)
{
   print TEMP "\n=Input: ";
   print TEMP "\n<$correctIn[$i]";
   print TEMP "\n=$ans[$i]";
   print TEMP "\n\$";
}

for($i = 0; $i < 5 ; $i++)
{
   print TEMP "\n=Input: ";
   print TEMP "\n<$errorIn[$i]";
   print TEMP "\n=$errAns";
   print TEMP "\n\$";
}
print TEMP "\n=Input: ";
print TEMP "\n<7/0";
print TEMP "\n=Error: cannot divide by zero";
print TEMP "\n\$";

print TEMP "\n=Input: ";
print TEMP "\n<quit";
print TEMP "\n-";

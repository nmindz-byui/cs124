#!/usr/bin/perl

#Author: Burdette Pixton
#Program: hw16.pl
#Summary: this will make a file that will be used for testing
#         the rational number calculator (hw16)

open(TEMP, ">hw16.test");
print TEMP "+\n\$";

@correctIn = ("11/52+30/104","17/-5/-17/-5","7/3--36/12","3/4*4/3","5/8/0/3",
              "7/11+0/3","7/0-82/1","17/45+13/45","a/4/11/3");
@correctAns = ("1/2","-1/1","16/3","1/1","Error: cannot divide by zero",
               "7/11","Error: cannot divide by zero","2/3","Parse error");

print TEMP "\n=Enter an arithmetic expression using fractions";
print TEMP "\n=   (such as 1/2+3/4) or quit";

for($i = 0; $i < 9; $i++)
{
   print TEMP "\n=Input: ";
   print TEMP "\n<$correctIn[$i]";
   print TEMP "\n=@correctAns[$i]";
   print TEMP "\n\$";
}

print TEMP "\n=Input: ";
print TEMP "\n<QUIT";
print TEMP "\n-";

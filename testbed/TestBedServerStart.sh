mkdir -p /tmp/testbed
mkdir -p /tmp/testbed/IN
mkdir -p /tmp/testbed/OUT
chown testbed:student /tmp/testbed /tmp/testbed/IN  /tmp/testbed/OUT
chmod 777 /tmp/testbed /tmp/testbed/IN  /tmp/testbed/OUT
java -jar /mnt/local/testbed/TestBed.jar server >/tmp/testbed.log 2>&1 &

echo "ps looking for process:"
ps -ef | grep TestBed | grep -v grep

#Do cleanup
# kill the TestBed.jar
#pkill -u testbed java
#rm -rf /tmp/testbed /tmp/testbed.log 


#!/usr/bin/perl

#print @ARGV[0];
#print "\n";

if (@ARGV[0] =~ /.*\.cpp/)
{
   $lang = 0;
   system "g++ -o@ARGV[0].exe @ARGV[0] -fopenmp 2> @ARGV[0].compile";
}
elsif (@ARGV[0] =~ /.*\.c/)
{
   $lang = 0;
   system "gcc -o@ARGV[0].exe @ARGV[0] -fopenmp 2> @ARGV[0].compile";
}
elsif (@ARGV[0] =~ /.*\.java/)
{
   $lang = 1;
   system "javac @ARGV[0] 2> @ARGV[0].compile";
}
else
{
   #print "Invalid file format\n";
   exit 2;
}

open(COMP, "<@ARGV[0].compile");

if (<COMP>)
{
   $status = 1;
}

if ($lang == 0)
{
   open(EXE, "<@ARGV[0].exe");
}
elsif ($lang == 1)
{
   @filename = split (/\./, @ARGV[0]);
   $filename = shift @filename;
   #print "The filename is: ";
   #print "$filename";
   open(EXE, "<$filename.class");
}


if (! <EXE>)
{
   $status = 2;
}

#print $status + "\n";

exit $status;

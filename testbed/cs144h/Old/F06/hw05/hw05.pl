#! /usr/bin/perl

#Author: Burdette Pixton
#Program: hw03.pl
#Summary: this will make a file that will be used for testing
#         the change maker program (hw03)


#please note hw03.test takes a very long time to run. (about 1.5 min)
open(TEMP, ">hw03.test");
$numToTest = 101;

for($i = 0; $i < $numToTest; $i++)
{
   $input = 100 - $i;
   $quarter = $input / 25;
   $input %= 25;
   $dime = $input / 10;
   $input %= 10;
   $nickel = $input / 5;
   $input %= 5;
   $qu = &trunk($quarter);
   $di = &trunk($dime);
   $ni = &trunk($nickel);
   $in = &trunk($input);
   $answer="Your change is $qu quarter(s), $di dime(s), $ni nickel(s),";
   $answer2 ="and $in penny(ies).";

   print TEMP "+\n";
   print TEMP "\$\n";
   print TEMP "=Enter the cost of an item in whole cents (0-100): ";
   print TEMP "\n<$i";
   print TEMP "\n=The item costs $i cents.";
   print TEMP "\n=$answer $answer2";
   print TEMP "\n-\n";
}
   sub trunk
   #this subroutine trunckates the number.
   {
     $_ = @_[0];
     /(\d+?)/;
     return $1;
   }




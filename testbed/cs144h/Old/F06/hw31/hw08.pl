#! /usr/bin/perl

#Author: Burdette Pixton
#Program: hw04.pl
#Summary: this will make a file that will be used for testing
#         the temperature (hw04)

#the first array is the input, the second array is the correct output
#for fahreheit, the thrid arrayt is the correct output for celsius

@correct = (-10.1, 68.4, "846.0","-533.0","100.0",0.9,100.5,0.8,0.9,"90.0",933.3,
         -476.2,"80.0",98.6,"-40.0");
@fah = ( 13.8,155.1,1554.8,-927.4,"212.0",33.6,212.9,33.4,33.6,"194.0",1711.9,
         -825.2,"176.0",209.5,"-40.0");
@cel = (-23.4,20.2,452.2,-313.9,37.8,-17.3,38.1,-17.3,-17.3,32.2,500.7,
         -282.3,26.7,"37.0","-40.0");

open(TEMP, ">hw04.test");

for($i = 0; $i < 15; $i++)
{
   print TEMP "+\n";
   print TEMP "\$\n";
   print TEMP "=Please enter a floating point number: ";
   print TEMP "\n<$correct[$i]\n";
   print TEMP "=$correct[$i]C is $fah[$i]F\n";
   print TEMP "=$correct[$i]F is $cel[$i]C\n";
   print TEMP "-\n";
}




+
=Select the test you want to run:\n
=\t1. Just create and destroy a BinaryNode\n
=\t2. The above plus add a few nodes to create a Binary Tree\n
=\t3. The above plus display the contents of a Binary Tree\n
=\t4. The above plus merge Binary Trees\n
=\ta. To generate Huffman codes\n
=> 
<1
#Empty tree of size one
=Create a bool BinaryNode using the default constructor\n
=\tSize:    1\n
#A singleton tree of size one
=Create a double BinaryNode using the non-default constructor\n
=\tSize:    1\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BinaryNode\n
=\t2. The above plus add a few nodes to create a Binary Tree\n
=\t3. The above plus display the contents of a Binary Tree\n
=\t4. The above plus merge Binary Trees\n
=\ta. To generate Huffman codes\n
=> 
<2
#
#Create an integer Binary Tree with the non-default constructor.
#Next we will add six items to make a tree
=The elements in the binary tree:\n
=\tRoot......... 1\n
=\tLeft......... 2\n
=\tRight........ 3\n
=\tLeft-Left.... 4\n
=\tLeft-Right... 5\n
=\tRight-Left... 6\n
=\tRight-Right.. 7\n
=\tSize: 7\n
#
#Test to make sure pParent is set up correctly
=All the parent nodes are correctly set\n
#
#Check to see if we can move nodes around
=Was able to move the '6' and '7' nodes\n
#
#Check to see if deleteBinaryTree() works with partial trees
=Size after deleting half the nodes: 4\n
#
#Check to see if deleteBinaryTree() can delete the rest of the tree.
#If this fails, it probably means that the first call to deleteBinaryTree()
#left the tree in an invalid state
=Was able to delete the rest of the binary tree\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BinaryNode\n
=\t2. The above plus add a few nodes to create a Binary Tree\n
=\t3. The above plus display the contents of a Binary Tree\n
=\t4. The above plus merge Binary Trees\n
=\ta. To generate Huffman codes\n
=> 
<3
#Create a string Binary Node with the default constructor\n
=Enter seven words\n
#
#Test to see if you can add a node to a NULL root
=\tRoot node:         
<four
#
#The next two cases were exercised in Test 2
=\tLeft child:        
<two
=\tRight child:       
<six
#
#These four also have been exercised in Test 2
=\tLeft-Left child:   
<one
=\tLeft-Right child:  
<three
=\tRight-Left child:  
<five
=\tRight-Right child: 
<seven
#
#Two tests here.
#1. See if we can add NULL nodes
#2. See if the insertion operator was correctly written
=Completed tree: { one two three four five six seven }\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BinaryNode\n
=\t2. The above plus add a few nodes to create a Binary Tree\n
=\t3. The above plus display the contents of a Binary Tree\n
=\t4. The above plus merge Binary Trees\n
=\ta. To generate Huffman codes\n
=> 
<4
#
#Create the middle tree, nothing fancy
=Middle tree: { l m n } size = 3\n
#
#Create the lower tree
=Lower tree: { a b c } size = 3\n
#
#Create the upper tree
=Upper tree: { x y z } size = 3\n
#
#The merged tree.
#We are putting { a b c } under the l of { l m n }
#We are putting { x y z } under the n of { l m n }
=Merged tree: { a b c l m n x y z } size = 9\n
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a BinaryNode\n
=\t2. The above plus add a few nodes to create a Binary Tree\n
=\t3. The above plus display the contents of a Binary Tree\n
=\t4. The above plus merge Binary Trees\n
=\ta. To generate Huffman codes\n
=> 
<a
=Enter the filename containing the value frequencies.\n
=Enter "quit" when done.\n
#
#The example from the assignment
#               1- A
#               |
#          1----+
#          |    |
#          |    0- D
#     1----+
#     |    |    1- C
#     |    |    |
# ----+    0----+
#     |         |
#     |         0- B
#     0-E
=> 
</home/cs235/week08/huffman1.txt
=A = 111\n
=B = 100\n
=C = 101\n
=D = 110\n
=E = 0\n
#
#
#I have seen this done by hand once by a student; it was huge!
=> 
</home/cs235/week08/huffman3.txt
=a = 1111\n
=b = 101001\n
=c = 10001\n
=d = 10101\n
=e = 011\n
=f = 00010\n
=g = 101000\n
=h = 0100\n
=i = 1011\n
=j = 0000011101\n
=k = 0000010\n
=l = 11101\n
=m = 00001\n
=n = 1100\n
=o = 1101\n
=p = 00011\n
=q = 000001111\n
=r = 0101\n
=s = 1001\n
=t = 001\n
=u = 10000\n
=v = 000000\n
=w = 111001\n
=x = 00000110\n
=y = 111000\n
=z = 0000011100\n
=> 
<quit
-

+
=Select the test you want to run:\n
=\t1. Just create and destroy a Deque\n
=\t2. The above plus push, pop, top\n
=\t3. The above plus test implementation of wrapping\n
=\t4. The above plus exercise the error Deque\n
=\ta. Now Serving\n
=> 
<1
#Create, destory, and copy a Deque
=Create a bool Deque using default constructor\n
=\tSize:     0\n
=\tCapacity: 0\n
=\tEmpty?    Yes\n
=Create a double Deque using the non-default constructor\n
=\tSize:     0\n
=\tCapacity: 10\n
=\tEmpty?    Yes\n
=Create a double Deque using the copy constructor\n
=\tSize:     0\n
>\tCapacity: 10\n
=\tEmpty?    Yes\n
=Copy a double Deque using the assignment operator\n
=\tSize:     0\n
>\tCapacity: 10\n
=\tEmpty?    Yes\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Deque\n
=\t2. The above plus push, pop, top\n
=\t3. The above plus test implementation of wrapping\n
=\t4. The above plus exercise the error Deque\n
=\ta. Now Serving\n
=> 
<2
#Create an integer Deque with the default constructor\n
=Enter integer values, type 0 when done\n
#Initially empty
=\t{ } > 
<10
#First resize should set the capacity to one
=\t{ 10 } > 
<11
#Now the capacity should be two
=\t{ 10 11 } > 
<12
#Double again to four
=\t{ 10 11 12 } > 
<13
=\t{ 10 11 12 13 } > 
<14
#Double again to 8
=\t{ 10 11 12 13 14 } > 
<0
=\tSize:     5\n
=\tEmpty?    No\n
=\tCapacity: 8\n
#When we clear d1, then d2 and d3 should keep their data
=\td1 = { }\n
=\td2 = { 10 11 12 13 14 }\n
=\td3 = { 10 11 12 13 14 }\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Deque\n
=\t2. The above plus push, pop, top\n
=\t3. The above plus test implementation of wrapping\n
=\t4. The above plus exercise the error Deque\n
=\ta. Now Serving\n
=> 
<3
#Create a string Deque with the non-default constructor\n
=instructions:\n
=\t+f dog   pushes dog onto the front\n
=\t+b cat   pushes cat onto the back\n
=\t-f       pops off the front\n
=\t-b       pops off the back\n
=\t*        clear the deque\n
=\t?        shows the statistics of the deque\n
=\t!        quit\n
#
#This first test will work much like a stack. We are pushing onto the
#back and popping off of the back.
={ } > 
<+b one
={ one } > 
<+b two
={ one two } > 
<+b three
={ one two three } > 
<?
=\tSize:     3\n
=\tCapacity: 4\n
={ one two three } > 
<-b
=\tpop: three\n
={ one two } > 
<-b
=\tpop: two\n
={ one } > 
<-b
=\tpop: one\n
={ } > 
<?
=\tSize:     0\n
=\tCapacity: 4\n
#
#Now we will be pushing onto the front. This will require our index
#to go negative. We will need some logic to turn a negative deque index
#into a positive array index
={ } > 
<+f alfa
={ alfa } > 
<+f beta
={ beta alfa } > 
<+f charlie
={ charlie beta alfa } > 
<?
=\tSize:     3\n
=\tCapacity: 4\n
#
#Now we will do a full wrap round the front. After we
#add the 4th item, we will remove an item off the back.
#The very next item we push off the front will be the 
#second wrap.
={ charlie beta alfa } > 
<+f delta
={ delta charlie beta alfa } > 
<-b
=\tpop: alfa\n
={ delta charlie beta } > 
<+f echo
={ echo delta charlie beta } > 
<?
=\tSize:     4\n
=\tCapacity: 4\n
#
#As we add the 5th item, we will cause a resize. In order to not mess
#up the order, we will have to unroll the deque so the front is at 0 again
={ echo delta charlie beta } > 
<+f foxtrot
={ foxtrot echo delta charlie beta } > 
<?
=\tSize:     5\n
=\tCapacity: 8\n
#
#Now we will unroll the deque to empty it
={ foxtrot echo delta charlie beta } > 
<-b
=\tpop: beta\n
={ foxtrot echo delta charlie } > 
<-f
=\tpop: foxtrot\n
={ echo delta charlie } > 
<-b
=\tpop: charlie\n
={ echo delta } > 
<-f
=\tpop: echo\n
={ delta } > 
<-b 
=\tpop: delta\n
#
#Empty again. Now we will put 6 items in the deque
={ } > 
<+b four
={ four } > 
<+f three
={ three four } > 
<+b five
={ three four five } > 
<+f two
={ two three four five } > 
<+b six
={ two three four five six } > 
<+f one
={ one two three four five six } > 
<+b seven
={ one two three four five six seven } > 
<+f zero
={ zero one two three four five six seven } > 
<+b eight
#
#one more reallocate. Again we must preserve the order
={ zero one two three four five six seven eight } > 
<?
=\tSize:     9\n
=\tCapacity: 16\n
={ zero one two three four five six seven eight } > 
<*
={ } > 
<?
=\tSize:     0\n
=\tCapacity: 16\n
={ } > 
<!
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Deque\n
=\t2. The above plus push, pop, top\n
=\t3. The above plus test implementation of wrapping\n
=\t4. The above plus exercise the error Deque\n
=\ta. Now Serving\n
=> 
<4
#
#Test front() from an empty deque
=\tDeque::front() error message correctly caught.\n
=\t"ERROR: unable to access data from an empty deque"\n
#
#Test back() from an empty deque
=\tDeque::back() error message correctly caught.\n
=\t"ERROR: unable to access data from an empty deque"\n
#
#Test pop_front from an empty deque
=\tDeque::pop_front() error message correctly caught.\n
=\t"ERROR: unable to pop from the front of empty deque"\n
#
#Test pop_back from an empty deque
=\tDeque::pop_back() error message correctly caught.\n
=\t"ERROR: unable to pop from the back of empty deque"\n
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Deque\n
=\t2. The above plus push, pop, top\n
=\t3. The above plus test implementation of wrapping\n
=\t4. The above plus exercise the error Deque\n
=\ta. Now Serving\n
=> 
<a
=Every prompt is one minute.  The following input is accepted:\n
=\t<class> <name> <#minutes>    : a normal help request\n
=\t!! <class> <name> <#minutes> : an emergency help request\n
=\tnone                         : no new request this minute\n
=\tfinished                     : end simulation\n
=<0> 
<cs124 Sam 2
=\tCurrently serving Sam for class cs124. Time left: 2\n
=<1> 
<none
=\tCurrently serving Sam for class cs124. Time left: 1\n
=<2> 
<none
=<3> 
<cs124 Sue 3
=\tCurrently serving Sue for class cs124. Time left: 3\n
=<4> 
<cs165 Steve 2
=\tCurrently serving Sue for class cs124. Time left: 2\n
=<5> 
<!! cs124 Joseph 1
=\tCurrently serving Sue for class cs124. Time left: 1\n
=<6> 
<none
=\tEmergency for Joseph for class cs124. Time left: 1\n
=<7> 
<none
=\tCurrently serving Steve for class cs165. Time left: 2\n
=<8> 
<cs124 Sam 1
=\tCurrently serving Steve for class cs165. Time left: 1\n
=<9> 
<none
=\tCurrently serving Sam for class cs124. Time left: 1\n
=<10> 
<none
=<11> 
<finished
=End of simulation\n
-






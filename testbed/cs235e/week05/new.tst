+
=Select the test you want to run:\n
=\t1. Just create and destroy a Set.\n
=\t2. The above plus fill and iterate through the Set.\n
=\t3. The above plus find if an item is in the Set.\n
=\t4. The above plus union and intersection.\n
=\ta. Go Fish!\n
=> 
<1
#Create, destory, and copy a Set
=Create a bool Set using default constructor\n
=\tSize:     0\n
=\tCapacity: 0\n
=\tEmpty?    Yes\n
=Create a double Set using the non-default constructor\n
=\tSize:     0\n
=\tCapacity: 10\n
=\tEmpty?    Yes\n
=Create a double Set using the copy constructor\n
=\tSize:     0\n
>\tCapacity: 10\n
=\tEmpty?    Yes\n
=Copy a double Set using the assignment operator\n
=\tSize:     0\n
>\tCapacity: 10\n
=\tEmpty?    Yes\n
=Test 1 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Set.\n
=\t2. The above plus fill and iterate through the Set.\n
=\t3. The above plus find if an item is in the Set.\n
=\t4. The above plus union and intersection.\n
=\ta. Go Fish!\n
=> 
<2
=Enter numbers, type 0 when done\n
#An empty list 
=\t{ } > 
<4
#Resize to capacity of one. Since 4's are duplicate, only the first is added
=\t{ 4 } > 
<4
=\t{ 4 } > 
<4
=\t{ 4 } > 
<2
#Resize to capacity of two
=\t{ 2 4 } > 
<6
#Resize to capacity of four where the last slot is empty
=\t{ 2 4 6 } > 
<3
=\t{ 2 3 4 6 } > 
<5
#Resize to capacity of eight were the last three are empty
=\t{ 2 3 4 5 6 } > 
<4
=\t{ 2 3 4 5 6 } > 
<4
=\t{ 2 3 4 5 6 } > 
<1
=\t{ 1 2 3 4 5 6 } > 
<7
=\t{ 1 2 3 4 5 6 7 } > 
<0
=\tSize:     7\n
=\tEmpty?    No\n
>\tCapacity: 8\n
#s1 is emptied but s2 and s3 should be faithful copies of the original s1
=\ts1 = { }\n
=\ts2 = { 1 2 3 4 5 6 7 }\n
=\ts3 = { 1 2 3 4 5 6 7 }\n
=Test 2 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Set.\n
=\t2. The above plus fill and iterate through the Set.\n
=\t3. The above plus find if an item is in the Set.\n
=\t4. The above plus union and intersection.\n
=\ta. Go Fish!\n
=> 
<3
=Enter text, type "quit" when done\n
#
#Display an empty set. The iterator should work with zero size
=\t{ } > 
<beta
#
#Test putting a single item in the set
=\t{ beta } > 
<alpha
#
#Since "alpha" comes before "beta", it should go in slot 0
=\t{ alpha beta } > 
<alpha
#
#Since "epsilon" comes last, we do not need to shift here
=\t{ alpha beta } > 
<epsilon
#
#"delta" goes in the middle of the set
=\t{ alpha beta epsilon } > 
<delta
=\t{ alpha beta delta epsilon } > 
<theta
=\t{ alpha beta delta epsilon theta } > 
<platypus
=\t{ alpha beta delta epsilon platypus theta } > 
<theta
=\t{ alpha beta delta epsilon platypus theta } > 
<upsilon
=\t{ alpha beta delta epsilon platypus theta upsilon } > 
<capybara
=\t{ alpha beta capybara delta epsilon platypus theta upsilon } > 
<quit
=Find items in the set and delete.\n
=Enter words to search for, type "quit" when done\n
#
#First find and remove capybara - the largest member of the rodent family
=\t{ alpha beta capybara delta epsilon platypus theta upsilon } > 
<capybara
=\tFound and removed!\n
#
#The second attempt to remove capybara will fail; it should be gone!
=\t{ alpha beta delta epsilon platypus theta upsilon } > 
<capybara
=\tNot found\n
#
#There is no tapir, a relative of the elephant
=\t{ alpha beta delta epsilon platypus theta upsilon } > 
<tapir
=\tNot found\n
#
#The platypus, proof that God has a sense of humor!
=\t{ alpha beta delta epsilon platypus theta upsilon } > 
<platypus
=\tFound and removed!\n
#Nothing but the Greek alphabet remains
=\t{ alpha beta delta epsilon theta upsilon } > 
<quit
=The remaining set after the items were removed\n
=\t{ alpha beta delta epsilon theta upsilon }\n
=The items in the set before the items were removed\n
=\t{ alpha beta capybara delta epsilon platypus theta upsilon }\n
=Test 3 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Set.\n
=\t2. The above plus fill and iterate through the Set.\n
=\t3. The above plus find if an item is in the Set.\n
=\t4. The above plus union and intersection.\n
=\ta. Go Fish!\n
=> 
<4
=First set: enter numbers, type 0.0 when done\n
=\t> 
<2
=\t> 
<4
=\t> 
<6
=\t> 
<8
=\t> 
<0
=Second set: enter numbers, type 0.0 when done\n
=\t> 
<3
=\t> 
<4
=\t> 
<5
=\t> 
<6
=\t> 
<7
=\t> 
<0
=s1 = { 2.0 4.0 6.0 8.0 }\n
=s2 = { 3.0 4.0 5.0 6.0 7.0 }\n
=s1 && s2 = { 4.0 6.0 }\n
=s1 || s2 = { 2.0 3.0 4.0 5.0 6.0 7.0 8.0 }\n
=Test 4 complete\n
-
+
=Select the test you want to run:\n
=\t1. Just create and destroy a Set.\n
=\t2. The above plus fill and iterate through the Set.\n
=\t3. The above plus find if an item is in the Set.\n
=\t4. The above plus union and intersection.\n
=\ta. Go Fish!\n
=> 
<a
=We will play 5 rounds of Go Fish.  Guess the card in the hand\n
=round 1: 
<Shark
=\tYou got a match!\n
=round 2: 
<Shark
=\tGo Fish!\n
=round 3: 
<Goldfish
=\tGo Fish!\n
=round 4: 
<Salmon
=\tGo Fish!\n
=round 5: 
<Cod
=\tYou got a match!\n
=You have 2 matches!\n
=The remaining cards: AngleFish, Crab, Dolphin, SeaHorse\n
-

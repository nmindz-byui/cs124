#!/usr/bin/perl

##
## NOTE: All makefile varients (makefile and .tar) will expect the 
##       executable to be called "out"
##

if (@ARGV[0] =~ /.*\.tar/)
{
    $lang = 0;
    system "mkdir @ARGV[0].dir";
    system "cp @ARGV[0] @ARGV[0].dir";
    system "tar -xf  @ARGV[0].dir/@ARGV[0] -C @ARGV[0].dir";
    system "make a.out -C @ARGV[0].dir 2> @ARGV[0].compile > /dev/null";
    system "mv @ARGV[0].dir/a.out @ARGV[0].exe";
    system "rm -rf @ARGV[0].dir";
}
elsif (@ARGV[0] = makefile)
{
    $lang = 0;
    system "make a.out 2>> /dev/null";
    system "make all 2>> /dev/null";
    system "cp a.out @ARGV[0].exe";
}
else
{
   #print "Invalid file format\n";
   exit 2;
}

open(COMP, "<@ARGV[0].compile");

if (<COMP>)
{
   $status = 1;
}

if ($lang == 0)
{
   open(EXE, "<@ARGV[0].exe");
}
elsif ($lang == 1)
{
   @filename = split (/\./, @ARGV[0]);
   $filename = shift @filename;
   #print "The filename is: ";
   #print "$filename";
   open(EXE, "<$filename.class");
}


if (! <EXE>)
{
   $status = 2;
}

#print $status + "\n";

exit $status;

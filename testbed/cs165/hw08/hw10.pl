#!/usr/bin/perl

#Author: Burdette Pixton
#Program: hw13.pl
#Summary: this will make a file that will be used for testing
#        for correct rational number seperator program (hw13)

open(TEMP, ">hw13.test");
print TEMP "+\n";

@correctIn = ("10/24-13/44","-5/-6--7/-8","15/-6+-8/90","12/34/-5/78");
@correctOut = ("10","24","-","13","44","-5","-6","-","-7","-8","15","-6","+",
               "-8","90","12","34","/","-5","78");
@error = ("1+2/3/4","1\2+3/4","1/2/3","1/2","1","1/2%3/4");
$num1 = "First numerator:    ";
$den1 = "First denominator:  ";
$op =   "Operator:            ";
$num2 = "Second numerator:   ";
$den2 = "Second denominator: ";
$err = "Parse error";
print TEMP "\$\n";
print TEMP "=Enter an arithmetic expression using fractions";
print TEMP "\n=   (such as 1/2+3/4) or quit";

$j = 0;

for($i = 0; $i < 4; $i++)
{
   print TEMP "\n=Input: ";
   print TEMP "\n<$correctIn[$i]";
   print TEMP "\n=$num1 $correctOut[$j]";
   $j++;
   print TEMP "\n=$den1 $correctOut[$j]";
   $j++;
   print TEMP "\n=$op $correctOut[$j]";
   $j++;
   print TEMP "\n=$num2 $correctOut[$j]";
   $j++;
   print TEMP "\n=$den2 $correctOut[$j]";
   $j++;
   print TEMP "\n\$";
}
for($i = 0; $i < 6; $i++)
{
   print TEMP "\n=Input: ";
   print TEMP "\n<$error[$i]";
   print TEMP "\n=$err";
   print TEMP "\n\$";
}

print TEMP "\n=Input: ";
print TEMP "\n<quit";
print TEMP "\n-";






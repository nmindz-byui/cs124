/***********************************************************************
* Program:
*    Assignment 43, Command Line
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary: 
*    This program will convert up to 5 inputs from command line
*    from feet to meters.
*
*    Estimated:  0.5 hrs   
*    Actual:    -0.5 hrs
*
*    Sorry for the rant on last header. I guess I was just
*    really disappointed about myself and Project 10 felt
*    like the assignments did not really help me prepare much.
*
************************************************************************/

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

/**********************************************************************
 * FTOM
 * Feets to Meter. Simple but effective.
 ***********************************************************************/
float ftom(float arg)
{
   return arg / 3.2808399;
}

/**********************************************************************
 * MAIN
 * In this program, MAIN is everything. Yay!
 ***********************************************************************/
int main(int argc, char * argv[])
{
   cout.setf(ios::fixed);     // no scientific notation please
   cout.setf(ios::showpoint); // always show the decimal for real numbers
   cout.precision(1);         // two digits after the decimal

   float arg1 = atof(argv[1]);
   float arg2 = atof(argv[2]);
   float arg3 = atof(argv[3]);
   float arg4 = atof(argv[4]);
   float arg5 = atof(argv[5]);

   cout << arg1 << " feet is " << ftom(arg1) << " meters" << endl;
   cout << arg2 << " feet is " << ftom(arg2) << " meters" << endl;
   cout << arg3 << " feet is " << ftom(arg3) << " meters" << endl;
   cout << arg4 << " feet is " << ftom(arg4) << " meters" << endl;
   cout << arg5 << " feet is " << ftom(arg5) << " meters" << endl;

   return 0;
}
/***********************************************************************
* Program:
*    Assignment 11, Output
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    The code in this assignment/program should be able to output
*    the projected monthly budget as outlined in the assignment
*    specification
*    
*    Estimated:  1.0 hrs   
*    Actual:     3.0 hrs
*
*      The most difficult part was, undoubtedly, getting the text
*      to align properly. The "setw" function is nowhere near
*      intuitive and very hard to troubleshoot. Took me a while.
*
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

/**********************************************************************
 * The main subroutine of the application,
 * formats and outputs the monthly budget
 ***********************************************************************/
int main()
{
   cout.setf(ios::fixed);     // no scientific notation please
   cout.setf(ios::showpoint); // always show the decimal for real numbers
   cout.precision(2);         // two digits after the decimal

   // Displays the first row
   cout << "\t"
        << "Item"
        << setw(20)
        << "Projected"
        //<< setw(10)
        << endl;

   // Displays the second row
   cout << "\t"
        << "============="
        << "  "
        << "=========="
        << endl;

   // Displays the third row
   cout << "\t"
        << "Income"
        << setw(10)
        << "$" << setw(9) << 1000.00
        << endl;

   // Displays the fourth row
   cout << "\t"
        << "Taxes"
        << setw(11)
        << "$" << setw(9) << 100.00
        << endl;

   // Displays the fifith row
   cout << "\t"
        << "Tithing"
        << setw(9)
        << "$" << setw(9) << 100.00
        << endl;

   // Displays the sixth row
   cout << "\t"
        << "Living"
        << setw(10)
        << "$" << setw(9) << 650.00
        << endl;

   // Displays the seventh row
   cout << "\t"
        << "Other"
        << setw(11)
        << "$" << setw(9) << 90.00
        << endl;

   // Displays the eighth row
   cout << "\t"
        << "============="
        << "  "
        << "=========="
        << endl;

   // Displays the ninth row
   cout << "\t"
        << "Delta"
        << setw(11)
        << "$" << setw(9) << 60.00
        << endl;

   return 0;
}

/***********************************************************************
* Program:
*    Assignment 42, Dem Bones
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary: 
*    This program will display the words to the song Dem Bones by
*    using the string class
*
*    Estimated:  0.5 hrs   
*    Actual:    -0.5 hrs
*
*    It almost feels like it was too easy.
*    Is there a catch or something?
*
*    I feel like assignments/projects should have a linear
*    level of difficulty, instead of being too easy and then
*    way too hard/complex/time consuming at once. (assign VS project)
*
*    And, for all that matters, *please* request for someone
*    to FIX STYLECHECKER! I had to make an styling abortion
*    down at main() to make it pass! That makes no sense at all.
*
*    Or just drop the requirement of "have no styleChecker errors". >:(
*
************************************************************************/

#include <iostream>
#include <string>
using namespace std;

/********************************************************************
 * GENERATE SONG
 * This function will generate one long string that constitutes the
 * complete song "Dem Bones."  Note that the list of bones is provided
 * by the parameter list
 ********************************************************************/
string generateSong(string list[], int num)
{
   string output = "";
   string part1 = " bone connected to the ";
   string part2 = " bone\n";

   for (int i = 0; i < 8; i++)
   {
      output += list[i] + part1 + list[i + 1] + part2;
   }

   return output;
}

/**********************************************************************
 * MAIN
 * In this program, MAIN is a driver program.  It will feed to generateSong()
 * the list of bones and get back the complete song as a string.  MAIN will
 * then display the song
 ***********************************************************************/
int main()
{
   string list[9] =
   {
      "toe",
      "foot",
      "leg",
      "knee",
      "hip",
      "back",
      "neck",
      "jaw",
      "head"
   };

   // generate the song
   string song = generateSong(list, 9);

   // display the results
   cout << song;

   return 0;
}
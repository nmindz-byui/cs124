/***********************************************************************
* Program:
*    Assignment 34, Pointer Arithmetic
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    Manipulating strings, part 1 of 2
*    
*    Estimated:  0.50 hrs   
*    Actual:     0.50 hrs
*
*     Neat, pointer magixxx! :D 
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <string>

using std::string;
using namespace std;

/******************************************************************************
* Checks for and counts each letter occurrence in a string
******************************************************************************/
int countLetters(char letter, char* text)
{
   int count = 0;
   int i = 0;

   while (true)
   {
      if (*(text + i) == letter)
      {
         count++;
      }

      if (*(text + i) == '\0')
      {
         break;
      }

      i++;
   }
   return count;
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   char letter;
   char text[256];

   cout << "Enter a letter: ";
   cin >> letter;
   cin.ignore();

   cout << "Enter text: ";
   cin.getline(text, 256);

   cout << "Number of '"
      << letter << "'s: "
      << countLetters(letter, text)
      << endl;

   return 0;
}
/***********************************************************************
* Program:
*    Assignment 33, Pointers
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    Manipulating strings, part 1 of 2
*    
*    Estimated:  0.50 hrs   
*    Actual:     0.50 hrs
*
*     Piece of cake! 
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <string>

using std::string;
using namespace std;

/******************************************************************************
* Handles the dating costs
******************************************************************************/
void date(float *pAccount)
{
   float dinner = 0.00;
   float movie = 0.00;
   float icecream = 0.00;

   cout << "Cost of the date:" << endl;
   cout << "\tDinner:    ";
   cin >> dinner;
   cout << "\tMovie:     ";
   cin >> movie;
   cout << "\tIce cream: ";
   cin >> icecream;

   float debit = dinner + movie + icecream;

   *pAccount = *pAccount - debit;
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   float sam = 0.00;
   float sue = 0.00;

   cout << "What is Sam's balance? ";
   cin >> sam;
   cout << "What is Sue's balance? ";
   cin >> sue;

   date(sam > sue ? &sam : &sue);

   cout << "Sam's balance: $" << sam << endl;
   cout << "Sue's balance: $" << sue << endl;

   return 0;
}
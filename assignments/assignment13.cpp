/***********************************************************************
* Program:
*    Assignment 13, Expressions
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    This exercise will prompt the user for the temperature in Fahrenheit
*    and provide the converted equivalent in Celsius.
*    
*    Estimated:  1.0 hrs   
*    Actual:     0.5 hrs
*
*      Took me forever to realize you should only *output* as integer
*      but that everything else should actually be done as a float...
*
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

/**********************************************************************
 * Converts Fahrenheit to Celsius
 ***********************************************************************/
int main()
{
   cout.setf(ios::fixed);   // no scientific notation please
   cout.precision(0);       // always round up
   float fahrenheit = 0.00; // C = 5/9(F – 32)

   cout << "Please enter Fahrenheit degrees: ";

   // Stores the temperature value from stdin
   cin >> fahrenheit;

   // Calculates the C temperature from F
   float celsius = (5.0 / 9.0) * (fahrenheit - 32.00);

   cout << "Celsius: "
        << celsius
        << endl;

   return 0;
}

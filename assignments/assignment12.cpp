/***********************************************************************
* Program:
*    Assignment 12, Input & Variables
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    This exercise will prompt for the user's income, store the value in
*    variables and give back the formatted response with the inputted
*    information.
*    
*    Estimated:  1.0 hrs   
*    Actual:     1.0 hrs
*
*      Hardest part
*
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

/**********************************************************************
 * The main subroutine of the application, sets parameters, takes
 * input and formats back a response
 ***********************************************************************/
int main()
{
   cout.setf(ios::fixed);     // no scientific notation please
   cout.setf(ios::showpoint); // always show the decimal for real numbers
   cout.precision(2);         // two digits after the decimal

   // Declares the 'income' variable which will be used to retrieve user input
   float income = 0.00;

   cout << "\t"
        << "Your monthly income: ";

   // Stores the income value from stdin
   cin >> income;

   cout << "Your income is: $"
        << setw(9)
        << income
        << endl;

   return 0;
}

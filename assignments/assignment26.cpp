/***********************************************************************
* Program:
*    Assignment 26, Files
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    A small program to read grades from a file and calculate the average
*    
*    Estimated:  0.5 hrs   
*    Actual:     0.5 hrs
*
*     Reading from a file I guess, that's something new. :) 
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

using std::string;
using namespace std;

/******************************************************************************
* exception genericFileError
******************************************************************************/
int genericFileError(char fileName[])
{
   cout << "Error reading file \"" << fileName << "\"" << endl;

   return -1;
}

/******************************************************************************
* function getFileName
******************************************************************************/
char* getFileName(char* fileName)
{
   cout << "Please enter the filename: ";
   cin >> fileName;

   return fileName;
}

/******************************************************************************
* function displayAverage
******************************************************************************/
void display(float average)
{
   cout.setf(ios::fixed);     // no scientific notation please
   cout.precision(0);         // two digits after the decimal

   cout << "Average Grade: " << average << "%" << endl;
}

/******************************************************************************
* function readFile
******************************************************************************/
float readFile(char fileName[])
{
   ifstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      return genericFileError(fileName);
   }

   float sum = 0.00;
   int grade, count = 0;

   while (file >> grade)
   {
      sum += grade;
      count++;
   }

   if (count != 10)
   {
      return genericFileError(fileName);
   }

   file.close();

   return sum / 10;
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   char fileName[256];
   getFileName(fileName);

   float average = readFile(fileName);

   if (average < 0) {
      return 1;
   }

   display(average);
   
   return 0;
}
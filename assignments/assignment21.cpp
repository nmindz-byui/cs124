/***********************************************************************
* Program:
*    Assignment 21, Debugging
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    The whole program consists of a couple stubs of functions that
*    were mocked in preparation for another assignment.
*    
*    Estimated:  0.2 hrs   
*    Actual:     0.3 hrs
*
*      :)
*
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

/******************************************************************************
 * Gets the Mileage input from user
 *****************************************************************************/
double promptMileage()
{
   double mileage = 0.00;

   return mileage;
}

/******************************************************************************
 * Gets the Gas input from user
 *****************************************************************************/
double promptGas()
{
   double cost = 0.00;

   return cost;
}

/******************************************************************************
 * Gets the Repairs input from user
 *****************************************************************************/
double promptRepairs()
{
   double cost = 0.00;

   return cost;
}

/******************************************************************************
 * Gets the Tires input from user
 *****************************************************************************/
double promptTires()
{
   double cost = 0.00;

   return cost;
}

/******************************************************************************
 * Gets the Devalue input from user
 *****************************************************************************/
double promptDevalue()
{
   double cost = 0.00;

   return cost;
}

/******************************************************************************
 * Gets the Insurance input from user
 *****************************************************************************/
double promptInsurance()
{
   double cost = 0.00;

   return cost;
}

/******************************************************************************
 * Gets the Parking input from user
 *****************************************************************************/
double promptParking()
{
   double cost = 0.00;

   return cost;
}

/******************************************************************************
 * Calculates the periodic costs
 *****************************************************************************/
double getPeriodicCost()
{
   double costPeriodic = 0.00;

   costPeriodic += promptDevalue();
   costPeriodic += promptInsurance();
   costPeriodic += promptParking();

   return costPeriodic;
}

/******************************************************************************
 * Calculates the usage costs
 *****************************************************************************/
double getUsageCost()
{
   double costUsage = 0.00;

   costUsage += promptMileage();
   costUsage += promptGas();
   costUsage += promptRepairs();
   costUsage += promptTires();

   return costUsage;
}

/******************************************************************************
 * Displays the formatted output
 *****************************************************************************/
void display(double periodicCost, double usageCost)
{
   cout << "Success\n";
   
   return;
}

/******************************************************************************
 * The application entrypoint. The core. The beginning and the end.
 *****************************************************************************/
int main()
{
   display(getPeriodicCost(), getUsageCost());

   return 0;
}
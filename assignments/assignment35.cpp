/***********************************************************************
* Program:
*    Assignment 35, Advanced Conditionals
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    Exercise on switches
*    
*    Estimated:  0.50 hrs   
*    Actual:     0.50 hrs
*
*     The textbook gave me a nice idea!
*
************************************************************************/
#include <iomanip>
#include <iostream>

using namespace std;

/******************************************************************************
* computeLetterGrade()
* Write a function to return the letter grade from a number grade. The input
* will be an integer, the number grade. The output will be a character, the
* letter grade. You must use a switch statement for this function. Please see
* the syllabus for the meaning behind the various letter grades.
*
* 100% → 90% 	 A:  	Demonstrated mastery of the class
* 89.9% → 80% 	 B:  	All of the key concepts and skills have been learned
* 79.9% → 70% 	 C:  	Acceptable, but might not be ready or CS 165
* 69.9% → 60% 	 D:  	Developing; the class has yet to be mastered
* 59.9% → 0% 	 F:  	Failed to understand or complete the course
******************************************************************************/
char computeLetterGrade(int grade)
{
   switch (grade / 10)
   {
      case 10:
      case 9:
         return 'A';
         break;
      case 8:
         return 'B';
         break;
      case 7:
         return 'C';
         break;
      case 6:
         return 'D';
         break;
      default:
         return 'F';
   }
}

/******************************************************************************
* computeGradeSign()
* Write another function to return the grade sign (+ or -) from a number grade.
* The input will be the same as with computeLetterGrade() and the output will
* be a character. If there is no grade sign for a number grade like 85%=B,
* then return the symbol '*'. You must use at least one conditional expression.
* Please see the syllabus for the exact rules for applying the grade sign.
*
* Additionally, a minus (-) will be added when the last digit is a 0, 1, or 2
* for all grades except F's. A plus (+) will be added when the last digit is
* a 7, 8, or 9 for all grades except A's and F's.
******************************************************************************/
void computeGradeSign(int grade)
{
   if (grade < 60 || grade > 92)
   {
      cout << endl;
      return;
   }

   switch (grade % 10)
   {
      case 9:
      case 8:
      case 7:
         cout << "+" << endl;
         break;
      case 2:
      case 1:
      case 0:
         cout << "-" << endl;
         break;
      default:
         cout << endl;
         break;
   }
}

/******************************************************************************
* main()
* Create a main() that prompts the user for a number
* graded then displays the letter grade.
******************************************************************************/
int main()
{
   int grade = 0;

   cout << "Enter number grade: ";
   cin >> grade;

   cout << grade
      << "\% is "
      << computeLetterGrade(grade);

   computeGradeSign(grade);

   return 0;
}
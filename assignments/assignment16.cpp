/***********************************************************************
* Program:
*    Assignment 16, If Statements
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    This program works taking your income and deciding on which
*    tax bracket you belong according to US Tax regulations,
*    specified by the assignment spec sheet.
*    
*    Estimated:  0.5 hrs   
*    Actual:     0.5 hrs
*
*      The hardest piece of this assignment, to me, was computeTax
*      since the numbers seemed a bit "unreal", but it might just be
*      me not being used to US tax regulations.
*
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

/******************************************************
 *  function computeTax - defines tax bracket according to income
 *****************************************************/
double computeTax(double &income)
{
   double taxPercent = 0.00;

   if (income > 336550.00)
   {
      taxPercent = 0.35;
   }
   else if (income > 188450.00)
   {
      taxPercent = 0.33;
   }
   else if (income > 123700.00)
   {
      taxPercent = 0.28;
   }
   else if (income > 61300.00)
   {
      taxPercent = 0.25;
   }
   else if (income > 15100.00)
   {
      taxPercent = 0.15;
   }
   else if (income > 0.00)
   {
      taxPercent = 0.10;
   }

   return taxPercent;
}

/******************************************************************************
 * The application entrypoint. The core. The beginning and the end.
 *****************************************************************************/

/******************************************************
 *  function main 
 *****************************************************/
int main()
{
   cout.setf(ios::fixed);     // no scientific notation please
   cout.precision(0);         // two digits after the decimal

   double income = 0.00;

   // Ask for income
   cout << "Income: ";
   cin >> income;

   cout << "Your tax bracket is " << computeTax(income) * 100 << '%' << endl;

   return 0;
}
/***********************************************************************
* Program:
*    Assignment 44, Search Speed
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary: 
*    This program compares the search speed of the linear search
*    and the binary search.
*
*    Estimated:  0.0 hrs   
*    Actual:     0.0 hrs
*
*    Sometimes I feel like this header is like a those quick
*    emails one would write a friend or relative during their
*    mission, that would only get read later and you would be
*    left wondering what their reaction was until later next
*    week. lol
*
************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

int readNumbers(int* list, int max);
float computeAverageLinear(int* pList, const int MAX);
float computeAverageBinary(int* pList, const int MAX);
int linear(int* pList, int num, int search);
int binary(int* pList, int num, int search);

/**********************************************************************
* main reads the list, determines the speed of the searches, and outputs
* the results.
***********************************************************************/
int main()
{
   const int MAX = 1024;
   int pList[MAX];
   int num;

   // read the numbers
   if (!(num = readNumbers(pList, MAX)))
      return 1;

   // determine how long it takes for a linear search
   float averageLinear;
   averageLinear = computeAverageLinear(pList, num);

   // determine how long it takes for a binary search
   float averageBinary;
   averageBinary = computeAverageBinary(pList, num);

   // display results
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(1);
   cout << "Linear search: " << setw(10) << averageLinear << endl;
   cout << "Binary search: " << setw(10) << averageBinary << endl;

   return 0;
}

/************************************************************
 * READ NUMBERS
 * Input:
 *    list:    a list of numbers to search through
 *    max:     the size of the numbers list
 * Output:
 *    num:     the number of items actually read
 ***********************************************************/
int readNumbers(int* list, int max)
{
   char fileName[256];
   int  num = 0;

   // get the filename
   cout << "Enter filename of list: ";
   cin  >> fileName;

   // open the file
   ifstream fin(fileName);
   if (fin.fail())
   {
      cout << "Unable to open file " << fileName << endl;
      return 0;
   }

   // read the file
   while (num < max && fin >> list[num])
   {
      num++;
   }

   // make like a tree
   fin.close();
   return num;
}

/*********************************************************
 * COMPUTE AVERAGE LINEAR
 * Input:
 *    list:    a list of numbers to search through
 *    num:     the size of the numbers list
 * Output:
 *    averageLinear: the average number of comparisons it takes
 *                   to find each item in the array
 *******************************************************/
float computeAverageLinear(int* pList, const int MAX)
{
   float averageLinear = 0.0;

   for (int i = 0; i < MAX; i++)
   {
      averageLinear += linear(pList, MAX, pList[i]);
   }

   return averageLinear / MAX;
}

/*********************************************************
 * COMPUTE AVERAGE BINARY
 * Input:
 *    list:    a list of numbers to search through
 *    num:    the size of the numbers list
 * Output:
 *    averageBinary: the average number of comparisons it takes
 *                   to find each item in the array
 *******************************************************/
float computeAverageBinary(int* pList, const int MAX)
{
   float averageBinary = 0.0;

   for (int i = 0; i < MAX; i++)
   {
      averageBinary += binary(pList, MAX, pList[i]);
   }

   return averageBinary / MAX;
}

/*******************************************************
 * LINEAR
 * Input:
 *    list:    a list of numbers to search through
 *    num:     the size of the numbers list
 *    search:  the number that you are searching for
 * Output:
 *    compares: the number of compares that were made
 *              before 'search' was found in the 'numbers' array
 ******************************************************/
int linear(int* pList, int num, int search)
{
   bool found = false;
   int  compares = 0;  // you will need to compute this

   for (int i = 0; i < num && ! found; i++)
   {
      if (search == pList[i])
      {
         found = true;
      }
      compares++;
   }
   return compares;
}

/*******************************************************
 * BINARY
 * Input:
 *    list:    a list of numbers to search through
 *    num:     the size of the numbers list
 *    search:  the number that you are searching for
 * Output:
 *    compares: the number of compares that were made
 *              before 'search' was found in the 'numbers' array
 ******************************************************/
int binary(int* pList, int num, int search)
{
   bool found = false;
   int  compares = 0;  // you will need to compute this

   // set the bounds of the search space, initially the whole list
   int iFirst = 0;
   int iLast = num - 1;

   // continue until found or the search size is not zero
   while (iLast >= iFirst && !found)
   {
      int iMiddle = (iLast + iFirst) / 2;

      // note that both the == and > count as one comparison
      if (pList[iMiddle] == search)
      {
         found = true;
      }
      else if (pList[iMiddle] > search)
      {
         iLast = iMiddle - 1;
      }
      else
      {
         iFirst = iMiddle + 1;
      }
      compares++;
   }

   return compares;
}

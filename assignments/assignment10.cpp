/***********************************************************************
* Program:
*    Assignment 10, Hello World
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    This is a simple C++ program that is expected to write
*    "Hello World" to STDOUT.
*
*    Estimated:  0.5 hrs   
*    Actual:     0.1 hrs
*      Please describe briefly what was the most difficult part.
************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * The program's entrypoint function - main() - makes use of the
 * standard I/O library in C/C++ to write "Hello World" to STDOUT
 ***********************************************************************/
int main()
{
   // Echoes "Hello World"
   cout << "Hello World\n";
  
   return 0;
}

/***********************************************************************
* Program:
*    Assignment 40, Multi-Dimensional Arrays
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    A Multi-D char array exercise... to teach us how to handle those,
*    just after a massive project that basically depended on it.
*    Thanks, BYU-I.
*    
*    Estimated:  1.25 hrs   
*    Actual:     0.50 hrs
*
*     Writing feedback on Project 10 in I-Learn.
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;

/******************************************************************************
* exception genericFileError
******************************************************************************/
int genericFileError(char fileName[], const char* OPERATION)
{
   cout << "Error " << OPERATION << " file \"" << fileName << "\"" << endl;

   return -1;
}

/******************************************************************************
* function printRow()
******************************************************************************/
void printRow()
{
   cout << "---+---+---" << endl;
}

/******************************************************************************
* Prints the board on screen
******************************************************************************/
int printBoard(char (*pBoard)[3])
{
   for (int i = 0; i < 3; i++)
   {
      cout << " " << pBoard[i][0]
         << " | " << pBoard[i][1]
         << " | " << pBoard[i][2]
         << " " << endl;

      if (i < 2)
      {
         printRow();
      }
   }
}

/******************************************************************************
* Replaces cursor char with appropriate value, reading
******************************************************************************/
char rfReplace(const char CURSOR)
{
   switch (CURSOR)
   {
      case '.':
         return ' ';
         break;
      default:
         return CURSOR;
         break;
   }
}

/******************************************************************************
* Replaces cursor char with appropriate value, writing
******************************************************************************/
char wfReplace(const char CURSOR)
{
   switch (CURSOR)
   {
      case ' ':
         return '.';
         break;
      default:
         return CURSOR;
         break;
   }
}

/******************************************************************************
* Writes the board back to another file
******************************************************************************/
int writeFile(char fileName[], char (*pBoard)[3])
{
   ofstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      return genericFileError (fileName, "writing");
   }

   for (int i = 0; i < 3; i++)
   {
      file << wfReplace(pBoard[i][0]) << " "
         << wfReplace(pBoard[i][1]) << " "
         << wfReplace(pBoard[i][2]);

      if (i < 2)
      {
         file << endl;
      }
   }

   file.close();

   cout << "File written" << endl;

   return 0;
}

/******************************************************************************
* Reads the file and writes back to the board pointer
******************************************************************************/
int readFile(char fileName[], char (*pBoard)[3])
{
   ifstream file;          // declare the file stream
   file.open(fileName);    // open the file by referencing the variable.

   if (file.fail())
   {
      return genericFileError (fileName, "reading");
   }

   int row = 0;
   int c = 0;
   char cursor;

   while (file >> cursor)
   {
      if (c > 2)
      {
         c = 0;
         row++;
      }
      pBoard[row][c] = rfReplace(cursor);
      c++;
   }

   return 0;
}

/******************************************************************************
* main() - It all needs to begin somewhere right
* Keep things as clean as possible, main pointers that will
* be used throughout the program (such as the board) and function
* calls to print and format (read/write)
******************************************************************************/
int main()
{
   char filename[256];
   char board[3][3];
   char (*pBoard)[3] = board;

   cout << "Enter source filename: ";
   cin.getline(filename, 256);

   int read = readFile(filename, pBoard);
   if (read != 0)
   {
      return read;
   }

   printBoard(pBoard);

   cout << "Enter destination filename: ";
   cin.getline(filename, 256);

   int written = writeFile(filename, pBoard);
   if (written != 0)
   {
      return written;
   }

   return 0;
}
/***********************************************************************
* Program:
*    Assignment 40, Multi-Dimensional Arrays
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    Well, even though I failed Project10, it taught me a lot
*    about pointers (or so I think :D) and does make it all
*    sound a lot more reasonable than before.
*    
*    Estimated:  1.00 hrs   
*    Actual:     0.50 hrs
*
*     Hmm. Project 10 was the hardest part. Hahahaha.
*
************************************************************************/
#include <iomanip>
#include <iostream>

using namespace std;


/******************************************************************************
* Here is where the magic happens
******************************************************************************/
int main()
{
   // How many characters will need to be allocated
   int bytes = 0;

   cout << "Number of characters: ";
   // Get input and clear the stream
   cin >> bytes;
   cin.ignore();

   // Attempts to allocate necessary space
   char* input = new (nothrow) char[bytes + 1];

   // Checks for allocation and returns in case it fails
   if (input == NULL)
   {
      cout << "Allocation failure!" << endl;
      return 1;
   }
   
   // Get input text
   cout << "Enter Text: ";
   cin.getline(input, bytes + 1);

   // Displays input text within the allocated range
   cout << "Text: "
      << input
      << endl;

   return 0;
}
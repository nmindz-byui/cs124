/***********************************************************************
* Program:
*    Assignment 25, Loop Design
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    This is a stub from a calendar project function
*    
*    Estimated:  0.5 hrs   
*    Actual:     0.5 hrs
*
*     The hard thing to get right was making the rule of breaking
*     the lines based on their weekday offset align to the table
*     columns; that took me a while to refine.  
*
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

/******************************************************************************
* Function responsible for calculating display offsets and printing
* the calendar in a table format
*****************************************************************************/
void displayTable(int numDays, int offset)
{
   int padding = 2 * 2;
   int daysPadding = 0;

   if(offset < 6)
   {
      daysPadding = padding * (offset+1);
   }

   int day = 1;
   int whileOffset = offset;
   
   string header = "  Su  Mo  Tu  We  Th  Fr  Sa";
   cout << header << endl;
   cout << setw(daysPadding) << "";
   
   // Make first row end and align properly
   ++whileOffset;
   
   while (day <= numDays)
   {
      while (whileOffset < 7)
      {
         if (day > numDays) { break; }
         cout << setw(padding) << day;
         whileOffset += 1;
         day += 1;
      }
      whileOffset = 0;

      // Treats the exception case when the week starts in a Sunday,
      // avoiding a breakline in the first row right after day 1
      if (offset == 6 && day == 2) { whileOffset++; continue; }

      cout << endl;
   }
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
*****************************************************************************/
int main()
{
   int numDays;
   int offset;

   cout << "Number of days: ";
   cin >> numDays;
   
   cout << "Offset: ";
   cin >> offset;
   
   displayTable(numDays, offset);
   
   return 0;
}
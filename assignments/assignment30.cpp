/***********************************************************************
* Program:
*    Assignment 30, Array Syntax
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    An array exercise to calculate grades
*    
*    Estimated:  0.50 hrs   
*    Actual:     0.50 hrs
*
*     Nothing much I guess 
*
************************************************************************/
#include <iomanip>
#include <iostream>
#include <string>

using std::string;
using namespace std;

/******************************************************************************
* Gets user input as an array of grades
******************************************************************************/
void getGrades(int grades[])
{
   int i = 0;
   while (i < 10)
   {
      cout << "Grade " << i + 1 << ": ";
      cin >> grades[i];
      i++;
   }
}

/******************************************************************************
* The average of all grades
******************************************************************************/
int averageGrades(int grades[])
{
   int i = 0;
   int sum = 0;
   while (i < 10)
   {
      sum += grades[i];
      i++;
   }
   return sum / 10;
}

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
   int i = 0;
   int grades[10];
   getGrades(grades);

   int average = averageGrades(grades);
   cout << "Average Grade: " << average << '%' << endl;
   
   return 0;
}
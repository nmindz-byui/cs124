/***********************************************************************
* Program:
*    Assignment 23, Loop Syntax
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    Sue wrote this to help her silly brother, Steve.
*    
*    Estimated:  0.5 hrs   
*    Actual:     0.5 hrs
*
*      :)
*
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
*****************************************************************************/
int main()
{
   int loop = 100;
   int sum = 0;
   int number = 0;
   
   cout << "What multiples are we adding? ";
   cin >> number;
   
   while (loop > 0)
   {
      loop--;
      loop % number != 0 ?: sum += loop;
   }
   
   cout << "The sum of multiples of " << number << " less than 100 are: " << sum << endl;
   
   return 0;
}
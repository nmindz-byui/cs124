/***********************************************************************
* Program:
*    Week #09, Bonus Assignment
*    Brother Schwieder, CS124
* Author:
*    Evandro Camargo
* Summary:
*    Bonus Week #09 Developer Forum
*    
*    Estimated:  0.50 hrs   
*    Actual:     0.50 hrs
*
*     Follow the map:
*
*         ##       3,2
*        ####      2,4
*       ######     1,6
*      ########    0,8
*      ########    0,8
*       ######     1,6
*        ####      2,4
*         ##       3,2 
*
************************************************************************/
#include <iostream>

using namespace std;

/******************************************************************************
* The application entrypoint. The core. The beginning and the end.
******************************************************************************/
int main()
{
    int columns = 8;
    int loop = 1;
    int skip = 1;
    int reverse = 0;

    while (loop > 0)
    {
        int hashtags = 2 * loop;
        int spaces = 3 - loop + 1;
        int c_row = 1;

        while (c_row < columns)
        {
            while (spaces > 0)
            {
                cout << " ";
                spaces--;
            }
            while (hashtags > 0)
            {
                cout << "#";
                hashtags--;
            }
            c_row++;
        }
        cout << "\n";

        loop > 3 ? reverse = 1 : 0;
        if (loop == columns / 2 && skip) { skip = 0; continue; }
        reverse ? loop-- : loop++;
    }
}
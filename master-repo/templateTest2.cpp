/***********************************************************************
* Program:
*    Test 2, ????          (e.g. Test 2, Flip a coin)  
*    Brother {Cook, Comeau, Falin, Lilya, Unsicker, Phair}, CS124
* Author:
*    your name
* Summary: 
*    Enter a brief description of your program here!  Please note that if
*    you do not take the time to fill out this block, YOU WILL LOSE POINTS.
*    Before you begin working, estimate the time you think it will
*    take you to do the assignment and include it in this header block.
*
************************************************************************/

#include <iostream>   // for CIN, COUT
#include <ctime>      // for time(), part of the random process
#include <stdlib.h>   // for rand() and srand()
using namespace std;

/**********************************************************************
 * Add text here to describe what the function "main" does. Also don't forget
 * to fill this out with meaningful text or YOU WILL LOSE POINTS.
 ***********************************************************************/
int main(int argc, char **argv)
{
   // this code is necessary to set up the random number generator. If
   // your program uses a random number generator, you will need this 
   // code. Otherwise, you can safely delete it.  Note: this must go in main()
   srand(argc == 1 ? time(NULL) : (int)argv[1][1]);

   // this code will actually generate a random number between 0 and 999
   cout << rand() % 1000 << endl;
   
   return 0;
}

/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate counter-controlled loops through generating an
 *      ASCII table
 ************************************************************************/

#include <iostream>    // for COUT
#include <iomanip>     // for SETW
using namespace std;

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // display the header
   cout << "Decimal value     Hex value     ASCII glyph\n";
   
   // loop through all the values from ' ' through 'z' on the ASCII Table
   for (char character = ' '; character <= 'z'; character++)
      cout << dec << setw(10) << (int)character
           << hex << setw(15) << (int)character
           <<        setw(15) << character
           << endl;

   return 0;
}


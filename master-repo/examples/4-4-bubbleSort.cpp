/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate the bubble sort
 *      Demonstrate how to instrument a function: the bubble sort!
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

int getNumItems();                               // size of the list 
int *allocate(int numItems);                     // allocate the list
void fillList(int *list, int numItems);          // prompt the user for data
void displayList(const int *list, int numItems); // display the list
int  sort(int array[], int numElements);         // sort using Bubble Sort

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // get the size of the list
   int numItems = getNumItems();
   assert(numItems > 0);

   // allocate and fill
   int *list = allocate(numItems);
   if (list == NULL)
      return 1;
   assert(list != NULL);
   fillList(list, numItems);

   // sort
   int numCompares = 0;
   numCompares = sort(list, numItems);
   cout << "The list was sorted with "
        << numCompares
        << " comparisions\n";

   // show the results
   displayList(list, numItems);

   // make like a tree
   delete [] list;
   return 0;
}


/***************************************
 * SORT
 * The bubble sort!
 **************************************/
int sort(int array[], int numElements)
{
   int numCompares = 0;   // number of comparisions is initially zero
   
   // If we make it all the way through the outer loop represented by
   // iSpot without performing a swap, then we are sorted.  This variable
   // keeps track of that.  It is initially true to force us to go through
   // the inner looop (iCheck) at least once.
   bool switched = true;

   // The outer loop checks each spot in the array and looks for the
   // item to go there.
   for (int iSpot = numElements - 1; iSpot >= 1 && switched; iSpot--)

      // The inner loop brings the correct item to the spot.  This is done
      // by "bubbling" the item to the correct location.
      for (int iCheck = 0, switched = false; iCheck <= iSpot - 1; iCheck++)
      {
         numCompares++;      // each time we are going to compare, add one
         
         // If a pair is out of order, swap them.
         if (array[iCheck] > array[iCheck + 1])
            {
               // swap involves a temp variable because a variable can only
               // hold one item at a time.
               int temp = array[iCheck];
               array[iCheck] = array[iCheck + 1];
               array[iCheck  + 1] = temp;

               // once a swap has occured, set switched to true so we know
               // we need to go through the outer loop again.
               switched = true;
            }
      }
   
   return numCompares;
}


/********************************************
 * GET NUM ITEMS
 * Prompt the user for the number of items in his list
 *******************************************/
int getNumItems()
{
   int num;
   do
   {
      cout << "How many items? ";
      cin  >> num;
   }
   while (num <= 0); // better be greater than zero!

   return num;
}

/*****************************************
 * ALLOCATE
 * Allocate the necessary memory
 *****************************************/
int *allocate(int numItems)
{
   assert(numItems > 0);
   // Allocate the necessary memory
   int *p = new(nothrow) int[numItems];

   // if p == NULL, we failed to allocate
   if (!p)
      cout << "Unable to allocate " << numItems * 4 << " bytes of memory\n";

   return p;
}

/***********************************
 * FILL LIST
 * Prompt the user for the values in the list
 ***********************************/
void fillList(int *list, int numItems)
{
   // paranioa
   assert(list != NULL);
   assert(numItems > 0);

   // fill the data
   cout << "Please enter " << numItems << " values\n";
   for (int i = 0; i < numItems; i++)
   {
      cout << "\t# " << i + 1 << ": ";
      cin  >> list[i];
   }
}

/**********************************
 * DISPLAY LIST
 * Show the user all the data in the list
 **********************************/
void displayList(const int *list, int numItems)
{
   // paranioa
   assert(list != NULL);
   assert(numItems > 0);

   cout << "A display of the list:\n";
   for (int i = 0; i < numItems; i++)
      cout << "\t" << list[i] << endl;
}

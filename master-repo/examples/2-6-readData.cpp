/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to read data from a file.  In this case,
 *      the data is a string and an integer:
 *           Sue 19
 ************************************************************************/

#include <iostream>     // for COUT and CIN
#include <fstream>      // for IFSTREAM 
using namespace std;

/********************************************
 * READ
 * Read a name and an age. Return FALSE for
 * error and TRUE for success
 *******************************************/
bool read(char fileName[])
{
   // open the file for reading
   ifstream fin(fileName);              // connect to "fileName"
   if (fin.fail())                      // never forget to check for errors
   {
      cout << "Unable to open file "    // inform user of the error
           << fileName
           << endl;
      return false;                     // return and report
   }
   
   // do the work
   char userName[256];
   int  userAge;
   fin >> userName >> userAge;          // get both pieces of data
   if (fin.fail())                      // did anything go wrong?
   {
      cout << "Unable to read name and age from "
           << fileName
           << endl;
      return false;
   }

   // user-friendly display of the data
   cout << "The user "
        << userName
        << " is "
        << userAge
        << " years old\n";
         
   // all done
   fin.close();                         // don't forget to clsoe the file
   return true;                         // return and report
}

/**********************************************************************
 * main(): A simple driver program for read()
 ***********************************************************************/
int main()
{
   char fileName[256];               // lots of space for file names

   do                                // catch misspelled filenames
   {
      cout << "What is the fileName? ";
      cin  >> fileName;
   }
   while (!read(fileName));          // continue as long as there are errors

   return 0;
}


/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to create and use a jagged array
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * MAIN: Just a simple demo program for jagged arrays
 ***********************************************************************/
int main()
{
   int ** numbers;                  // pointer to a pointer to an integer

   // 1. Allocate the array of arrays
   numbers = new(nothrow) int *[4]; // an array of pointers to integers

   // 2. Allocate each individual array
   numbers[0] = new(nothrow) int[3];
   numbers[1] = new(nothrow) int[10];
   numbers[2] = new(nothrow) int[2];
   numbers[3] = new(nothrow) int[5];

   // 3. Assign a value
   numbers[2][1] = 42;             // access is the same as with standard
                                   //    multi-dimensional arrays
   // 4. Free each array
   for (int i = 0; i < 4; i++)     // we must free each individual array or
      delete [] numbers[i];        //    we will forget about them and
                                   //    have a "memory leak"
   // 5. Free the array of arrays
   delete [] numbers;              // the original array   
   
   return 0;
}


/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to gather data from the command line
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * MAIN: Reflect back to the use what he
 *       typed on the command line
 ***********************************************************************/
int main(int argc, char ** argv)  // MAIN really takes two parameters
{
   // name of the program
   cout << "The name of the program is: "
        << argv[0]                // the first item in the list is always
        << endl;                  //     the command the user typed
   
   // number of parameters
   cout << "There are "
        << argc - 1               // don't forget to subtract one due to
        << " parameters\n";       //     the first being the program name

   // show each parameters
   for (int iArg = 1; iArg < argc; iArg++)  // standard command line loop
      cout << "\tParameter " << iArg
           << ": " << argv[iArg] << endl;   // each argv[i] is a c-string

   return 0;
}


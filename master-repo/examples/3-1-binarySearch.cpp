/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate binary searching
 ************************************************************************/

#include <iostream>
using namespace std;

bool binarySearch(const int numbers[], int size, int search);

/**********************************************************************
 * MAIN: Driver for linear() and binary()
 ***********************************************************************/
int main()
{
   // list to search in
   int values[] =
   {
      1, 3, 5, 7, 7, 8, 9, 11, 13
   };

   // the value to search for
   int search;  
   cout << "Which item to you want to find? ";
   cin  >> search;

   // binary search
   if (binarySearch(values, sizeof(values) / sizeof(values[0]), search))
      cout << "\tBinary found it\n";
   else
      cout << "\tBinary did not find it\n";

   return 0;
}

/*******************************************************
 * BINARY SEARCH: perform a binary search
 *    1. Start in the middle (iMiddle)
 *    2. If the number is greater, you can rule out the beginning
 *    3. If the number is smaller, you can rule out the end
 *    4. Continue with steps 1-3 until we either find the number or
 *       there are no values betweein iFirst and iLast
 * Input:
 *    numbers: a list of numbers to search through
 *    size:    the size of the numbers list
 *    search:  the number that you are searching for
 * Output:
 *    bool:    true if found, false if not found
 ******************************************************/
bool binarySearch(const int numbers[], int size, int search)
{
   int iFirst = 0;                        // iFirst and iLast represent the
   int iLast  = size - 1;                 //    range of possible values:
                                          //    the entire list

   while (iLast >= iFirst)                // continue as long as the range
   {                                      //     is not empty
      int iMiddle = (iLast + iFirst) / 2; // the center (step 1)

      if (numbers[iMiddle] == search)     // if we found it, then stop
         return true;
      if (numbers[iMiddle] > search)      // if middle is bigger, focus on
         iLast = iMiddle - 1;             //     the beginning (step 2)
      else                                // otherwise, focus on the end
         iFirst = iMiddle + 1;            //     of the list (step 3)
   }

   // we only get there if we didn't find the value
   return false;
}



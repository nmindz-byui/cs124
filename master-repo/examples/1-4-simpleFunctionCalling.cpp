/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate creating a function and calling it
 ************************************************************************/

#include <iostream>
using namespace std;

/************************************************************
 * RETURN NOTHING
 * This function will not return anything.  It's only purpose is
 * to display text on the screen.  In this case, it will display
 * one of the great questions of the universe
 ************************************************************/
void returnNothing()
{
   // display our profound question
   cout << "What is the meaning of the life, universe, and everything?\n";

   // send no information back to main()
   return;
}

/*******************************************************************
 * RETURN A VALUE
 * This function, when called, will return a single integer value.
 ******************************************************************/
int returnAValue()
{
   // did you guess what value we would be returning here?
   return 42;
}

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // call the function asking the profound question
   returnNothing();

   // display the answer:
   cout << "The answer is: "
        << returnAValue()
        << endl;
   
   return 0;
}

/***********************************************************************
* Summary: 
*    Demonstrating pass-by-value, pass-by-reference, and pass-by-pointer.
*    From this demo, we will see how pass-by-reference works: both
*    the caller and the callee are referring tot he same location in
*    memory. This is exactly how pass-by-pointer works.
************************************************************************/

#include <iostream>
using namespace std;

/********************************************************************
 * Demonstrate pass by value, pass by reference, and pass by pointer
 *******************************************************************/
void function(int value, int &reference, int * pointer)
{
   cout << "value:     " << value     << " &value:     " << &value     << endl;
   cout << "reference: " << reference << " &reference: " << &reference << endl;
   cout << "*pointer:  " << *pointer  << " pointer:    " << pointer    << endl;
}


/**********************************************************************
* Just call a function.  No big deal really.
***********************************************************************/
int main()
{
   int number;
   cout << "Please give me a number: ";
   cin  >> number;
   cout << "number:    " << number
        << "\t&number: " << &number
        << endl << endl;


   function(number  /*by value*/,
            number  /*by reference*/,
            &number /*by pointer*/);
   
   
   return 0;
}

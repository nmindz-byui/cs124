/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate a simple FOR loop
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * main(): just a simple demonstration function
 ***********************************************************************/
int main()
{
   // get the start
   cout << "What value do you want to start at? ";
   int start;
   cin  >> start;
   
   // get the end
   cout << "What value do you want to end at? ";
   int end;
   cin  >> end;

   // get the increment
   cout << "What will you count by (example: 2s): ";
   int increment;
   cin  >> increment;
   
   // count
   for (int count = start; count <= end; count += increment)
      cout << "\t" << count << endl;

   return 0;
}


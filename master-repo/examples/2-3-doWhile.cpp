/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to use the doWhile loop by adding a collection
 *      of numbers
 ************************************************************************/

#include <iostream>
using namespace std;

/******************************************************
 * PROMPT FOR NUMBERS
 * Prompt the user for a bunch of numbers, stopping when
 * he enteres zero.  With each number, add it to a sum value
 *****************************************************/
int promptForNumbers()
{
   // display instructions
   cout << "Please enter a collection of integer values.  When\n"
        << "\tyou are done, enter zero (0).\n";
   int sum = 0;
   int value;
   
   // perform the loop
   do
   {
      // prompt for value
      cout << "> ";
      cin  >> value;
      
      // add value to sum
      sum += value;
   }
   while (value != 0);
   // continue until the user enters zero

   // return and report
   return sum;
}

/**********************************************************************
 * main(): Driver program for promptForNumbers() function
 ***********************************************************************/
int main()
{
   // prompt for the sum
   int sum = promptForNumbers();
   
   // display the sum
   cout << "The sum is: " << sum << endl;

   return 0;
}


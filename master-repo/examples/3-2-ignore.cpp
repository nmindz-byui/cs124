/***********************************************************************
 * This program is designed to demonstrate:
 *      How cin.ignore() works
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************
 * WITHOUT IGNORE
 * Demonstrate what happens when we input a number
 * followed by a getline().
 **********************************************/
void withoutIgnore(int &value, char text[])
{
   // prompt for a number.  This will accept only the user input
   // that consists of digits.  The first non-digit will hault user
   // input.
   cout << "Enter a number: ";
   cin  >> value;

   // now accept text input with getline().  This will accept all user
   // input up to the first newline character
   cin.getline(text, 256);
   
   return;
}

/**********************************************
 * WITH IGNORE
 * Demonstrate what happens when we input a number
 * followed by a cin.ignore() before the getline().
 **********************************************/
void withIgnore(int &value, char text[])
{
   // prompt for a number.  This will accept only the user input
   // that consists of digits.  The first non-digit will hault user
   // input.
   cout << "Enter a number: ";
   cin  >> value;

   // ignore all characters up to the \n
   cin.ignore(256, '\n');   // up to 256 characters preceeding the \n
   //cin.ignore();         // same as cin.ignore(1, '\n');

   // now accept text input with getline().  This will accept all user
   // input up to the first newline character
   cin.getline(text, 256);

   return;
}


/**********************************************************************
 * MAIN
 * This will test our two functions.  To see the difference, try the
 * following input...
 *      123text\nsecond line\n
 * ...where \n is actually a newline character
 ***********************************************************************/
int main()
{
   // a simple switch
   char choice;
   cout << "Demonstrate input with cin.ignore() (y/n)? ";
   cin  >> choice;

   // data to be read: a number followed by text
   int number;
   char text[256];
   
   // when cin.ignore() is missing
   if (choice == 'y' || choice == 'Y')
      withIgnore(number, text);
   else
      withoutIgnore(number, text);   

   // display the results
   cout << "number: " << number << endl;
   cout << "text:   " << text   << endl;
   
   return 0;
}

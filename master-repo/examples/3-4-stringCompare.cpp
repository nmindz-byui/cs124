/***********************************************************************
 * This demo program is designed to:
 *      demonstrate three ways to determine if two strings are equal:
 *       1. using array index notation (isEqualArray())
 *       2. using standard FOR loop (isEqualPointer())
 *       3. optimal solution (isEqualOptimal())
 ************************************************************************/

#include <iostream>
using namespace std;

bool isEqualArray(  const char * text1, const char * text2);
bool isEqualPointer(const char * text1, const char * text2);
bool isEqualOptimal(const char * text1, const char * text2);

/**********************************************************************
 * MAIN
 * Simple driver program for our three functions
 ***********************************************************************/
int main()
{
   // prompts
   char text1[256];
   char text2[256];
   cout << "Please enter text 1: ";
   cin.getline(text1, 256);
   cout << "Please enter text 2: ";
   cin.getline(text2, 256);

   // display the results
   cout << "\tArray notation:    "
        << (isEqualArray  (text1, text2) ? "Equal" : "Not Equal")
        << endl;
   cout << "\tPointer notation:  "
        << (isEqualPointer(text1, text2) ? "Equal" : "Not Equal")
        << endl;
   cout << "\tOptimal version:   "
        << (isEqualOptimal(text1, text2) ? "Equal" : "Not Equal")
        << endl;

   return 0;
}

/**********************************************************
 * IS EQUAL: Array index version
 * Walk through both arrays with a single index
 *********************************************************/
bool isEqualArray(const char * text1, const char * text2)
{
   int i;                          // share the index with both strings

   for (i = 0;                     // start at the beginning
        text1[i] == text2[i] &&    // as long as both are the same and
           text1[i] && text2[i];   //    we are not at the end of the string
        i++)                       // continue walking through the string
      ;
           
   return text1[i] == text2[i];    // same if both are the null character
}

/*******************************************************
 * IS EQUAL: using pointer notation
 * Use two pointers to walk through the strings
 *******************************************************/
bool isEqualPointer(const char * text1, const char * text2)
{
   const char * p1;              // for text1.  Needs to be a const
   const char * p2;              // for text2

   for (p1 = text1, p2 = text2;  // 2 pointers requires 2 initializations
        *p1 == *p2 &&            // same logic as the array index
           *p1 && *p2;           //   version
        p1++, p2++)              // both pointers are advanced
      ;

   return (*p1 == *p2);
}

/*****************************************************
 * IS EQUAL: The optimal version
 * Reuse the text1 and text2 variables. Also we only
 * need to do one null check.
 ****************************************************/
bool isEqualOptimal(const char * text1, const char * text2)
{
   while (*text1 == *text2 && *text1)  // no redundant checks
   {
      text1++;                         // use text1 and text2 to iterate
      text2++;                         //    through the strings rather
   }                                   //    than p1 and p2
   
   return (*text1 == *text2);          // same final check
}

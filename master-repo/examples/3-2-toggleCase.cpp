/***********************************************************************
 * This demo program is designed to:
 *      How to walk through a string using a FOR loop.  In this case, we
 *      will convert uppercase to lowercase (and vice-versa)
 ************************************************************************/

#include <iostream>
#include <cassert>
#include <cctype>
using namespace std;

void prompt(char text[], int max);
void convert(char text[]);
void display(const char text[]);

/**********************************************************************
 * MAIN
 * Just a simple driver program for our convert() function
 ***********************************************************************/
int main()
{
   // get the user's text
   char text[256];
   prompt(text, sizeof(text) / sizeof(text[0]));

   // convert uppercase to lowercase and vice-versa
   convert(text);
   
   // display the results
   display(text);

   return 0;
}

/**************************************************
 * PROMPT
 * Ask the user for his text
 **************************************************/
void prompt(char text[], int max)
{
   assert(max > 0);
   cout << "Please enter your text: ";
   cin.getline(text, max);
}

/*************************************************
 * CONVERT
 * tHIS PROGRAM WILL CONVERT uPPERCASE TO lOWERCASE
 * AND vICE-vERSA.
 *************************************************/
void convert(char text[])
{
   for (int i = 0; text[i]; i++)
      if (isupper(text[i]))
         text[i] = tolower(text[i]);
      else
         text[i] = toupper(text[i]);
}

/***************************************************
 * DISPLAY
 * Exactly the same functionality as
 *     cout << text << endl;
 **************************************************/
void display(const char text[])
{
   for (int i = 0; text[i]; i++)
      cout << text[i];
   cout << endl;
}

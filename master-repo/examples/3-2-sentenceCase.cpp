/***********************************************************************
 * This demo program is designed to:
 *      Convert text into sentence case (all lowercase except the first
 *      letter of a sentence
 ************************************************************************/

#include <iostream>
using namespace std;

/***************************************
 * CONVERT
 * Walk through the string one character at
 * a time.  Convert everything to lowercase
 * except those characters at the beginning of the
 * sentence.  These convert to uppercase
 ***************************************/
void convert(char text[])
{
   // the first letter of the input is the start of a sentence
   bool isNewSentence = true;
   
   // traverse the string
   for (int i = 0; text[i]; i++)
   {
      if (text[i] == '.' || text[i] == '!' || text[i] == '?')
         isNewSentence = true;
      
      // convert the first letter to uppercase
      if (isNewSentence && isalpha(text[i]))
      {
         text[i] = toupper(text[i]);
         isNewSentence = false;
      }
      // everything else to lowercase
      else
         text[i] = tolower(text[i]);
   }
}


/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // input
   char input[256];
   cout << "Please enter your input text: ";
   cin.getline(input, 256);
   
   // convert
   convert(input);

   // output
   cout << "The sentence-case version of the input text is:\n\t"
        << input
        << endl;

   return 0;
}


/***********************************************************************
 * This demo program is designed to:
 *      Traversing an array using the pointer notation
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

/**********************************************
 * DISPLAY LIST
 * Display a list using the pointer notation
 **********************************************/
void displayList(int array[], int num)
{
   assert(num >= 0);
   
   int * pEnd = array + num;
   for (int * p = array; p < pEnd; p++)
      cout << *p << endl;

   return;
}

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   const int NUM = 5;
   int array[NUM] =
   {
      43, 96, 21, 35, 42
   };
   assert(NUM == sizeof(array) / sizeof(*array));

   // display the list
   displayList(array, NUM);

   return 0;
}


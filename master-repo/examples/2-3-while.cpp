/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate the While loop to prompt the user for grade
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************
 * GET GRADE
 * Prompt the user for a grade and
 * make sure it is within the valid
 * range of A,B,C,D,F
 *********************************/
char getGrade()
{
   char grade;   // the value we will be returning

   // initial prompt
   cout << "Please enter your letter grade: ";
   cin  >> grade;

   // validate the value
   while (grade != 'A' && grade != 'B' && grade != 'C' &&
          grade != 'D' && grade != 'F')
   {
      cout << "Invalid grade.  Please enter a letter grade {A,B,C,D,F} ";
      cin  >> grade;
   }

   // return when done
   return grade;
}

/**********************************************************************
 * MAIN: Simple driver program for getGrade
 ***********************************************************************/
int main()
{
   char grade = getGrade();

   cout << "Your grade is " << grade << endl;

   return 0;
}


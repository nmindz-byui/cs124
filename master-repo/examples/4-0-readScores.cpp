/***********************************************************************
 * This demo program is designed to:
 *      Display the scores of students which were read from a file. 
 ************************************************************************/

#include <iostream>
#include <fstream>
using namespace std;

bool readData(int grades[][5], int numStudents, const char *fileName);
void displayAssigns(const int grades[][5], int numStudents);
void displayStudents(const int grades[][5], int numStudents);

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   int grades[10][5]; // five grades for each of 10 students

   // read grades
   char fileName[256];
   cout << "Filename: ";
   cin  >> fileName;
   if (!readData(grades, 10 /*numStudents*/, fileName))
      return 1;

   // display average for each student
   displayStudents(grades, 10 /*numStudents*/);

   // display average for each assignment
   displayAssigns(grades, 10 /*numStudents*/);

   return 0;
}

/*************************************
 * READ DATA
 * Read the data from fileName and put it
 * in the grades[] array.
 *************************************/
bool readData(int grades[][5], int numStudents, const char *fileName)
{
   ifstream fin(fileName);
   if (fin.fail())
   {
      cout << "ERROR: Unable to open file "
           << fileName
           << endl;
      return false;
   }

   // read the data from the file, one row (student) at a time
   for (int iStudent = 0; iStudent < numStudents; iStudent++)
   {
      // read all the data for a given student: 5 assignments
      for (int iAssign = 0; iAssign < 5; iAssign++)
         fin >> grades[iStudent][iAssign];

      if (fin.fail())
      {
         cout << "ERROR: Problem with the file "
              << fileName
              << ".  Please check the file and try again.\n";
         fin.close();
         return false;
      }
   }
   
   fin.close();
   return true;
}

/**********************************
 * DISPLAY ASSIGNS
 * Display the average grade for each assignment
 *********************************/
void displayAssigns(const int grades[][5], int numStudents)
{
   // loop through each assignment
   for (int iAssign = 0; iAssign < 5; iAssign++)
   {
      // sum up the scores for each student
      int sum = 0;
      for (int iStudent = 0; iStudent < numStudents; iStudent++)
         sum += grades[iStudent][iAssign];

      // compute the average
      int average = sum / numStudents;
      
      // display the results
      cout << "Average grade for assignment #"
           << iAssign
           << " is "
           << average
           << "%\n";
   }   
}

/********************************
 * DISPLAY STUDENTS
 * Display the average grade for each
 * of the numStudents students
 ********************************/
void displayStudents(const int grades[][5], int numStudents)
{
   // for each student
   for (int iStudent = 0; iStudent < numStudents; iStudent++)
   {
      // compute the sum of all the points
      int sum = 0;
      for (int iAssign = 0; iAssign < 5; iAssign++)
         sum += grades[iStudent][iAssign];
      
      // find the average for the student
      int average = sum / 5;
      
      // display the results
      cout << "Student "
           << iStudent
           << "'s average grade is "
           << average
           << "%\n";
   }
}

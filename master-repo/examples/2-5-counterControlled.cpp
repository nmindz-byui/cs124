/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate a counter-controlled loop
 *
 *      This will be accomplished by validating Gauss' equation.  In other
 *      words, Gauss said that 1 + 2 + 3 + ... n = (n + 1) n / 2.
 *      This program will use both Gauss' equation (computeGauss(n)) and
 *      use a counter-controlled loop (countLoop(n))
 ************************************************************************/

#include <iostream>
using namespace std;

int computeGauss(int n);                  // Gauss' equation
int countLoop(int n);                     // Counter-controlled loop
void display(int sumGauss, int sumLoop);  // display the results

/**********************************************************************
 * main(): Driver program for both computeGauss() and countLoop()
 ***********************************************************************/
int main()
{
   // 1. Prompt the user for a number
   int n;
   cout << "This program will add the numbers from 1 to n.\n";
   cout << "Please specify the n: ";
   cin  >> n;
   
   // 2. Compute the values
   int sumGauss = computeGauss(n);
   int sumLoop  = countLoop(n);
   
   // 3. Display the results
   display(sumGauss, sumLoop);

   return 0;
}

/********************************************
 * COMPUTE GAUSS
 * Compute the sum using Gauss' equation
 *    sum = (n + 1)n / 2
 ********************************************/
int computeGauss(int n)
{
   return (n + 1) * n / 2;
}

/*****************************************
 * COUNT LOOP
 * Compute the sum using a loop
 ****************************************/
int countLoop(int n)
{
   int sum = 0;
   for (int count = 1; count <= n; count++)
      sum += count;

   return sum;
}

/******************************************
 * DISPLAY
 * Display the results and compare the values
 *****************************************/
void display(int sumGauss, int sumLoop)
{
   // Output the results
   cout << "Answer adding the values one by one: "
        << sumLoop
        << endl;
   cout << "Answer using Gauss' equation: "
        << sumGauss
        << endl;
   
   // Compare the answers
   if (sumLoop == sumGauss)
      cout << "They are the same\n";
   else
      cout << "They are different!\n";

   return;
}

/***********************************************************************
 * This demo program is designed to:
 *    Demonstrate stubbing out Project 1
 ************************************************************************/


#include <iostream>
#include <iomanip>

using namespace std;


double computeTax(double incomeMonthly)
{
   return 0.0;
}

double computeTithing(double income)
{
   return 0.0;
}

void display(double income,
             double budgetLiving, 
             double actualTax, 
             double actualTithing, 
             double actualLiving, 
             double actualOther)
{
   double budgetTax     = computeTax(income);
   double budgetTithing = computeTithing(income);

   return;
}

double getIncome()
{
   return 0.0;
}

double getBudgetLiving()
{
   return 0.0;
}

double getActualTax()
{
   return 0.0;
}

double getActualTithing()
{
   return 0.0;
}

double getActualLiving()
{
   return 0.0;
}

double getActualOther()
{
   return 0.0;
}

int main()
{
   double income          = getIncome();
   double budgetLiving    = getBudgetLiving();
   double actualLiving    = getActualLiving();
   double actualTax       = getActualTax();
   double actualTithing   = getActualTithing();
   double actualOther     = getActualOther();

   display(income, budgetLiving, actualTax, actualTithing,
           actualLiving, actualOther);

   return 0;
}


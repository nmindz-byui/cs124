/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how multiple variables can refer to the same location
 *      in memory through the use of pointers. 
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * main(): Just one function for now...
 ***********************************************************************/
int main()
{
   // initial grade for the student
   int gradeProf = 86;                       // a normal data variable

   // two people have access
   const int * pGradeStudent   = &gradeProf; // a const so student can.t change
   const int  *pGradeRegistrar = &gradeProf; // registrar can't change either

   // show the status of the variables before the change
   cout << "Registrar's copy:        " << *pGradeRegistrar << endl;
   cout << "Student's copy:          " << *pGradeStudent   << endl;
   cout << "Professor's master copy: " << gradeProf        << endl;

   // professor updates the grade.
   cout << "\nThe grade was changed!\n";
   gradeProf = 89;

   // report the results
   cout << "Registrar's copy:        " << *pGradeRegistrar << endl;
   cout << "Student's copy:          " << *pGradeStudent   << endl;
   cout << "Professor's master copy: " << gradeProf        << endl;

   return 0;
}


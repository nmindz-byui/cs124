/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to perform a table-lookup to solve the computeTax
 *      problem from Project 1
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

/*********************************************************************
 * COMPUTE TAX BRACKET
 * Given a user's income, compute the tax bracket.
 *********************************************************************/
int computeTaxBracket(int income)
{
   assert(income >= 0);
   int lowerRange[] =
   { // 10%    15%    25%     28%     33%     35%
          0,  15100, 61300, 123700, 188450, 336550
   };
   int upperRange[] =
   { // 10%    15%    25%     28%     33%     35%
      15100, 61300, 123700, 188450, 336550, 999999999
   };
   int bracket[] =
   {
      10,      15,    25,     28,     33,     35
   };

   // loop through the list, comparing income with lowerRange and upperRange
   for (int i = 0; i < 6; i++)           // the index for the 3 arrays
      if (lowerRange[i] <= income && income <= upperRange[i])
         return bracket[i];

   assert(false);
   return -1;                            // I should never be here!
}

/**********************************************************************
 * MAIN
 * A simple driver program to test computeTax
 ***********************************************************************/
int main()
{
   // prompt for the user income
   int income;
   cout << "What is your income? ";
   cin  >> income;
   
   // compute the tax bracket
   int bracket = computeTaxBracket(income);
   
   // display the results
   cout << "Your tax bracket is: "
        << bracket
        << "%\n";

   return 0;
}


/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate a simple sentinel-controlled loop:
 *
 *      This program will simulate your experience in CS 124: you continue
 *      taking the class until the requirements are met.  Of course, there
 *      are many reasons why one may before forced to re-take the class...
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * main(): Simple program to demonstrate a sentinel-controlled loop
 ***********************************************************************/
int main()
{
   // declare the sentinel
   bool passed = false;     // initially we have not passed the class

   // the main loop
   while (!passed)
   {
      cout << "Welcome to CS 124\n";

      // check for grade
      float grade;
      cout << "What is your class grade? ";
      cin  >> grade;
      if (grade >= 70.0)
         passed = true;            // we passed with a high enough grade
      
      // check to see if the student cheated
      char cheated;
      cout << "Did you cheat in the class? (y/n) ";
      cin  >> cheated;
      if (cheated == 'y' || cheated == 'Y')
         passed = false;           // fail if we cheated
   }
   
   // display the success message
   cout << "Great job!  Get ready for CS 165\n";

   return 0;
}


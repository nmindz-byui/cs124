/***********************************************************************
* Program:
*    Test 3, Mileage
*    Brother Helfrich, CS124
* Author:
*    The Dark Avenger
* Summary: 
*    Display the number of items in a list of mileage data
*    that is below a certain threshold
*
*    Estimated:  0.5 hrs   
*    Actual:     0.0 hrs
*      Please describe briefly what was the most difficult part.
************************************************************************/

#include <iostream>
#include <fstream>
#include <cassert>
using namespace std;

#define MAX 100

void  getFilename(char filename[]);
void  getData(float mileage[], int & num);
float getThreshold();
void  display(const float mileage[], const int num, const float threshold);

/**********************************************************************
 * MAIN
 * Call the functions and store the data
 ***********************************************************************/
int main()
{
   // get the mileage data
   float mileage[MAX];
   int num;
   getData(mileage, num);

   // get the threshold data
   float threshold = getThreshold();
   
   // display the results
   display(mileage, num, threshold);
   
   return 0;
}

void getFilename(char filename[])
{
   cout << "What is the name of the file: ";
   cin  >> filename;
}

/*********************************************
 * GET DATA
 * Prompt the user for the filename and read the data
 * into the mileage array
 ********************************************/
void  getData(float mileage[], int & num)
{
   // prompt for filename
   char filename[256];
   getFilename(filename);
   
   // open the file
   ifstream fin(filename);
   if (fin.fail())
   {
      cout << "ERROR: Unable to open file "
           << filename
           << ". You are a LOOOSER!!\n";
      num = 0;
      return;
   }

   // read the data
   num = 0;
   while (num < MAX && fin >> mileage[num])
      num++;

   // close the file
   fin.close();
   return;
}

/***************************************************
 * GET THRESHOLD
 * Prompt the user for the threshold
 **************************************************/
float getThreshold()
{
   cout << "What is the threshold: ";
   float threshold;
   cin  >> threshold;
   return threshold;
}

/**************************************************
 * DISPLAY
 * Display a list of all the mileage values that are bad
 **************************************************/
void  display(const float mileage[], const int num, const float threshold)
{
   // get ready, get set...
   cout << "The following items are below the threshold:\n";
   cout.setf(ios::fixed | ios::showpoint);
   cout.precision(1);

   // Loop through the list
   for (int i = 0; i < num; i++)
      // if a mileage value is bad
      if (mileage[i] < threshold)
         // display it
         cout << "\t" << mileage[i] << endl;
}


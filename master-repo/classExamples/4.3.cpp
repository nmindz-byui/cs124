/***********************************************************************
 * This program is designed to demonstrate:
 *      Practice 4.3 - Title Case
 ************************************************************************/

#include <iostream>   // CIN and COUT
#include <fstream>    // IFSTREAM
#include <cctype>     // TOUPPER and TOLOWER
using namespace std;

void getFileName(char * fileName);
void readFile(char * text, int max, const char * fileName);
void convert(char * text);

/**********************************************************************
 * MAIN
 * Read some text from a file and display it Title Case
 ***********************************************************************/
int main()
{
   // get the filename
   char fileName[256];
   getFileName(fileName);
   
   // read the file
   char text[256];
   readFile(text, 256, fileName);

   // convert
   convert(text);

   // display in Title Case
   if (*text != '\0')
      cout << text << endl;
   
   return 0;
}

/**********************************************
 * GET FILE NAME
 * Prompt the user for the file name
 **********************************************/
void getFileName(char * fileName)
{
   cout << "Please enter the filename: ";
   cin  >> fileName;
}

/***********************************************
 * READ FILE
 * Read all the data from my file
 ***********************************************/
void readFile(char * text, int max, const char * fileName)
{
   // open the file
   ifstream fin(fileName);

   // check for errors
   if (fin.fail())
   {
      cout << "The file is empty\n";
      text[0] = '\0';
      return;
   }

   // read the file into a c-string
   fin.getline(text, max);
   
   // close the file
   fin.close();
}

/*************************************************
 * CONVERT
 * convert everything to lowercase except:
 *  1. when there is a space before
 *  2. the first letter in the string
 *************************************************/
void convert(char * text)
{
   // paranoid check and do nothing if the string is empty
   if (text[0] == '\0')
      return;

   // handle the first character of the string
   text[0] = toupper(text[0]);
   
   // loop through the entire string, checking for spaces.
   for (char * p = text; *p; p++)
   {
      // when I find a space,
      if (*p == ' ' || *p == '\t' || *p == '-')
         // I will convert the next character into uppercase
         p[1] = toupper(p[1]);
      else
         p[1] = tolower(p[1]);
   }
}

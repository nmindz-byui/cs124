/***********************************************************************
 * This program is designed to demonstrate:
 *      string class speed program thingy
 ************************************************************************/

#include <iostream>   // CIN and COUT
#include <cstring>    // STRLEN
#include <iomanip>    // SETW
#include <time.h>     // CLOCK
#include <string>     // STRING
using namespace std;


/****************************************
 * TEST STRING CLASS
 * Stupid is as stupid does
 ***************************************/
float testStringClass(int size)
{
   string text;
   text.reserve(size);

   int msBegin = clock();

   for (int i = 0; i < size; i++)
      text += 'a';

   int msEnd = clock();

   return ((float)(msEnd - msBegin)) / 1000000.0;
}

/****************************************
 * TEST C STRING SMART
 * this is the smart way to do it
 ***************************************/
float testCStringSmart(int size)
{
   char * text = new(nothrow) char[size + 1];

   int length = 0;
   text[length] = '\0';

   int msBegin = clock();

   for (int i = 0; i < size; i++)
   {
      text[length] = 'a';
      text[++length] = '\0';
   }

   int msEnd = clock();

   delete [] text;

   return ((float)(msEnd - msBegin)) / 1000000.0;
}


/****************************************
 * TEST C STRING STUPID
 * this is the stupid way to do it
 ***************************************/
float testCStringStupid(int size)
{
   char * text = new(nothrow) char[size + 1];

   text[0] = '\0';

   int msBegin = clock();
   
   for (int i = 0; i < size; i++)
   {
      int length = strlen(text);
      text[length] = 'a';
      text[length + 1] = '\0';
   }

   int msEnd = clock();

   delete [] text;

   return ((float)(msEnd - msBegin)) / 1000000.0;
}




/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   int length;


   cout.setf(ios::fixed | ios::showpoint);
   cout.precision(3);

   cout 
        << setw(20) << "Smart"
        << setw(10) << "Class"
        << endl;
   
   for (int i = 1; true; i *= 2)
      cout << setw(10) << i
         //    << setw(10) << testCStringStupid(i)
           << setw(10) << testCStringSmart(i)
           << setw(10) << testStringClass(i)
           << endl;

   return 0;
}

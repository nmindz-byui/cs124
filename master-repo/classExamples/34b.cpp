/***********************************************************************
 * This program is designed to demonstrate:
 *      melt some brainzzzzz
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   int array[] = {42, 76, 92, 16};

   cout << "array[2]     = " << array[2]     << endl;
   cout << "*(array + 2) = " << *(array + 2) << endl;
   cout << "*(2 + array) = " << *(2 + array) << endl;
   cout << "2[array]     = " << 2[array]     << endl;
   
   return 0;
}

/***********************************************************************
 * This program is designed to demonstrate:
 *      passing pointers
 ************************************************************************/

#include <iostream>
using namespace std;


void function(const char * data)
{
//   data++;
   *data = '.';
}


/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   char text[] = "Banana";

   function(text);
   cout << text << endl;
   
   
   return 0;
}

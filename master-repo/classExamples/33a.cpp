/***********************************************************************
 * This program is designed to demonstrate:
 *      arrays and pointers
 ************************************************************************/

#include <iostream>
using namespace std;

void display1(const char text[])
{
   for (int i = 0; text[i]; i++)
      cout << text[i];
   cout << endl;
}
           
void display2(const char * text)
{
   for (int i = 0; text[i]; i++)
      cout << text[i];
   cout << endl;
}

void display3(const char * text)
{
   for (const char * p = text; *p; p++)
      cout << *p;
   cout << endl;
}

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   const char * text = "This is a test";

   display1(text);
   display2(text);
   display3(text);
   
   return 0;
}

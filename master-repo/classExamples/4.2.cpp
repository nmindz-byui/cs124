/***********************************************************************
 * This program is designed to demonstrate:
 *      Practice 4.2
 ************************************************************************/

#include <iostream>   // CIN and COUT
#include <string>     // for STRING
#include <fstream>    // for FIN
using namespace std;

/**************************************
 * GET FILE NAME
 * Prompt the user for the fileName
 ************************************/
string getFileName()
{
   string fileName;
   cout << "Please enter the file name: ";
   cin  >> fileName;
   return fileName;
}

/**********************************
 * READ FILE
 * Read the file one line at an time
 * and promptly forget all about it as if
 * you were reading Isaiah or your Biology textbook
 **********************************/
int readFile(const char * fileName)
{
   // open the file
   ifstream fin(fileName);

   // check for errors
   if (fin.fail())
      return 0;

   // keep reading one line of the file at a time  until we reach the end
   string line;
   int numLines = 0;
   while (getline(fin, line))
      // increment my line counter
      numLines++;
      
   // close the file
   fin.close();
   return numLines;
}

/*******************************************
 * DISPLAY
 * display the file name and the number of lines
 ******************************************/
void display(const char * fileName, int numLines)
{
   cout << "\t" << fileName << " ";

   switch (numLines)
   {
      case 0:
         cout << "is empty";
         break;
      case 1:
         cout << "has 1 line";
         break;
      default:
         cout << "has " << numLines << " lines";
   }

   cout << ".\n";
}

/**********************************************************************
 * MAIN
 * Get the file name and read it and then display the number of lines
 ***********************************************************************/
int main(int argc, char ** argv)
{
   // get the file name
   string fileName;
   if (argc == 1)
      fileName = getFileName();
   else
      fileName = argv[1];

   // read the file to get the number of lines
   int numLines = readFile(fileName.c_str());

   // display
   display(fileName.c_str(), numLines);

   return 0;
}

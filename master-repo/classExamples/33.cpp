/***********************************************************************
 * This program is designed to demonstrate:
 *      passing pointers and stuff
 ************************************************************************/

#include <iostream>
using namespace std;

void function(int value, int & reference, int * pointer)
{
   cout << "    value = " << value
        << "      &value = " << &value <<  endl;
   cout << "reference = " << reference
        << "  &reference = " << &reference << endl;
   cout << " *pointer = " << *pointer
        << "     pointer = " << pointer << endl;
}

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   int value = 42;

   cout << "value =  " << value << endl;
   cout << "&value = " << &value << endl;
   cout << "-=-=-=-=-=-=-=-=-=-=-\n";

   function(value, value, &value);
   
   
   return 0;
}

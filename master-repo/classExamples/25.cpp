/***********************************************************************
 * This program is designed to demonstrate:
 *      
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   int letter = 65;

   cout << "letter ==       " << letter       << endl;
   cout << "(char)letter == " << (char)letter << endl;

   char input;
   cout << "> ";
   cin  >> input;
   cout << "input == " << input << endl;
   cout << "input - '0' == " << (input - '0') << endl; 
   
   return 0;
}

/***********************************************************************
 * This program is designed to demonstrate:
 *      guess ASCII
 ************************************************************************/

#include <iostream>
using namespace std;

char promptLetter();
int  play(char letter);
void display(int num);

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   // prompt for letter
   char letter = promptLetter();

   // play the game
   int num = play(letter);

   // display the results
   display(num);
   
   return 0;
}

/************************************
 * PROMPT LETTER
 * yep
 ***********************************/
char promptLetter()
{
   char letter;
   cout << "Which value would you like to guess: ";
   cin  >> letter;
   return letter;
}

const int CORRECT = 92;
#define TOO_HIGH 6284
#define TOO_LOW -521

/**********************************
 * COMPARE
 * compare value with letter
 *********************************/
int compare(int value, int letter)
{
   if (value > letter)
      return TOO_HIGH;
   else if (value < letter)
      return TOO_LOW;
   else
      return CORRECT;
}

/**************************************
 * PLAY
 * Yes, this is child's play!
 **************************************/
int play(char letter)
{
   int numGuesses = 0;
   int guess;
   
   do
   {
      // prompt
      int value;
      cout << "What is the value of '" << letter << "'? ";
      cin  >> value;

      // figure out how the user did
      guess = compare(value, letter);

      // give helpful message
      if (guess == TOO_HIGH)
         cout << "Too high\n";
      else if (guess == TOO_LOW)
         cout << "Too low\n";
      numGuesses++;
   }
   while (guess != CORRECT);
   
   return numGuesses;
}

/****************************************
 * DISPLAY
 ****************************************/
void display(int num)
{
   cout << "You got it in " << num << " guesses.\n";
}

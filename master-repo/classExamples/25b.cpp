/***********************************************************************
 * This program is designed to demonstrate:
 *      
 ************************************************************************/

#include <iostream>
#include <iomanip>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   for (int letter = 32; letter <= 255; letter++)
      cout << letter << setw(4) << (char)letter << endl;
   
   return 0;
}

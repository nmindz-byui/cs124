/***********************************************************************
 * This program is designed to demonstrate:
 *      
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   cout << "To infinity... " << endl;

   for (int i = 0; i >= 0; i++)
      ;

   cout << "... and beyond\n";
   
   return 0;
}

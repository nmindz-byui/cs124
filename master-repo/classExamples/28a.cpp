/***********************************************************************
 * This program is designed to demonstrate:
 *      Practice 2.1
 ************************************************************************/

#include <iostream>
#include <cassert>    // paranoia will destroy ya
#include <ctime>      // for time(), part of the random process
#include <stdlib.h>   // for rand() and srand()
using namespace std;

/******************************************************
 * PROMPT NUM FLIPS
 * yep
 ******************************************************/
int promptNumFlips()
{
   int numFlips;

   do
   {
      cout << "How many coin flips for this experiment: ";
      cin  >> numFlips;
   }
   while (numFlips < 0);
      
   return numFlips;
}

/****************************************************
 * PERFORM EXPERIMENT
 * Flip the coin
 ***************************************************/
int performExperiment(int numFlips)
{
   int numHeads = 0;

   // FOR i = 1 .. numFlips
   for (int i = 1; i <= numFlips; i++)
   {
      // IF rand() MOD 2 = 1
      //    numHeads++
      if (rand() % 2 == 0)
         numHeads++;
   }

   // RETURN numHeads
   return numHeads;
}

/****************************************************
 * DISPLAY
 * Display the number of heads and the number of tails
 ****************************************************/
void display(int numHeads, int numTails)
{
   assert(numHeads >= 0);
   assert(numTails >= 0);
   
   // display the number of heads
   cout << "There were " << numHeads << " heads.\n";

   // display the number of tails
   cout << "There were " << numTails << " tails.\n";
}


/**********************************************************************
* MAIN
* Sorta like Main Street but completely different
***********************************************************************/
int main(int argc, char **argv)
{
   // this code is necessary to set up the random number generator. If
   // your program uses a random number generator, you will need this
   // code. Otherwise, you can safely delete it.  Note: this must go in main()
   srand(argc == 1 ? time(NULL) : (int)argv[1][1]);


   // get the number of flips
   int numFlips = promptNumFlips();
   assert(numFlips >= 0);
   
   // perform the experiment
   int numHeads = performExperiment(numFlips);
   
   // display the results to the user
   assert(numFlips >= numHeads);
   assert(numHeads >= 0);
   display(numHeads, numFlips - numHeads);
   
   return 0;
}

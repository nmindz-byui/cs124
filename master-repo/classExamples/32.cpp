/***********************************************************************
 * This program is designed to demonstrate:
 *      arrays of strings
 ************************************************************************/

#include <iostream>
using namespace std;

void displayWords(const char words[4][32], int num)
{
   for (int i = 0; i < num; i++)
      cout << '\t' << words[i] << endl;
}

void displayWord(const char word[])
{
   cout << '\t' << word << endl;
}

void displayLetter(const char letter)
{
   cout << "\tThis is brought to you by the letter '"
        << letter
        << "'\n";
}


/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   char words[4 /*numWords*/ ][32 /*lengthOfEachWord*/] =
   {
      "John",
      "Jacob",
      "Jingleheimer",
      "Schmitt"
   };

   cout << "All the words:\n";
   displayWords(words, 4);

   cout << "One word:\n";
   displayWord(words[0]);
   cout << "Another word:\n";
   displayWord(words[1]);

   cout << "One letter:\n";
   displayLetter(words[0][1]);
   
   return 0;
}

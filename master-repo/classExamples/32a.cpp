/***********************************************************************
 * This program is designed to demonstrate:
 *      the crazy null character
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   char text[256];
   cout << "Give me text or give me death! ";
   cin.getline(text, 256);

   char letter;
   cout << "Give me a letter please: ";
   cin >> letter;

   cout << "Text without modification: '"
        << text
        << "'\n";

   for (int i = 0; text[i]; i++)
      if (text[i] == letter)
         text[i] = '\0';

   cout << "Text after modification: '"
        << text
        << "'\n";
   
   return 0;
}

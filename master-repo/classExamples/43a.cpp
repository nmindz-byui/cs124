/***********************************************************************
 * This program is designed to demonstrate:
 *      display the absolute value
 ************************************************************************/

#include <iostream>
#include <cstdlib>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main(int argc, char ** argv)
{
   if (argc == 1)
   {
      cerr << "ERROR! Expected parameter after program name. Usage:\n"
           << "\t" << argv[0] << " [number] [number] ... \n";
      return 1;
   }
   
   for (int i = 1; i < argc; i++)
   {
      float num = atof(argv[i]);
      if (num < 0)
         num *= -1.0;
      
      cout << "The absolute value of "
           << argv[i]
           << " is "
           << num
           << endl;
   }

   
   return 0;
}

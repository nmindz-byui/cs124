/***********************************************************************
 * This program is designed to demonstrate:
 *      fun with pointers
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   char text[256];
   cout << "> ";
   cin.getline(text, 256);


   for (char * p = text; *p; p++)
      cout << p << endl;
   
   return 0;
}

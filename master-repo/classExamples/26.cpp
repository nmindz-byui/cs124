/***********************************************************************
 * This program is designed to demonstrate:
 *      cin and fin
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{

   int x = -1;
   char y[256];
   int z = -1;

   cout << "> ";
   cin  >> x >> y >> z;

   cout << "x: " << x << endl
        << "y: " << y << endl
        << "z: " << z << endl;

   
   return 0;
   
}

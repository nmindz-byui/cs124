/***********************************************************************
* Program:
*    Test 3, count Letter
*    Brother Helfrich, CS124
* Author:
*    the Joker
* Summary: 
*    Read a file looking for letters
*
*    Estimated:  0.25 hrs   
*    Actual:     0.0 hrs
*      Please describe briefly what was the most difficult part.
************************************************************************/

#include <iostream>
#include <fstream>
using namespace std;

/***********************************
 * GET FILENAME
 ***********************************/
void getFilename(char filename[])
{
   cout << "What is the name of the file: ";
   cin  >> filename;
   cin.ignore();
}

/*******************************
 * GET LETTER
 ******************************/
char getLetter()
{
   char letter;
   cout << "What letter should we count: ";
   letter = cin.get();
   return letter;
}

/*************************************
 * READ DATA
 * Get the filename, open the file, read
 * the text looking for 'letter', and
 * return what I found
 ************************************/
void readData(int & num, char & letter)
{
   num = 0;

   // get the stuff
   char filename[256];
   getFilename(filename);
   letter = getLetter();

   // open the file
   ifstream fin(filename);
   if (fin.fail())
   {
      cout << "Unable to open file '" << filename << "'\n";
      return;
   }
   
   // read the data, one word at a time
   char letterRead;
   while (!fin.eof())
   {
      letterRead = fin.get();
      if (letterRead == letter)
         num++;
   }
   
   // close the file
   fin.close();
}

/**************************************
 * DISPLAY
 * yep
 *************************************/
void display (int   num, char   letter)
{
   cout << "There are " << num << " " << letter << "'s in the file\n";
}

/**********************************************************************
 * MAIN
 * Count the number of letters in a file
 ***********************************************************************/
int main()
{
   // read the data
   int num;
   char letter;
   readData(num, letter);

   // display
   display(num, letter);
      
   return 0;
}

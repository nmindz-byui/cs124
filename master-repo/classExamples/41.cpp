/***********************************************************************
 * This program is designed to demonstrate:
 *      cout and cerr
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
* This will be just a simple driver program 
***********************************************************************/
int main()
{
   cout << "COUT\n";
   cerr << "CERR\n";
   
   return 0;
}
